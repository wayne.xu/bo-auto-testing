import allure

from typing import List
from datetime import datetime, timedelta

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC

from bo_shared.bo_service_pages import BoService
from rebate_service.pages.rebate_member_exclusion_share import RebateMEShare


class RebateMERebateLevel(RebateMEShare):

    PAGE_NAME = BoService.Rebate.RebateLevel

    def __init__(self, driver: WebDriver):
        super(RebateMERebateLevel, self).__init__(driver)

    # 點擊 Member Rebate Level
    @allure.step("Click Member Rebate Level")
    def click_the_member_rebate_level(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(.,'Member Rebate Level')]"
        ).click()

    # 點擊 Member Blacklist
    @allure.step("Click Member Blacklist")
    def click_the_member_blacklist(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(.,'Member Blacklist')]"
        ).click()

    # Select all by Member Blacklist
    @allure.step("Click Member Blacklist")
    def select_all_by_member_blacklist(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[2]/div[2]/div/div[3]//table/thead/tr/th[1]/div/label/span'
        ).click()

    # 全選當前頁面 Member
    @allure.step("Select all member")
    def select_all_member(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "(//input[@value=''])[5]"
        ).click()

    # 點選批次修改按鈕
    @allure.step("Click the Batch Modification Button")
    def click_the_batch_modify_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(.,'Manage Rebate Level')]"
        ).click()

    # Set up members permission
    @allure.step("Set up members permission")
    def set_up_the_member_permission(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(.,'Select Member Codes')]"
        ).click()

    # Input the membercode in Manage Rebate Level page
    @allure.step("Input the membercode in Manage Rebate Level page")
    def input_membercode_for_rebate_level(self, membercode):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//input[@id='memberCode']"
        ).send_keys(membercode)

    # Set-up the SLOT permission
    @allure.step("Set-up the slot permission")
    def setup_the_slot_permission(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="slot"]'
        ).click()
        # # General
        # self.find_element_wait(
        #     EC.presence_of_element_located,
        #     By.XPATH,
        #     "/html/body/div[4]/div/div/div/div[2]/div[1]/div/div/div[1]"
        # ).click()
        # # VIP
        # self.find_element_wait(
        #     EC.presence_of_element_located,
        #     By.XPATH,
        #     '/html/body/div[4]/div/div/div/div[2]/div[1]/div/div/div[2]'
        # ).click()

    # Set-up the PT permission
    @allure.step("Set-up the slot permission")
    def setup_the_pt_permission(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="pt"]'
        ).click()

    # Set-up the AK permission
    @allure.step("Set-up the slot permission")
    def setup_the_ak_permission(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="ak"]'
        ).click()

    # Click the Bulk Switch Level button
    @allure.step("Click the Bulk Switch Level button")
    def click_the_bulk_switch(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(.,'Bulk Switch Level')]"
        ).click()

    # Select the Mark all as VIP
    @allure.step("Select the Mark all as VIP")
    def mark_all_as_vip(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(.,'Mark all as VIP')]"
        ).click()

    # Select the Mark all as General
    @allure.step("Select the Mark all as General")
    def mark_all_as_general(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(.,'Mark all as General')]"
        ).click()

    # Click Save button
    @allure.step("Click Save button")
    def click_the_save_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(.,'Save')]"
        ).click()

    # Click Update button
    @allure.step("Click Update button")
    def click_the_update_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]/section/section/main/div/form/div/ul/li/span/div/button'
        ).click()

    # 取得 MemberExclusion 的 membercode
    @allure.step("Get the Membercode from MemberExclusion page")
    def get_the_membercode1_form_exclusion_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[3]/div[2]//div[2]//table/tbody'
        )
        return element.text.strip()

    # 取得 MemberExclusion 的 list membercode
    @allure.step("Get the Membercode from MemberExclusion page")
    def get_the_1st_membercode_from_exclusion_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[3]/div[2]/div/div[2]//table/tbody/tr[*]/td[2]'
        )
        return element.text.strip()

    # 取得 Manage Rebate Level 的 1st membercode
    @allure.step("Get the Membercode from MemberExclusion page")
    def get_the_1st_membercode_from_mrl_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[2]/div[1]/h1'
        )
        return element.text.strip()

    # 取得 Manage Rebate Level 的 membercode
    @allure.step("Get the Membercode from Manage Rebate Level page")
    def get_the_membercode_from_mrl_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div/div[1]/div/div[2]/div/div[1]/div/div[2]'
        )
        return element.text.strip()

    # Click the edit button for 1st member
    @allure.step("Click the edit button for 1st member")
    def click_the_edit_btn_for_1st_member(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[3]/div[2]/div/div[2]//table/tbody/tr[2]/td[4]/button'
        ).click()

    # Click the search button for main page
    @allure.step("Click the search button for main page")
    def click_the_search_btn_for_main(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[3]/div[1]/div/div[2]/div[2]/button[2]'
        ).click()

    # 點擊Date picker
    @allure.step("Click the data picker")
    def click_the_date_picker(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//*[@id="__next"]//div[3]/div[1]/div/div[1]//div[1]/div/div[2]/div/div/div/div[1]'
        ).click()

    # 輸入起始時間
    @allure.step("Input the Start date")
    def input_the_date_str(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//div[2]//div[2]/div/div[1]/div/div[2]/table/tbody/tr[3]/td[4]'
        ).click()

    # 輸入結束時間
    @allure.step("Input the End date")
    def input_the_date_end(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//div[2]//div[2]/div/div[2]/div/div[2]/table/tbody/tr[3]/td[3]'
        ).click()

    # 取得Last Updated的起始時間
    @allure.step("Get the Start date")
    def get_the_date_str(self):
        date_str = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "(//input[@placeholder='Start date'])[1]"
        ).get_attribute("value")
        day_before = datetime.strptime(date_str, '%Y-%m-%d')
        # month_int_str_0 = int(date_str[5])
        # month_int_str_1 = int(date_str[6])
        # # 重組月份
        # month_int_str_result = month_int_str_1 * 10 + month_int_str_0
        return day_before

    # 取得Last Updated的當前時間
    @allure.step("Get the End date")
    def get_the_date_end(self):
        date_end = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "(//input[@placeholder='End date'])[1]"
        ).get_attribute("value")
        current_day = datetime.strptime(date_end, '%Y-%m-%d')
        # month_int_end_0 = int(date_end[8])
        # month_int_end_1 = int(date_end[9])
        # # 重組月份
        # month_int_end_result = month_int_end_0 * 10 + month_int_end_1
        return current_day

    # 取得 Member code 輸入窗的值
    @allure.step("Get the membercode date")
    def get_the_date_membercode(self):
        date_membercode = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="memberCode"]'
        ).get_attribute("value")
        return date_membercode

    # 取得 Last Update By 輸入窗的值
    @allure.step("Get the last update by date")
    def get_the_date_lastupdateby(self):
        date_lastupdateby = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="updateBy"]'
        ).get_attribute("value")
        return date_lastupdateby

    # 取得 Last Update By in List
    @allure.step("Get the last update by in list")
    def get_the_date_lastupdateby_in_list(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[3]/div[2]//div[2]//table/tbody'
        )
        return element.text.strip()

    # Input the last Update By Who
    @allure.step("Input the last Update By Who")
    def input_the_last_update_by_who(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="updateBy"]'
        ).send_keys("Editor01")
