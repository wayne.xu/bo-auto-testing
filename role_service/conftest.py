import pytest

from utils.utils import TestDataLoader, SystemLogger

log = SystemLogger.get_logger()


# Define pytest.fixture for role_service testing use.

@pytest.fixture()
def test_account() -> dict:
    test_data = TestDataLoader()
    test_account = test_data['test_login_account']
    return {
        'email': test_account['email'],
        'password': test_account['password']
    }


@pytest.fixture()
def test_role_data() -> dict:
    test_data = TestDataLoader()
    test_role_data = test_data['data_for_role_service']
    return {
        'new_policy_name': test_role_data['NewPolicyName'],
        'duplicate_policy_name': test_role_data['DuplicatePolicyName'],
        'modify_policy_name': test_role_data['ModifyPolicyName'],
        'new_role_name': test_role_data['NewRoleName'],
        'duplicate_role_name': test_role_data['DuplicateRoleName'],
        'modify_role_name': test_role_data['ModifyRoleName'],
        'user_name': test_role_data['UserName'],
        'user_0_email': test_role_data['User_0_Email'],
        'user_1_email': test_role_data['User_1_Email'],
        'user_2_email': test_role_data['User_2_Email'],
        'status_eq_activate': test_role_data['StatusEqActivate'],
        'status_eq_deactivate': test_role_data['StatusEqDeactivate'],
        'first_parent_role': test_role_data['1stRoleParentTeam'],
        'second_parent_role': test_role_data['2ndRoleParentTeam']
    }


@pytest.fixture()
def test_role_breadcrumb() -> dict:
    test_data = TestDataLoader()
    test_role_breadcrumb = test_data['data_for_role_breadcrumb']
    return {
        'add_role': test_role_breadcrumb['AddRole'],
        'add_policy': test_role_breadcrumb['AddPolicy']
    }
