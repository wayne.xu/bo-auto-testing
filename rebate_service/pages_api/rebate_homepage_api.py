import allure
import pytest
import requests

from utils.utils import TestDataLoader
from utils.base_api_request import BaseAPIRequest

# It's just a sample code for demonstrating API testing.


class RebateHomePageAPI(BaseAPIRequest):

    def __init__(self):
        super(RebateHomePageAPI, self).__init__()

    # def get_test_data(self, params: dict):
    #     api_url = self.url_router.get_api_url('get_data')
    #     print(api_url)
    #     res = self.requests.get(api_url, params=params)
    #     return res


