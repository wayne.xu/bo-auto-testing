import allure
import pytest

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC

from utils.utils import SystemLogger

# log = SystemLogger.get_logger()


# @pytest.fixture
# def funpodium_products():
#     return {'Content Management Service', 'Microservices', 'PayBnB'}


# @pytest.fixture
# def funpodium_exprtises():
#     return {'Experience Design', 'Mobile App'}


# @allure.severity(allure.severity_level.CRITICAL)
# @allure.title('Test product: {funpodium_products}')
# @allure.feature('Test visit funpodium and get all products')
# @pytest.mark.xdist_group(name='group1')
# def test_get_funpodium_products(driver: WebDriver, funpodium_products):
#     url = 'https://www.funpodium.net/?ul=t'
#     driver.get(url)
#     product_elements = WebDriverWait(driver, 20).until(
#         EC.presence_of_all_elements_located(
#             (By.XPATH, '//*[@id="sectionProducts"]//a/div/section/h5')
#         )
#     )
#     all_products = [element.text.strip() for element in product_elements]
#     log.info(f'products: {all_products}')
#     assert funpodium_products == set(all_products)


# @allure.severity(allure.severity_level.CRITICAL)
# @allure.title('Test exprtises: {funpodium_exprtises}')
# @allure.feature('Test visit funpodium and get all exprtises')
# @pytest.mark.xdist_group(name='group1')
# def test_get_funpodium_exprtises(driver: WebDriver, funpodium_exprtises):
#     url = 'https://www.funpodium.net/?ul=t'
#     driver.get(url)
#     expertises_elements = WebDriverWait(driver, 20).until(
#         EC.presence_of_all_elements_located(
#             (By.XPATH, '//*[@id="sectionSolutions"]//a/div/section/h5')
#         )
#     )
#     all_expertises = [ele.text.strip() for ele in expertises_elements]
#     log.info(f'all_expertise: {all_expertises}')
#     assert funpodium_exprtises == set(all_expertises)


# @allure.severity(allure.severity_level.BLOCKER)
# @allure.feature('Test visit google page')
# @pytest.mark.xdist_group(name='group2')
# def test_visit_google(driver: WebDriver):
#     url = 'https://www.google.com/'
#     driver.get('https://www.google.com/')
#     log.info(f"Visit Page => `{url}`")
#     assert driver.current_url == url


# @allure.severity(allure.severity_level.MINOR)
# @allure.feature('Test visit google news page, get all news, and current_position')
# @pytest.mark.xdist_group(name='group3')
# def test_get_google_news(driver: WebDriver):
#     driver.get('https://news.google.com/')
#     elements = WebDriverWait(driver, 20).until(
#         EC.presence_of_all_elements_located(
#             (By.XPATH, '//*[@id="yDmH0d"]//div/article/h3/a')
#         )
#     )
#     today_news = [
#         {'title': element.text, 'url': element.get_attribute('href')}\
#         for element in elements
#     ]
#     location_element = WebDriverWait(driver, 20).until(
#         EC.presence_of_element_located(
#             (By.XPATH, '//*[@id="yDmH0d"]//div/div[1]/div/div[1]/div/div[1]/h2')
#         )
#     )
#     log.info(f'today_news:\n {today_news}')
#     log.info(f'Current locate at: {location_element.text}')
#     assert isinstance(today_news, list)
#     assert isinstance(location_element.text, str)