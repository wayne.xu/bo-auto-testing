import time
import allure

import pytest
import pytest_check as check

from utils.envs import Envs
from utils.decorators import execute_env
from utils.test_data_gen import TestDataGenerator
from rebate_service.pages.rebate_management import RebateManagement

from bo_shared.ui_shared import RecurrenceOptions, StatusOptions
from bo_shared.components.ant_table import AntTable
from bo_shared.components.radio_form import RadioForm, RADIO_FORM_INFO
from bo_shared.components.check_box_form import CheckBoxForm, CHECK_BOX_INFO
from bo_shared.components.select_dropdown import SelectDropDown, DROP_DOWN_INFO
from bo_shared.components.date_picker import DatePicker, DATE_PICKER_INFO


@pytest.fixture
def expected_accumulate_list():
    return [
        'Total Turnover',
        'Total deposit',
        'Total withdraw',
        'Total rebate given'
    ]


@pytest.fixture
def approved_action_names():
    return {
        'tab_name': 'approved',
        'action_names': ['view_performance', 'more'],
    }


@pytest.fixture
def pending_action_names():
    return {
        'tab_name': 'pending',
        'action_names': ['approve', 'reject', 'more'],
    }


@pytest.fixture
def rejected_action_names():
    return {
        'tab_name': 'rejected',
        'action_names': ['view_rules', 'more'],
    }


@pytest.fixture
def add_rebate_step1_required_fields():
    return [
        'rebate_name', 'platform', 'period',
        'rebate_level', 'product_group', 'wallet',
        'calculate_rebate_based_on', 'minimum_amount'
    ]


def test_recurrence_logic_data():
    current_date_plus_one = TestDataGenerator.current_date_delta(1)
    three_range_start_date, three_range_end_date = TestDataGenerator.generate_range_date(1, 3)
    eight_range_start_date, eight_range_end_date = TestDataGenerator.generate_range_date(1, 8)
    return [
        [
            current_date_plus_one,
            current_date_plus_one,
            (
                RecurrenceOptions.NO_RECURRENCE.value,
                RecurrenceOptions.DAILY.value,
                RecurrenceOptions.WEEKLY.value,
                RecurrenceOptions.MONTHLY.value,
            )
        ],
        [
            three_range_start_date,
            three_range_end_date,
            (
                RecurrenceOptions.NO_RECURRENCE.value,
                RecurrenceOptions.WEEKLY.value,
                RecurrenceOptions.MONTHLY.value
            )
        ],
        [
            eight_range_start_date,
            eight_range_end_date,
            (
                RecurrenceOptions.NO_RECURRENCE.value,
                RecurrenceOptions.MONTHLY.value
            )
        ]
    ]


@allure.feature('Rebate Service Management test')
class TestRebateManagement():

    @execute_env(envs=(Envs.ALL.value))
    @allure.story("Test for add rebate btn could direct to right page or not")
    def test_add_rebate_btn(self, driver, test_account):
        management = RebateManagement(driver)
        management.login(test_account)
        management.click_add_rebate_btn()
        current_page = management.get_current_page_name(include_parents=True)
        step = management.get_add_rebate_current_step()
        assert current_page == 'Rebate/Management/Add Rebate'
        assert step == 1

#     # Use case Add rebate
#     # Case 1 Input inspection
#     # Case 2 Batch select member

#     # Case 3 Create a new one Rebate
#     @allure.title("Case 3 Create a new one Rebate")
#     @allure.severity(allure.severity_level.BLOCKER)
#     @allure.link('https://hermes.devgambit.net/browse/PSS-302', name='[Management] Add rebate')
#     @allure.story("Test for the Add a Rebate")
#     def test_for_the_add_a_rebate(self, driver, test_account, test_add_rebate_data):
#         management = RebateManagement(driver)
#         management.login(test_account)
#         management.click_add_rebate_btn()
#         # 確認畫面提留在Add rebate的Step1
#         page_step1 = management.get_the_page_step_1()
#         check.equal(page_step1, "step 1")
#         management.input_name_for_add_rebate_by_step1(test_add_rebate_data['rebate_name'])
#         management.input_tncURL_for_add_rebate_by_step1(test_add_rebate_data['tnc_url'])
#         management.select_platform_D_for_add_rebate_by_step1()
#         management.select_platform_M_for_add_rebate_by_step1()
#         management.select_rules_align_for_add_rebate_by_step1()
#         management.select_rules_different_for_add_rebate_by_step1()
#         # 將畫面滾動至顯示下半部
#         management.scroll_down_add_rebate_page()
#         management.select_the_day_for_period_by_step1()
#         management.select_recurrence_norepeat_for_add_rebate_by_step1()
#         management.select_recurrence_monthly_for_add_rebate_by_step1()
#         management.select_am_for_add_rebate_by_step1()
#         management.select_sml_for_add_rebate_by_step1()
#         management.select_sm_for_add_rebate_by_step1()
#         # rebate_management.select_memberlevel_vip_for_add_rebate_by_step1()
#         # rebate_management.select_memberlevel_general_for_add_rebate_by_step1()
#         # rebate_management.select_memberlevel_Level1_for_add_rebate_by_step1()
#         # rebate_management.select_memberlevel_Level2_for_add_rebate_by_step1()
#         # rebate_management.select_memberlevel_Level3_for_add_rebate_by_step1()
#         # rebate_management.select_memberlevel_Level4_for_add_rebate_by_step1()
#         management.select_rebatesending_everyday_for_add_rebate_by_step1()
#         management.select_rebatesending_everyweek_for_add_rebate_by_step1()
#         management.select_rebatesending_everymonth_for_add_rebate_by_step1()
#         management.select_product_for_add_rebate_by_step1()
#         management.select_accumulate_for_add_rebate_by_step1()
#         management.select_one_accumulate()
#         management.input_min_amount_for_add_rebate_by_step1(test_add_rebate_data['mini_amount'])
#         # 進入Add a rebate的Step 2
#         management.click_the_next_btn_to_step2()
#         # 確認畫面提留在Add rebate的Step2
#         page_step1 = management.get_the_page_step_2()
#         check.equal(page_step1, "step 2")
#         # 設定maximum rebate given as per/member
#         management.select_the_maximum_rebate_given_molimit_pm()
#         management.select_the_maximum_rebate_given_havelimit_pm(test_add_rebate_data['rebate_given_per_member'])
#         # 判斷max rebate given == 33.33 (Maximum rebate given per member :: mgpm)
#         mgpm = management.get_max_rebate_given_per_member()
#         check.equal(mgpm, test_add_rebate_data['rebate_given_per_member'])
#         # 設定maximum rebate given as this event
#         management.select_the_maximum_rebate_given_molimit_te()
#         management.select_the_maximum_rebate_given_havelimit_te(test_add_rebate_data['rebate_given_this_event'])
#         # 判斷max rebate given == 66.66
#         mgte = management.get_max_rebate_given_this_event()
#         check.equal(mgte, test_add_rebate_data['rebate_given_this_event'])
#         # Add Tier
#         management.click_the_add_tier_btn()
#         # 填充 rebate tier 1
#         management.setup_the_rebate_tier_1(test_add_rebate_data['t1min'], test_add_rebate_data['t1max'], test_add_rebate_data['t1rebate'])
#         # 判斷 rebate tier 2 的 Min. deposit amount == tier 1 的 Max. deposit amount + 0.01
#         value_tier2_min = management.get_min_deposit_tier2()
#         tier1add = management.tier_add(test_add_rebate_data['t1max'])
#         # pytest.check(value_tier2_min == tier2add)
#         check.equal(value_tier2_min, tier1add)
#         # assert value_tier2_min == tier2add
#         # 填充 rebate tier 2
#         management.setup_the_rebate_tier_2(test_add_rebate_data['t2min'], test_add_rebate_data['t2max'], test_add_rebate_data['t2rebate'])
#         # 進入Add a rebate的Step 3
#         management.click_the_next_btn_to_step3()
#         # 確認畫面提留在Add rebate的Step3
#         page_step1 = management.get_the_page_step_3()
#         check.equal(page_step1, "step 3")
#         # assert rebate.Name
#         rebate_name = management.get_rebate_name_in_step3()
#         check.equal(rebate_name, test_add_rebate_data['rebate_name'])
#         # assert rebate.TnC URL
#         rebate_tnc_url = management.get_tnc_url_in_step3()
#         check.equal(rebate_tnc_url, test_add_rebate_data['tnc_url'])
#         # assert rebate.Platform
#         rebate_platform = management.get_platform_in_step3()
#         check.equal(rebate_platform, "Desktop\nMobile")
#         # assert rebate.Rules
#         rebate_rules = management.get_rules_in_step3()
#         check.equal(rebate_rules, "Different rules in 2 platforms")
#         # assert rebate.Period (GMT+8)
#         rebate_period = management.get_period_in_step3()
#         check.equal(rebate_period, "date Start:"" ~ date To:"".")
#         # assert rebate.Repetition
#         rebate_repetition = management.get_repetition_in_step3()
#         check.equal(rebate_repetition, "No repeat")
#         # assert T1 rebate Range
#         rebate_t1_range = management.get_tier1_range_desktop()
#         range_t1_result = management.reorg_tier1_range_desktop(test_add_rebate_data['t1min'], test_add_rebate_data['t1max'])
#         check.equal(rebate_t1_range, range_t1_result)
#         # assert T1 rebate Rate
#         rebate_t1_rate = management.get_tier1_rate_desktop()
#         rate_t1_result = management.reorg_tier1_rate_desktop(test_add_rebate_data['t1rebate'])
#         check.equal(rebate_t1_rate, rate_t1_result)
#         # assert T2 rebate Range
#         rebate_t2_range = management.get_tier2_range_desktop()
#         range_t2_result = management.reorg_tier2_range_desktop(test_add_rebate_data['t1max'])
#         check.equal(rebate_t2_range, range_t2_result)
#         # assert T2 rebate Rate
#         rebate_t2_rate = management.get_tier2_rate_desktop()
#         rate_t2_result = management.reorg_tier2_rate_desktop(test_add_rebate_data['t2rebate'])
#         check.equal(rebate_t2_rate, rate_t2_result)

#     # Case 4 Duplicate from exist rebate

    # Use case View
    # Case 5 The default is Approved Request
    @execute_env(envs=(Envs.ALL.value))
    @allure.title("Case 5 The default is Approved Request")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-303', name='[[Management][rebate history] View')
    @allure.story("Test for default selected item")
    def test_management_page_default_tab_name(self, driver, test_account):
        management = RebateManagement(driver)
        management.login(test_account)
        tab_name = management.get_current_selected_tab()
        check.equal(tab_name, 'approved')

    # Case 6 Check the elements in list > Approved Request
    @execute_env(envs=(Envs.ALL.value))
    @allure.title("Case 6 Check the elements in list > Approved Request")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-303', name='[[Management][rebate history] View')
    @allure.story("Test for Action button element")
    def test_approved_tab_default_action_element(self, driver, test_account, approved_action_names):
        expect_tab_name = approved_action_names['tab_name']
        expect_names = approved_action_names['action_names']
        management = RebateManagement(driver)
        management.login(test_account)
        tab_name = management.get_current_selected_tab()
        check.equal(tab_name, expect_tab_name)
        management.is_page_loading_success()
        all_action_info = management.get_table_action_elements()
        for expect_name, action_info in zip(expect_names, all_action_info[0]):
            assert expect_name == action_info['name']

    # Case 6 Check the elements in list > Pending Request
    @execute_env(envs=(Envs.ALL.value))
    @allure.title("Case 6 Check the elements in list > Pending Request")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-303', name='[[Management][rebate history] View')
    @allure.story("Test for Action button element")
    def test_pending_tab_btn_default_action_element(self, driver, test_account, pending_action_names):
        expect_tab_name = pending_action_names['tab_name']
        expect_names = pending_action_names['action_names']
        management = RebateManagement(driver)
        management.login(test_account)
        management.click_tab_list(expect_tab_name)
        current_tab_name = management.get_current_selected_tab()
        check.equal(expect_tab_name, current_tab_name)
        all_action_info = management.get_table_action_elements()
        for expect_name, element_info in zip(expect_names, all_action_info[0]):
            assert expect_name == element_info['name']

    # Case 6 Check the elements in list > Rejected Request
    @execute_env(envs=(Envs.ALL.value))
    @allure.title("Case 6 Check the elements in list > Rejected Request")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-303', name='[[Management][rebate history] View')
    @allure.story("Test for Action button element")
    def test_rejected_tab_btn_default_action_element(self, driver, test_account, rejected_action_names):
        expect_tab_name = rejected_action_names['tab_name']
        expect_names = rejected_action_names['action_names']
        management = RebateManagement(driver)
        management.login(test_account)
        management.click_tab_list(expect_tab_name)
        current_tab_name = management.get_current_selected_tab()
        check.equal(expect_tab_name, current_tab_name)
        all_action_info = management.get_table_action_elements()
        for expect_name, element_info in zip(expect_names, all_action_info[0]):
            assert expect_name == element_info['name']

    @execute_env(envs=(Envs.ALL.value))
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story("Test Add rebate step1 required fields")
    def test_add_rebate_step1_required_fields(self, driver, test_account, add_rebate_step1_required_fields):
        management = RebateManagement(driver)
        management.login(test_account)
        management.click_add_rebate_btn()
        # Click add rebate page next button
        management.click_add_rebate_page_btn('next')
        time.sleep(3)
        require_fields = management.get_add_rebate_required_field_names()
        for current_fields, test_fields in zip(require_fields, add_rebate_step1_required_fields):
            check.equal(current_fields, test_fields)

    @execute_env(envs=(Envs.ALL.value))
    @allure.severity(allure.severity_level.MINOR)
    @allure.story("Test add rebate page platform CheckBoxForm multiple logic display")
    def test_add_rebate_check_box_form_multiple_logic_display(self, driver, test_account):
        management = RebateManagement(driver)
        management.login(test_account)
        management.click_add_rebate_btn()
        time.sleep(1)
        platform_rule_radio_form = RadioForm(
            driver,
            'platform_rule',
            RADIO_FORM_INFO['platform_rule']['form_data_test_id'],
            RADIO_FORM_INFO['platform_rule']['options_data_test_id']
        )
        is_hidden = platform_rule_radio_form.is_form_hidden()
        check.equal(is_hidden, True)
        platform_check_box = CheckBoxForm(
            driver,
            'platform',
            CHECK_BOX_INFO['platform']['form_data_test_id'],
            CHECK_BOX_INFO['platform']['options_data_test_id']
        )
        platform_check_box.select_options(['desktop', 'mobile'])
        is_hidden = platform_rule_radio_form.is_form_hidden()
        check.equal(is_hidden, False)

    @execute_env(envs=(Envs.ALL.value))
    @allure.severity(allure.severity_level.MINOR)
    @allure.story("Test add rebate page recurrence logic")
    @pytest.mark.parametrize(
        'start_date, end_date, expect_options',
        test_recurrence_logic_data()
    )
    def test_add_rebate_recurrence_logic(self, driver, test_account, start_date, end_date, expect_options):
        management = RebateManagement(driver)
        management.login(test_account)
        management.click_add_rebate_btn()
        management.scroll_down_percent(1/5)
        time.sleep(1)
        period_date_picker = DatePicker(
            driver,
            'period',
            DATE_PICKER_INFO['period']['data_test_id']
        )
        period_date_picker.click_start_date_input()
        period_date_picker.select_date_picker(start_date, end_date)
        recurrence_dropdown = SelectDropDown(
            driver,
            DROP_DOWN_INFO['add_rebate_recurrence']['form_data_test_id'],
            DROP_DOWN_INFO['add_rebate_recurrence']['form_info_data_test_id'],
            DROP_DOWN_INFO['add_rebate_recurrence']['options_locate_id'],
            'add_rebate_recurrence'
        )
        recurrence_dropdown.click_select_dropdown()
        current_options = recurrence_dropdown.get_current_options_content()
        assert len(current_options) == len(expect_options)
        for current_option, expect_option in zip(current_options, expect_options):
            check.equal(current_option, expect_option)

    @execute_env(envs=(Envs.ALL.value))
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story("Test rebate management status filter")
    @pytest.mark.parametrize(
        'tab_name, status_option',
        [
            ['approved', StatusOptions.NOT_STARTED.value],
            ['approved', StatusOptions.IN_PROGRESS.value],
            ['approved', StatusOptions.FINISHED.value],
            ['approved', StatusOptions.DEACTIVATE.value],
            ['pending', 'Needs to Check'],
            ['pending', 'Expired']
        ]
    )
    def test_rebate_management_status_filter(self, driver, test_account, tab_name, status_option):
        management = RebateManagement(driver)
        management.login(test_account)
        management.click_tab_list(tab_name)
        status_select_dropdown = SelectDropDown(
            driver,
            DROP_DOWN_INFO['management_status']['form_data_test_id'],
            DROP_DOWN_INFO['management_status']['form_info_data_test_id'],
            DROP_DOWN_INFO['management_status']['options_locate_id'],
            'management_status'
        )
        status_select_dropdown.click_select_dropdown()
        status_select_dropdown.select_options([status_option, ])
        management.click_filter_manipulation_btn('search')
        time.sleep(3)
        ant_table = AntTable(driver)
        content_elements = ant_table.get_table_contents_with_column_name('status')
        for content_element in content_elements:
            content = content_element.text.strip()
            assert content == status_option

    @execute_env(envs=(Envs.ALL.value))
    @allure.severity(allure.severity_level.CRITICAL)
    @allure.story("Test rebate management recurrence filter")
    @pytest.mark.parametrize(
        'tab_name, recurrence_option',
        [
            ['approved', RecurrenceOptions.NO_RECURRENCE.value],
            ['approved', RecurrenceOptions.DAILY.value],
            ['approved', RecurrenceOptions.WEEKLY.value],
            ['approved', RecurrenceOptions.MONTHLY.value],
            ['pending', RecurrenceOptions.NO_RECURRENCE.value],
            ['pending', RecurrenceOptions.DAILY.value],
            ['pending', RecurrenceOptions.WEEKLY.value],
            ['pending', RecurrenceOptions.MONTHLY.value],
        ]
    )
    def test_rebate_management_recurrence_filter(self, driver, test_account, tab_name, recurrence_option):
        management = RebateManagement(driver)
        management.login(test_account)
        management.click_tab_list(tab_name)
        management.click_filter_manipulation_btn('expand')
        status_select_dropdown = SelectDropDown(
            driver,
            DROP_DOWN_INFO['management_recurrence']['form_data_test_id'],
            DROP_DOWN_INFO['management_recurrence']['form_info_data_test_id'],
            DROP_DOWN_INFO['management_recurrence']['options_locate_id'],
            'management_recurrence'
        )
        status_select_dropdown.click_select_dropdown()
        status_select_dropdown.select_options([recurrence_option, ])
        management.click_filter_manipulation_btn('search')
        time.sleep(3)
        ant_table = AntTable(driver)
        content_elements = ant_table.get_table_contents_with_column_name('recurrence')
        for content_element in content_elements:
            content = content_element.text.strip()
            assert content == recurrence_option

#     # Use case Actions button (Approved Request)
#     # Case 7 View rules
#     # Case 8 Duplicate
#     # Case 9 Change Logs
#     @allure.title("Case 9 Change Logs")
#     @allure.severity(allure.severity_level.NORMAL)
#     @allure.link('https://hermes.devgambit.net/browse/PSS-304', name='[Management][Actions button] Status as Approved Request')
#     @allure.story("Test for Change Logs button")
#     def test_for_the_action_change_logs_btn(self, driver, test_account):
#         management = RebateManagement(driver)
#         management.login(test_account)
#         management.click_the_action_list_btn()
#         management.click_the_change_logs_btn()
#         breadcrumb = management.get_current_breadcrumb_lv4()
#         check.is_in("change-log", breadcrumb)

#     # Case 10 Deactivate
#     @allure.title("Case 10 Deactivate")
#     @allure.severity(allure.severity_level.NORMAL)
#     @allure.link('https://hermes.devgambit.net/browse/PSS-304', name='[Management][Actions button] Status as Approved Request')
#     @allure.story("Test for Deactivate button")
#     def test_for_the_action_deactivate_btn(self, driver, test_account):
#         management = RebateManagement(driver)
#         management.login(test_account)
#         management.click_the_action_list_btn()
#         management.click_the_deactivate_logs_btn()
#         management.open_the_dp_for_deactivate()
#         time.sleep(1)
#         management.submit_date_for_deactivate()
#         time.sleep(1)
#         management.submit_deactivate()
#         # check.is_in(a, b, "msg=122345")

#     # Case 11 Delete`
#     # Use case Approved Request
#     # Case 12 View performance
#     @allure.title("Case 12 View performance")
#     @allure.severity(allure.severity_level.NORMAL)
#     @allure.link('https://hermes.devgambit.net/browse/PSS-305', name='[Management]rebate records status is Approved Request')
#     @allure.story("Test for View performance")
#     def test_for_the_view_performance_btn(self, driver, test_account):
#         management = RebateManagement(driver)
#         management.login(test_account)
#         time.sleep(1)
#         management.click_the_view_performance_btn()
#         breadcrumb = management.get_current_breadcrumb_lv3()
#         check.is_in("Performance0", breadcrumb)

#     # Case 13 View Performance and Retry All
#     @allure.title("Case 13 View Performance and Retry All")
#     @allure.severity(allure.severity_level.NORMAL)
#     @allure.link('https://hermes.devgambit.net/browse/PSS-305', name='[Management]rebate records status is Approved Request')
#     @allure.story("Test for retry all rebate")
#     def test_for_the_retry_all(self, driver, test_account):
#         management = RebateManagement(driver)
#         management.login(test_account)
#         time.sleep(1)
#         management.click_the_view_performance_btn()
#         time.sleep(1)
#         management.click_the_retry_all_btn()
#         # check.equal(a, b)

#     # Case 14 View performance and Retry one
#     # Case 15 View performance and copy Rebate ID
#     @allure.title("Case 15 View performance and copy Rebate ID")
#     @allure.severity(allure.severity_level.NORMAL)
#     @allure.link('https://hermes.devgambit.net/browse/PSS-305', name='[Management]rebate records status is Approved Request')
#     @allure.story("View performance and copy Rebate ID")
#     def test_for_copy_the_rebate_id(self, driver, test_account):
#         test_num = 0
#         management = RebateManagement(driver)
#         management.login(test_account)
#         time.sleep(1)
#         management.click_the_view_performance_btn(test_num)
#         time.sleep(1)
#         management.click_the_copy_btn_for_rebate_id(test_num)
#         clipboard = management.get_clipboard_text()
#         rebate_id = management.get_the_rebate_id_in_performance_page(test_num)
#         check.equal(rebate_id, clipboard)

#     # Case 16 Export search result
#     @allure.title("Case 16 Export search result")
#     @allure.severity(allure.severity_level.NORMAL)
#     @allure.link('https://hermes.devgambit.net/browse/PSS-305', name='[Management]rebate records status is Approved Request')
#     @allure.story("View performance and copy Rebate ID")
#     def test_export_file_for_approved_request(self, driver, test_account):
#         management = RebateManagement(driver)
#         management.login(test_account)
#         time.sleep(1)
#         management.click_the_export_file_btn()
#         # check.equal(a, b)

    # Use case Actions button (Pending Request
    # Case 17 View rules
    # Case 18 Edit rules
    # Case 19 Duplicate
    # Case 20 Change Logs
    # Case 21 Delete
    # Case 22 Export search result

    # Use case Actions button (Rejected Request
    # Case 23 Duplicate
    # Case  Change Logs
    # Case 25 Delete
    # Case 26 Export search result

    # Use case Filter > Date picker
    # Case 27 By default, select the past 3 months data
    # Case 28 Select more than past 1 year
    # Case 29 Select from past 1 year till now
    # Case 30 Select more than maximum 3 months

    # Use case Filter > Product
    # Case 31 By default, no select
    # Case 32 Multiple Select all items

    # Use case Filter > Platform
    # Case 33 By default, no select
    # Case 34 Select Desktop
    # Case 35 Select Mobile

    # Use case Filter > Rebate Repetition
    # Case 36 By default, no select
    # Case 37 Select Daily
    # Case 38 Select weekly
    # Case 39 Select monthly

    # Use case Filter > Status(different status with different Actions
    # Case 40 By default, no select
    # Case 41 No Started, In Progress, Finished, Deactivated, Expired, Stopped

    # Use case Filter > Eligible Member
    # Case 42 By default, no select
    # Case 43 Multiple Select all items

    # Use case Actions button
    # Case 44 View rules
    # Case 45 Duplicate
    # Case 46 Change Logs
    # Case 47 Deactivate
    # Case 48 Delete

    # Use case Sort
    # Case 49 Customize filter
    # Case 50 Pagination
    # Case 51 Export search result

    # Universal test
    # Case 52 Assert the page name == current page name

    # @allure.title("")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('', name='')
    # @allure.story("Test the title of viewDetail equal the title of records list")
    # def test_func_list(self, driver, test_account):
    #     rebate_management = RebateManagement(driver)
    #     rebate_management.login(test_account)
    #     rebate_management.click_functions_list_r()
    #     rebate_management.click_the_management_btn()
    #     page_name = rebate_management.get_current_page_name()
    #     assert page_name == "Management"
    #     title_of_view_rebate_detail = rebate_management.check_the_page_title_equal_record()
    #     rebate_management.click_the_view_detail_first()
    #     time.sleep(1)
    #     element_rebate_record = rebate_management.check_the_view_detail_first1()
    #     assert title_of_view_rebate_detail == element_rebate_record
    #     time.sleep(3)
    #
    # @allure.story("Test the Action List")
    # def test_the_action_list(self, driver, test_account, test_action_list_by_management):
    #     rebate_management = RebateManagement(driver)
    #     rebate_management.login(test_account)
    #     rebate_management.click_functions_list_r()
    #     rebate_management.click_the_management_btn()
    #     rebate_management.click_the_action_list_btn()
    #     rebate_management.get_Action_list_by_management()
    #     current_action_list = rebate_management.get_Action_list_by_management()
    #     time.sleep(1)
    #     # Check its length
    #     assert len(current_action_list) == len(test_action_list_by_management)
    #     # Check all of the contents whether are the same or not
    #     for current, test in zip(current_action_list, test_action_list_by_management):
    #         assert current == test
    #     time.sleep(3)
    #
    # @allure.story("Test the Detail page include str of Total Turnover and Total Rebate Given(CNY)")
    # def test_the_elemt_in_detail_page(self, driver, test_account):
    #     rebate_management = RebateManagement(driver)
    #     rebate_management.login(test_account)
    #     rebate_management.click_functions_list_r()
    #     rebate_management.click_the_management_btn()
    #     rebate_management.click_the_view_detail_first()
    #     page_elemt_turnover = rebate_management.get_the_str_total_turnover()
    #     assert page_elemt_turnover == ['Total Turnover(CNY)']
    #     page_elemt_rebate_given = rebate_management.get_the_str_total_rebate_given()
    #     assert page_elemt_rebate_given == ['Total Rebate Given(CNY)']
    #     time.sleep(3)
    #
    # @allure.story("Test the Copy btn in Detail page")
    # def test_the_copy_btn_in_detail(self, driver, test_account):
    #     rebate_management = RebateManagement(driver)
    #     rebate_management.login(test_account)
    #     rebate_management.click_functions_list_r()
    #     rebate_management.click_the_management_btn()
    #     rebate_management.click_the_view_detail_first()
    #     rebate_management.click_the_copy_btn_for_rebate_id()
    #     # rebate_management.clipboard_get()
    #     time.sleep(3)

    # @allure.story('Test get accumulate list whether conforming expected values')
    # def test_accumulate_list(self, driver, test_account: dict, expected_accumulate_list):
    #     management = RebateManagement(driver)
    #     management.login(test_account)
    #     management.click_add_rebate_btn()
    #     management.scroll_down_add_rebate_page()
    #     management.click_add_rebate_accumulate_list()
    #     time.sleep(3)
    #     accumulate_list = management.get_add_rebate_accumulate_list()
    #     for real, expected in zip(accumulate_list, expected_accumulate_list):
    #         assert real == expected
