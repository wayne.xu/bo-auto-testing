import re
import time
import allure

from copy import copy, deepcopy
from typing import Optional, List, Union, Tuple

from bo_shared.components.basic import BasicComponent

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


DROP_DOWN_INFO = {
    # DashBoard
    "dashboard_platform": {
        'form_data_test_id': 'platformIds',
        'form_info_data_test_id': 'platformOptions',
        'options_locate_id': 'platformIds_list',
    },
    "dashboard_recurrence": {
        'form_data_test_id': 'recurrenceId',
        'form_info_data_test_id': 'recurrenceOptions',
        'options_locate_id': 'recurrenceId_list',
    },
    # Management Page
    'management_platform': {
        'form_data_test_id': 'platformIds',
        'form_info_data_test_id': 'platformOptions',
        'options_locate_id': 'platformIds_list',
    },
    'management_status': {
        'form_data_test_id': 'statusIds',
        'form_info_data_test_id': 'statusOptions',
        'options_locate_id': 'statusIds_list'
    },
    'management_recurrence': {
        'form_data_test_id': 'recurrenceIds',
        'form_info_data_test_id': 'recurrenceOptions',
        'options_locate_id': 'recurrenceIds_list',
    },
    'management_rebate_calculation_frequency': {
        'form_data_test_id': 'lastCalculationTime',
        'form_info_data_test_id': 'calculationFrequencyOptions',
        'options_locate_id': 'calculationFrequencyIds_list',
    },
    'management_release_method': {
        'form_data_test_id': 'releaseMethodOptions',
        'form_info_data_test_id': 'releaseMethodOptions',
        'options_locate_id': 'releaseMethodOptions_list',
    },
    # Add rebate page
    'add_rebate_rebate_level': {
        'form_data_test_id': 'eligibleMember,levels',
        'form_info_data_test_id': 'levels',
        'options_locate_id': 'step1_eligibleMember_levels_list',
    },
    'add_rebate_wallet': {
        'form_data_test_id': 'walletCode',
        'form_info_data_test_id': 'wallet',
        'options_locate_id': 'step1_walletCode_list'
    },
    'add_rebate_recurrence': {
        'form_data_test_id': 'recurrence',
        'form_info_data_test_id': 'recurrence',
        'options_locate_id': 'step1_recurrence_list',
    }
}


class SelectDropDown(BasicComponent):

    click_dropdown_xpath_format = (
        '//div[@data-testid="{form_data_test_id}"]'
        '//div[@data-testid="{form_info_data_test_id}"]'
    )
    # To get the options
    options_xpath_format = (
        '//div[@id="{options_locate_id}"]/'
        'following-sibling::div//div[@data-testid]'
    )
    check_dropdown_type_xpath_format = (
        '//div[@id="{options_locate_id}"]'
        '/following-sibling::div/div[2]'
    )
    # It's used for getting the current height
    current_height_types_xpath_format = {
        'type1': (
            '//div[@id="{options_locate_id}"]/'
            'following-sibling::div/div[1]/div/div'
        ),
        'type2': (
            '//div[@id="{options_locate_id}"]'
            '/following-sibling::div/div[2]/div'
        )
    }
    scrollbar_xpath_format = (
        '//div[@id="{options_locate_id}"]'
        '/following-sibling::div/div[1]'
    )

    def __init__(
        self,
        driver: WebDriver,
        form_data_test_id: str,
        form_info_data_test_id: str,
        options_locate_id: str,
        name: Optional[str] = None,
        scroll_down_px: int = 200
    ):
        """
        An abstract object for drop down list

        Args:
            driver (WebDriver)
            form_data_test_id (str)
            form_info_data_test_id (str)
            options_locate_id (str)
            name (Optional[str], optional): Defaults to None.
            scroll_down_px (int, optional): Defaults to 200.
        """
        super(SelectDropDown, self).__init__(driver)
        self.form_data_test_id = form_data_test_id
        self.form_info_data_test_id = form_info_data_test_id
        self.options_locate_id = options_locate_id
        # Format the xpath
        self.dropdown_xpath = self.click_dropdown_xpath_format.format(
            form_data_test_id=self.form_data_test_id,
            form_info_data_test_id=self.form_info_data_test_id
        )
        self.check_dropdown_type_xpath = self.check_dropdown_type_xpath_format.format(
            options_locate_id=self.options_locate_id
        )
        self.name = name
        self.scroll_down_px = scroll_down_px

    @property
    def current_selection_items_xpath(self):
        return self.dropdown_xpath + (
            '//div[@class="ant-select-selector"]'
            '//span[@class="ant-select-selection-item"]'
        )

    @property
    def default_selection_value_xpath(self):
        return self.dropdown_xpath + (
            '//span[@class="ant-select-selection-placeholder"]'
        )

    @property
    def clear_input_xpath(self):
        return self.dropdown_xpath + '/span'

    def click_select_dropdown(self):
        """
        Click the select dropdown list
        """
        dropdown_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            self.dropdown_xpath
        )
        class_attrs = dropdown_element.get_attribute('class').split(' ')
        if 'ant-select-open' not in class_attrs:
            self.action_chains.click(dropdown_element).perform()
            self.log.debug(f"Click the dropdown, dropdown_xpath: {self.dropdown_xpath}")

    @allure.step("Get current selection items")
    def get_current_selection_items(
        self,
        return_element: bool = False
    ) -> List[Union[str, Tuple[str, WebElement]]]:
        elements = list()
        try:
            elements = self.driver.find_elements(
                By.XPATH,
                self.current_selection_items_xpath
            )
        except NoSuchElementException:
            self.log.debug("The current selection items is empty")

        if return_element:
            elements = [(ele.text.strip(), ele) for ele in elements]
        else:
            elements = [ele.text.strip() for ele in elements]
        return elements

    @allure.step("Get selection default value")
    def get_selection_default_value(self) -> str:
        element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            self.default_selection_value_xpath
        )
        return element.text.strip()

    @allure.step("Clear the current selections")
    def clear_selection(self):
        """
        Clear the current selections
        """
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            self.clear_input_xpath
        )
        for element in elements:
            attr = element.get_attribute('class').strip()
            if 'ant-select-clear' == attr:
                element.click()
                self.log.debug("Click the clear button")
                break
        self.log.debug(
            f"The {self.__class__.__name__} SelectDropDown "
            "elements haven't been selected"
        )

    @property
    def _is_select_multiple(self) -> bool:
        """
        Check the SelectDropDown is multiple selection or single selection
        """
        dropdown_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            self.dropdown_xpath
        )
        class_attrs = dropdown_element.get_attribute('class')
        class_attrs = class_attrs.split(' ')
        if "ant-select-multiple" in class_attrs:
            return True
        else:
            return False

    def _is_type1_position_xpath(self) -> bool:
        """
        Check the SelectDropDown is type1 or type2 position xpath
        """
        # To ensure the dropdown has been clicked
        self.click_select_dropdown()
        current_height_xpath_format = self.current_height_types_xpath_format['type1']
        current_height_xpath = current_height_xpath_format.format(
            options_locate_id=self.options_locate_id
        )
        element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            current_height_xpath
        )
        style_attrs = element.get_attribute('style')
        style_attrs = self.style_attrs_processor(style_attrs)
        if 'transform' in style_attrs:
            xpath_type = 'type1'
            is_type1 = True
        else:
            xpath_type ='type2'
            is_type1 = False
        self.log.debug(
            f"Using {xpath_type} xpath to get the current position "
            f"xpath: {current_height_xpath}"
        )
        return is_type1

    def _is_scrolling_dropdown(self) -> bool:
        """
        Check the drop down list that can scroll down or not
        """
        self.click_select_dropdown()
        element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            self.check_dropdown_type_xpath
        )
        attrs = element.get_attribute('class').split(' ')
        if 'rc-virtual-list-scrollbar-show' in attrs:
            return True
        else:
            return False

    def _get_type2_scroll_position(self):
        """
        Get current scrolling position by type1 current height xpath
        """
        match_pat = re.compile(r"[0-9\.]+")
        current_height_xpath_format = self.current_height_types_xpath_format['type2']
        current_height_xpath = current_height_xpath_format.format(
            options_locate_id=self.options_locate_id
        )
        current_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            current_height_xpath
        )
        style_attrs = current_element.get_attribute('style')
        style_attrs = self.style_attrs_processor(style_attrs)
        current_position = None
        if 'top' in style_attrs:
            position = style_attrs['top']
            search_result = match_pat.search(position)
            current_position = int(search_result.group())
        return current_position

    def _get_type1_scroll_position(self) -> Optional[int]:
        """
        Get current scrolling position by type2 current height xpath
        """
        number_pat = self.shared_regex['number_pat']
        current_height_xpath_format = self.current_height_types_xpath_format['type1']
        current_height_xpath = current_height_xpath_format.format(
            options_locate_id=self.options_locate_id
        )
        current_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            current_height_xpath
        )
        style_attrs = current_element.get_attribute('style')
        style_attrs = self.style_attrs_processor(style_attrs)
        current_position = None
        if 'transform' in style_attrs:
            position = style_attrs['transform']
            search_result = number_pat.search(position)
            current_position = int(search_result.group())
        return current_position

    def _get_current_scroll_position(self) -> Optional[int]:
        """
        Get current scroll position
        """
        if self._is_type1_position_xpath():
            current_position = self._get_type1_scroll_position()
        else:
            current_position = self._get_type2_scroll_position()
        return current_position

    def _scroll_down(self, scroll_step: int):
        scrollbar_xpath = self.scrollbar_xpath_format.format(
            options_locate_id=self.options_locate_id
        )
        self.log.debug(f"Using scrollbar_xpath: {scrollbar_xpath}")
        locator = (By.XPATH, scrollbar_xpath)
        self.scroll_to(0, scroll_step, locator)

    def _validate_selections_type(self, select_options: List[str]):
        if self._is_select_multiple == False:
            if len(select_options) > 1:
                raise ValueError(
                    f"The {self.name} is single selection DropDown, so "
                    f"the length of selection options can't be large than 1"
                )

    def _select_option_without_scrolling(
        self,
        select_options: List[str],
        all_options_info: dict
    ):
        """
        Select the options

        Args:
            select_options (List[str])
            all_options_info (dict)
        """
        self._validate_selections_type(select_options)
        select_options = set(select_options)
        wait_for_selected_options = copy(select_options)
        for _, options_info in all_options_info.items():
            if (
                options_info['title'] in select_options\
                and options_info['is_selectable']
            ):
                self.action_chains.click(options_info['element']).perform()
                self.log.debug(f"Select the {options_info['title']} options")
                # Remove the option from `wait_for_selected_options`
                wait_for_selected_options.remove(options_info['title'])

            if len(wait_for_selected_options) == 0:
                return None

        raise ValueError(
            f"Can't find the options: {wait_for_selected_options} in the "
            f"{[value['title'] for value in all_options_info.values()]}"
        )

    def _select_options_with_scrolling(
        self,
        select_options: List[str],
        all_options_info: dict
    ):
        """
        Scrolling the options until selecting the options

        Args:
            select_options (List[str])
            all_options_info (dict)
        """
        self._validate_selections_type(select_options)
        select_options = set(select_options)
        wait_for_selected_options = deepcopy(select_options)
        options_info = all_options_info
        while True:
            current_position = self._get_current_scroll_position()
            if current_position != 0:
                time.sleep(1)
                options_info = self.get_current_options(options_info)

            for option_info in options_info.values():
                if (
                    option_info['title'] in select_options\
                    and option_info['is_selectable']
                ):
                    try:
                        self.action_chains.click(option_info['element']).perform()
                        # Remove the option from `wait_for_selected_options`
                        wait_for_selected_options.remove(option_info['title'])
                    except Exception as e:
                        self.log.debug(
                            f"The option: {option_info['element']} "
                            "can't be selected, it need to scroll down."
                        )
                    if len(wait_for_selected_options) == 0:
                        return None
            # Scroll down then check current position.
            self._scroll_down(current_position + self.scroll_down_px)
            after_scrolling_position = self._get_current_scroll_position()
            if current_position >= after_scrolling_position:
                break
        if len(wait_for_selected_options) != 0:
            options = [value['title'] for value in options_info.values()]
            raise ValueError(
                f"Can't find the options: {wait_for_selected_options} "
                f"in the {options}"
            )

    @allure.step("Get current options and their information")
    def get_current_options(
        self,
        all_options_info: Optional[dict] = None
    ) -> dict:
        """
        Get current options which were displayed on HTML

        Args:
            all_options_info (Optional[dict], optional): Defaults to None.
            If the argument is None, it will create a new one to save data.
            But if it isn't None, it will update the given data.
        """
        options_xpath = self.options_xpath_format.format(
            options_locate_id=self.options_locate_id
        )
        option_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            options_xpath
        )
        if all_options_info is None:
            all_options_info = dict()

        for option_element in option_elements:
            data_test_id = option_element.get_attribute('data-testid').strip()
            is_selectable = option_element.get_attribute('aria-selected').strip()
            # False => It means the item could be selected.
            is_selectable = self.to_boolean(is_selectable)
            # Convert logic to be more readable
            is_selectable = False if is_selectable else True
            title = option_element.get_attribute('title').strip()
            if data_test_id in all_options_info:
                all_options_info[data_test_id]['is_selectable'] = is_selectable
                all_options_info[data_test_id]['element'] = option_element
            else:
                all_options_info[data_test_id] = {
                    'title': title,
                    'is_selectable': is_selectable,
                    'element': option_element
                }
        return all_options_info

    @allure.step("Get select the given options")
    def select_options(self, select_options: List[str]):
        """
        Select the drop down list options

        Args:
            select_options (List[str])
        """
        options_info = self.get_current_options()
        if self._is_scrolling_dropdown():
            self._select_options_with_scrolling(select_options, options_info)
        else:
            self._select_option_without_scrolling(select_options, options_info)

    @allure.step("Get current options content")
    def get_current_options_content(
        self,
        all_options_info: Optional[dict] = None
    ) -> List[str]:
        """Get the current options content

        Args:
            all_options_info (Optional[dict], optional): Defaults to None.
        """
        all_options_info = self.get_current_options(all_options_info)
        options_content = list()
        for info in all_options_info.values():
            options_content.append(info['title'])
        return options_content

    def __repr__(self):
        return (
            f"< {self.__class__.__name__} Component "
            f"form_data_test_id: {self.form_data_test_id}, "
            f"form_info_data_test_id: {self.form_info_data_test_id}, "
            f"options_locate_id: {self.options_locate_id}, "
            f"is_select_multiple: {self._is_select_multiple}, "
            f"scroll_down_px: {self.scroll_down_px} >"
        )
