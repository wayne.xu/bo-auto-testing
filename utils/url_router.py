import os
import re

from urllib import parse
from typing import Optional, Tuple

from utils.utils import SystemLogger


class URLRouter():
    """
    Url router for getting the specific page or the resource.
    The module support to directly input host and port, or load them from environment variables.

    Args:
        env_host: The `host` variable name display in environment, or the real host
        env_port: The `port` variable name display in environment, or the real port
        from_env: the flag is used for determining load variables from environment or not
    """
    ROUTE_MAP = {}

    def __init__(
        self,
        env_host: str,
        env_port: Optional[str] = None,
        from_env: bool = True
    ):
        self.log = SystemLogger.get_logger()
        self.env_host = env_host
        self.env_port = env_port
        self.from_env = from_env
        self.host = None
        self.port = None
        self.base_url = self.get_base_url()

    @classmethod
    def route_map_setter(cls, route_map: dict):
        """
        It's an interface for setting route map.
        """
        cls.ROUTE_MAP = route_map

    @classmethod
    def route_map_adder(
        cls,
        route: dict,
        force_update: bool = False
    ) -> dict:
        """
        Add new route into `ROUTE_MAP`
        """
        for k, v in route.items():
            if k in cls.ROUTE_MAP and force_update == False:
                raise ValueError(
                    f"The key: '{k}' has existed in `ROUTE_MAP`, "
                    f"please check it again. If you want to force update it, "
                    f"adjust the flag `force_update` from `False` to True.\n"
                    f"The ROUTE_MAP: {cls.ROUTE_MAP}"
                )
            cls.ROUTE_MAP.update({k: v})
        return cls.ROUTE_MAP

    def get_base_url(self) -> str:
        """
        Get host and port from environment, and combine them to base url.
        """
        # If from_env flag is True, then getting variables from environment
        if self.from_env:
            self.host = os.environ.get(self.env_host)
            if self.env_port:
                self.port = os.environ.get(self.env_port)
        else:
            self.host = self.env_host
            self.port = self.env_port

        if not self.host:
            raise ValueError(
                f"Can't get variable {self.env_host} from environment"
            )
        if self.port:
            url = f"{self.host.rstrip('/')}:{self.port}/"
            self.log.info(f"The base_url: {url}")
        else:
            url = self.host
            self.log.info(f"The port is empty, so the base_url: {url}")
        return url

    def get_page_url(self, page_name: str) -> str:
        if not self.ROUTE_MAP:
            raise ValueError((
                "The attribute: `ROUTE_MAP` "
                "doesn't declare."
            ))
        try:
            page_endpoint: str = self.ROUTE_MAP[page_name]
        except KeyError as e:
            self.log.error(
                f"The {page_name} doesn't exist in ROUTE_MAP.\n "
                f"ROUTE_MAP: {self.ROUTE_MAP}"
            )
            raise e
        if page_endpoint:
            page_url = '/'.join([
                self.base_url.rstrip('/'),
                page_endpoint.lstrip('/')
            ])
        else:
            page_url = self.base_url
        return page_url

    def get_url_without_query(self, url: str) -> str:
        """
        Get url without query parameter
        """
        parse_result = parse.urlparse(url)
        url_without_query = f'{parse_result.scheme}://{parse_result.netloc}{parse_result.path}/'
        return url_without_query

    def is_page_url(self, url: str, page_name: str) -> bool:
        """
        Check input url whether is page url
        """
        url_without_query = self.get_url_without_query(url)
        page_url = self.get_page_url(page_name)
        if url_without_query.rstrip('/') == page_url.rstrip('/'):
            return True
        else:
            self.log.debug(
                f"The url: '{url}' after parsing => '{url_without_query}' doesn't "
                f"equal to page_url: '{page_url}'"
            )
            return False

    def __is_format_url(self, url: str) -> Tuple[bool, Tuple[int]]:
        """
        Check given url whether existing format out

        Args:
            url (str)

        Returns:
            Tuple[bool, Tuple[int]]: (exist_or_not, (start_idx, end_idx))
        """
        curly_brackets_pat = re.compile(r'\{.*\}')
        result = curly_brackets_pat.search(url)
        if result:
            return True, result.span()
        else:
            return False, ()

    def __repr__(self) -> str:
        return (
            f"< args=(env_host={self.host}, "
            f"env_port={self.port}), "
            f"base_url={self.base_url}\n"
            f"ROUTE_MAP={self.ROUTE_MAP}>"
        )
