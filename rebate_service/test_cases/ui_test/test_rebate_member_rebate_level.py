import time

import allure
import pytest
import pytest_check as check
from selenium.webdriver.common.by import By

from utils.utils import TestDataLoader, SystemLogger
from rebate_service.pages.rebate_member_rebate_level import RebateMERebateLevel
from rebate_service.pages.rebate_management import RebateManagement

from bo_shared.bo_service_pages import BoService


log = SystemLogger.get_logger()

def test_upload_files():
    test_data = TestDataLoader()
    return [
        # [test_data.get_file_path('rebate_service', 'template.csv'), False],
        [test_data.get_file_path('rebate_service', 'rebate_level_test_data.xls'), True]
    ]

@pytest.fixture()
def test_memberexclusion_list() -> list:
    memberexclusion_list = [
        'Member Rebate Level', 'Member Blacklist'
    ]
    return memberexclusion_list


@allure.feature('Rebate Service memberExclusion test')
class TestRebateMERebateLevel():

    # Case 1 Import invalidate data (by xls file)
    @allure.title("Case 1 Import invalidate data (by xls file)")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.link('https://hermes.devgambit.net/browse/PSS-259',
                 name='[Member Exclusion][Rebate Level] Test scripts for Manage rebate level')
    @allure.story("Test upload spreadsheet file")
    @pytest.mark.parametrize(
        'test_upload_file, expect_result',
        test_upload_files()
    )
    def test_upload_spreadsheet_file(self, driver, test_account, test_upload_file, expect_result):
        rebate_level = RebateMERebateLevel(driver)
        rebate_level.login(test_account)
        is_available = rebate_level.upload_spreadsheet_file(test_upload_file)
        check.equal(is_available, expect_result)

    # Case 2 Set-up 1st member permission to VIP1~VIP7 (by csv file)
    # Case 3 Set-up all member permission to VIP1~VIP7 (by csv file)
    # Case 4 Set-up 2ed member permission to General (by csv file)
    # Case 5 Set-up all member permission to General (by csv file)

    # Case 6 Manage Rebate Level by select member code
    # 設定 Members 權限為 General
    # @allure.title("Case 6 Manage Rebate Level by select member code")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-259',
    #              name='[Member Exclusion][Rebate Level] Test scripts for Manage rebate level')
    # @allure.story("Test Set-up the MemberCode Permission")
    # def test_setup_the_membercode_permission(self, driver, test_account, test_player):
    #     rebate_level = RebateMERebateLevel(driver)
    #     rebate_level.login(test_account)
    #     rebate_level.hover_batch_manage_button()
    #     rebate_level.set_up_the_member_permission()
    #     time.sleep(1)
    #     rebate_level.input_membercode_for_rebate_level(test_player['membercode'])
    #     time.sleep(1)
    #     rebate_level.setup_the_slot_permission()
    #     rebate_level.setup_the_pt_permission()
    #     rebate_level.setup_the_ak_permission()
    #     rebate_level.click_the_bulk_switch()
    #     rebate_level.mark_all_as_vip()
    #     rebate_level.click_the_bulk_switch()
    #     rebate_level.mark_all_as_general()
    #     rebate_level.click_the_save_btn()
    #     page_name = rebate_level.get_current_page_name()
    #     check.equal(page_name, "Member Rebate Level")
    #     time.sleep(2)

    # # Case 7 Manage Rebate Level by check box (one)
    # # 修改１st member 的權限
    # @allure.title("Case 7 Manage Rebate Level by check box (one)")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-259',
    #              name='[Member Exclusion][Rebate Level] Test scripts for Manage rebate level')
    # @allure.story("Test Modification the 1st MemberCode Permission")
    # def test_modification_1st_member_permission(self, driver, test_account):
    #     rebate_level = RebateMERebateLevel(driver)
    #     rebate_level.login(test_account)
    #     element_member_1st = rebate_level.get_the_1st_membercode_from_exclusion_page()
    #     time.sleep(3)
    #     rebate_level.click_the_edit_btn_for_1st_member()
    #     element_member_1st_mrl = rebate_level.get_the_1st_membercode_from_mrl_page()
    #     # # check.is_in(element_member_1st, element_member_1st_mrl)
    #     # assert element_member_1st == element_member_1st_mrl
    #     rebate_level.click_the_bulk_switch()
    #     rebate_level.mark_all_as_general()
    #     time.sleep(1)
    #     rebate_level.click_the_update_btn()
    #     time.sleep(1)
    #     page_name = rebate_level.get_current_page_name()
    #     check.equal(page_name, "Member Rebate Level")
    #     time.sleep(2)

    # # Case 8 Manage Rebate Level by check box (multiple select)
    # # 批次修改Members 權限
    # @allure.title("Case 8 Manage Rebate Level by check box (multiple select)")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-259',
    #              name='[Member Exclusion][Rebate Level] Test scripts for Manage rebate level')
    # @allure.story("Test Batch Modification the MemberCode Permission")
    # def test_batch_modification_member_permission(self, driver, test_account):
    #     rebate_level = RebateMERebateLevel(driver)
    #     rebate_level.login(test_account)
    #     element_member_o = rebate_level.get_the_membercode1_form_exclusion_page()
    #     rebate_level.select_all_member()
    #     rebate_level.click_the_batch_modify_btn()
    #     time.sleep(1)
    #     element_member_i = rebate_level.get_the_membercode_from_mrl_page()
    #     check.is_in(element_member_i, element_member_o)
    #     rebate_level.click_the_bulk_switch()
    #     rebate_level.mark_all_as_general()
    #     time.sleep(1)
    #     rebate_level.click_the_save_btn()
    #     time.sleep(1)
    #     page_name = rebate_level.get_current_page_name()
    #     check.equal(page_name, "Member Rebate Level")
    #     time.sleep(2)

    # # Case 9 Last Updated > By default, select the past 3 months data
    # @allure.title("Case 9 Last Updated > By default, select the past 3 months data")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-261',
    #              name='[Member Exclusion][Rebate Level][Filter] Test scripts for filter Last Updated')
    # @allure.story("Test the default time sorting range")
    # def test_default_time_range(self, driver, test_account):
    #     rebate_level = RebateMERebateLevel(driver)
    #     rebate_level.login(test_account)
    #     db = rebate_level.get_the_date_str()
    #     cd = rebate_level.get_the_date_end()
    #     # Assert the default Time range
    #     check.equal((cd - db).days, 90)

    # Case 10 Last Updated > Select more than past 1 year
    # Case 11 Last Updated > Select from past 1 year till now

    # Case 12 Last Updated > Select more than maximum 3 months
    # @allure.title("Case 12 Last Updated > Select more than maximum 3 months")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-261',
    #              name='[Member Exclusion][Rebate Level][Filter] Test scripts for filter Last Updated')
    # @allure.story("Test the time range mpre than 3 months")
    # def test_select_more_than_three_months(self, driver, test_account):
    #     rebate_level = RebateMERebateLevel(driver)
    #     rebate_level.login(test_account)
    #     rebate_level.click_the_date_picker()
    #     rebate_level.input_the_date_str()
    #     rebate_level.input_the_date_end()
    #     time.sleep(1)
    #     value_str = rebate_level.driver.find_element(By.XPATH, "(//input[@id='updateAt'])[1]").get_attribute("value")
    #     check.equal(value_str, "2022-02-01")
    #     value_end = rebate_level.driver.find_element(By.XPATH,"(//input[@placeholder='End date'])[1]").get_attribute("value")
    #     check.equal(value_end, "2022-06-01")

    # Case 13 By default, no select (Member Code
    # @allure.title("Case 13 By default, no select (Member Code")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-261',
    #              name='[Member Exclusion][Rebate Level][Filter] Test scripts for filter Last Updated')
    # @allure.story("Test the time range mpre than 3 months")
    # def test_default_membercode(self, driver, test_account):
    #     rebate_level = RebateMERebateLevel(driver)
    #     rebate_level.login(test_account)

    # Case 14 Multiple Select all items
    # Case 15 By default, no select (Last Updated By
    # Case 16 Select Desktop
    # Case 17 Select Mobile
    # Case 18 Multiple Select all items
    # Case 19 Descending by Last Updated By
    # Case 20 Ascending by Last Updated By
    # Case 21 Customize filter
    # Case 22 Pagination
    # Case 23 Export search result

    # Case 24 Assert the page name == current page name
    @allure.title("Case 24 Assert the page name == current page name")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.link('https://hermes.devgambit.net/browse/PSS-259',
                 name='[Member Exclusion][Rebate Level] Test scripts for Manage rebate level')
    @allure.story("Assert the page name equal current page name")
    def test_current_page_name(self, driver, test_account):
        rebate_level = RebateMERebateLevel(driver)
        rebate_level.login(test_account)
        page_name = rebate_level.get_current_page_name()
        check.equal(page_name, "Member Rebate Level")

    # Case 25 Check the menu-list of member exclusion
    # @allure.title("Case 25 Check the menu-list of member exclusion")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-259',
    #              name='[Member Exclusion][Rebate Level] Test scripts for Manage rebate level')
    # @allure.story("Test member exclusion function lists")
    # def test_func_list(self, driver, test_memberexclusion_list, test_account):
    #     test_service_name = 'Member Exclusion'
    #     slash_pat = RebateMERebateLevel.shared_regex['slash_pat']
    #     rebate_level = RebateMERebateLevel(driver)
    #     rebate_level.login(test_account)
    #     rebate_all_list = rebate_level.get_service_list(BoService.Rebate.ServiceName, True)
    #     current_func_list = list()
    #     for rebate_list in rebate_all_list:
    #         if slash_pat.search(rebate_list) is None:
    #             continue
    #         func_title, sub_func = rebate_list.split('/')
    #         if func_title == test_service_name:
    #             current_func_list.append(sub_func)
    #     # Check its length
    #     assert len(current_func_list) == len(test_memberexclusion_list)
    #     # Check all of the contents whether are the same or not
    #     for current, test in zip(current_func_list, test_memberexclusion_list):
    #         assert current == test

    # # Test for Member Blacklist Page
    # @allure.story("Test for Member Blacklist page")
    # def test_for_select_all_member_and_export_file(self, driver, test_account):
    #     rebate_memberexclusion = RebateMERebateLevel(driver)
    #     rebate_memberexclusion.login(test_account)
    #     rebate_memberexclusion.click_functions_list_r()
    #     rebate_memberexclusion.click_functions_list_m()
    #     time.sleep(1)
    #     rebate_memberexclusion.click_the_member_blacklist()
    #     rebate_memberexclusion.select_all_by_member_blacklist()
    #     time.sleep(2)
