import re
import os
import time

import pyperclip

from copy import deepcopy
from typing import Callable, Optional, Any, Set, Union, Tuple
from functools import wraps

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import InvalidArgumentException
from selenium.common.exceptions import InvalidCookieDomainException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains

from utils.envs import Envs
from utils.url_router import URLRouter
from utils.utils import SystemLogger, HelperFuncs
from utils.file_manager import CookiesManagement


class BasePage():

    # Below attributes need to be declared by sub-class
    URL = ""
    PAGE_NAME = ""
    DEPLOYMENT_ENV = os.environ.get("DEPLOYMENT_ENV", "dev")

    shared_regex = {
        'slash_pat': re.compile(r'/'),
        'space_pat': re.compile(r'\s+'),
        'new_line_pat': re.compile(r'\n'),
        'alphabet_pat': re.compile(r'[a-zA-Z]+'),
        'plus_num_pat': re.compile(r'\+[\s0-9\.]+'),
        'brackets_word_pat': re.compile(r'\(\w+\)'),
        'brackets_pat': re.compile(r'[\(|\)]+'),
        'number_pat': re.compile(r'[0-9]+'),
    }

    def __init__(self, driver: WebDriver, url_router: URLRouter, timeout: int = 10):
        """
        Args:
            driver (WebDriver)
            url_router (URLRouter): The URLRouter object
            timeout (int, optional): The `explicit_wait`, Defaults to 10.
        """
        self.driver = driver
        self.timeout = timeout
        self.log = SystemLogger.get_logger()
        # Setting url_router and URL attribute
        self.init_url_router(url_router)
        self.attr_checker()
        if self.DEPLOYMENT_ENV in (Envs.DEV.value, ):
            self.log.info(
                f'The env: "{self.DEPLOYMENT_ENV}", '
                f'page_name: "{self.PAGE_NAME}", '
                f'and URL: "{self.URL}"'
            )

    def attr_checker(self):
        check_attr_names = ['URL', "PAGE_NAME"]
        for attr_name in check_attr_names:
            if not getattr(self, attr_name):
                raise ValueError(
                    f"Need to declare the object attribute: `{attr_name}` "
                    f"in {self.__class__.__name__}"
                )

    def init_url_router(self, url_router: URLRouter):
        """
        Initialize the url router, and set two attributes
        `url_router` and `URL`
        """
        if self.DEPLOYMENT_ENV in (Envs.DEV.value, ):
            self.log.info(f"Initialize the url_router: '{url_router.__name__}'")

        if issubclass(url_router, URLRouter):
            self.url_router: URLRouter = url_router()
            self.URL: str = self.url_router.get_page_url(self.PAGE_NAME)
        else:
            raise ValueError((
                "The `url_router` must be the subclass of `URLRouter`, "
                f"so initialize the {self.__class__.__name__} failed."
            ))

    def router_resetter(self, url_router: URLRouter):
        """
        Re-initialize the router again. It's used for reset router.
        """
        self.log.info(
            f"Re-initialize the router. "
            f"The new used url_router is '{url_router.__name__}' "
        )
        self.init_url_router(url_router)

    @property
    def action_chains(self):
        self.log.info("Initialize ActionChains object")
        return ActionChains(self.driver)

    def visit_page(self):
        """
        Visit the page
        """
        self.driver.get(self.URL)
        self.log.info(f"Visit {self.PAGE_NAME} page")

    def find_element_wait(self, method: Callable, *locator) -> Any:
        """
        Wait for specific condition is finished, and its response depend
        on input method and arguments
        """
        if not callable(method):
            raise ValueError(f"ec_method: {method.__name__} isn't callable.")
        try:
            res = WebDriverWait(self.driver, self.timeout).until(method(locator))
        except Exception as e:
            raise e
        return res

    def get_element_all_attrs_by_locator(self, method: Callable, *locator) -> dict:
        """
        Get attributes of specific web element
        """
        element = self.find_element_wait(method, *locator)
        return self.get_element_all_attrs(element)

    def get_element_all_attrs(self, element: WebElement) -> dict:
        """
        Get attributes of input web element
        """
        if type(element).__name__ != WebElement.__name__:
            raise TypeError(
                "find_element_wait method response not WebElement, "
                "so it can't get attributes.\n"
                f"find_element_wait response data: {element}, and"
                f"its type: {type(element)}"
            )
        get_attrs_js = (
            """
            var items = {};
            for (index = 0; index < arguments[0].attributes.length; ++index){
                items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value
            };
            return items;
            """
        )
        element_attrs = self.execute_script(get_attrs_js, element)
        return element_attrs

    def execute_script(self, javascript: str, *args):
        return self.driver.execute_script(javascript, *args)

    def scroll_to(self, x: int, y: int, locator: Union[tuple, list] = None):
        """
        Scroll to a location on the page.
        Args:
            x: The number of pixels to scroll horizontally.
            y: The number of pixels to scroll vertically.
            locator: The element locator, if it isn't None, it will use this element to do scroll.
        """
        if locator:
            element = self.find_element_wait(
                EC.presence_of_element_located,
                *locator
            )
            if type(element).__name__ != WebElement.__name__:
                raise TypeError(
                    "find_element_wait method response not WebElement, "
                    "so it can't get attributes.\n"
                    f"find_element_wait response data: {element}, and"
                    f"its type: {type(element)}"
                )
            self.log.info(
                f"Use the WebElement where is located on {locator}, and "
                f"scroll to ({x}, {y})"
            )
            time.sleep(1)
            self.execute_script(
                'arguments[0].scrollTo(arguments[1], arguments[2])',
                element, x, y
            )
        else:
            self.log.info(f"Scroll window to ({x}, {y})")
            self.execute_script('window.scrollTo(arguments[0], arguments[1]);', x, y)

    def get_doc_body_client_size(self) -> dict:
        """
        Get document body size
        """
        js_commands = {
            'height': 'return document.body.clientHeight',
            'width': 'return document.body.clientWidth'
        }
        size = {k: self.execute_script(command) for k, command in js_commands.items()}
        return size

    def get_scroll_size(self):
        js_commands = {
            'height': 'return document.body.scrollHeight',
            'width': 'return document.body.scrollWidth'
        }
        size = {k: self.execute_script(command) for k, command in js_commands.items()}
        return size

    @staticmethod
    def xpath_editor(xpath: str, **kwargs) -> str:
        """
        Edit the xpath for selecting the specific element
        """
        copy_kwargs = deepcopy(kwargs)
        idx_format = "[{}]"
        for k, v in copy_kwargs.items():
            # Check whether the type of value is integer or not
            if v is not None:
                if not isinstance(v, int):
                    raise TypeError(
                        f"The type of value must be integer. "
                        f"The '{k}': {v} doesn't conform the type restriction."
                    )
            # If value is None or 0, then convert to empty string
            if v is None or v == 0:
                v = ""
            if v:
                v = idx_format.format(str(v))
            kwargs[k] = v

        del copy_kwargs
        xpath = xpath.format(**kwargs)
        return xpath

    @staticmethod
    def get_clipboard_text() -> str:
        """
        Paste clipboard text
        """
        copy_string = pyperclip.paste()
        return copy_string

    @staticmethod
    def join_xpath(*args) -> str:
        return ''.join(args)

    @classmethod
    def normalize_string(cls, string: str) -> str:
        space_pat = cls.shared_regex['space_pat']
        return space_pat.sub('_', string.strip().lower())

    @classmethod
    def normalize_attribute_value(cls, attr: str) -> str:
        space_pat = cls.shared_regex['space_pat']
        return space_pat.sub('', attr)

    @staticmethod
    def drop_brackets_words(string: str) -> str:
        pat = re.compile(r'\(.+\)+')
        result = pat.search(string)
        if result:
            return string[:result.start()]
        else:
            return string

    def is_page_url(
        self,
        url: str,
        page_name: Optional[str] = None
    ) -> bool:
        """
        Invoke URLRouter.is_page_url method, let it to more easily use.
        """
        if page_name is None:
            page_name = self.PAGE_NAME
        return self.url_router.is_page_url(url, page_name)

    # Custom Excepted condition function
    def ec_is_page_url(self) -> Callable:
        """
        Wrap self.is_page_url for applying WebDriverWait.until invoked.
        Checking driver.current_url is page_url or not
        """
        def _predicate(driver: WebDriver):
            return self.is_page_url(driver.current_url)
        return _predicate

    def wait_for_is_page_url(self) -> bool:
        """
        Wait for directing to specific page url
        """
        try:
            result = WebDriverWait(self.driver, self.timeout).until(
                self.ec_is_page_url()
            )
        except TimeoutException as e:
            excepted_url = self.url_router.get_page_url(self.PAGE_NAME)
            self.log.error(
                f"Can't direct to {self.PAGE_NAME} page, "
                f"expected_url: {excepted_url}, "
                f"current_url: {self.driver.current_url}"
            )
            raise e
        except Exception as e:
            self.log.error("Occur unexpected error")
            raise e
        return result

    @staticmethod
    def _is_expected_element_attr_value(
        driver: WebDriver,
        locator: tuple,
        attr: str,
        expected_values: Union[set, tuple, list, Any]
    ) -> bool:
        """
        Check the attribute of element equal to expected value or not

        Args:
            driver (WebDriver)
            locator (tuple)
            attr (str): the html tag attribute name
            expected_values (Union[tuple, list, str])

        Returns:
            bool: bool: The attribute of the element is expected value or not
        """
        element = driver.find_element(*locator)
        attr_value = element.get_attribute(attr)

        is_expected = True
        if isinstance(expected_values, (set, tuple, list)):
            if  attr_value not in expected_values:
                is_expected = False
        else:
            if attr_value != expected_values:
                is_expected = False
        return is_expected

    def is_expected_element_attr_value(
        self,
        locator: tuple,
        attr: str,
        expected_values: Union[set, tuple, list, Any]
    ):
        """
        Invoke `self._is_expected_element_attr_value`
        """
        return self._is_expected_element_attr_value(
            self.driver,
            locator,
            attr,
            expected_values
        )

    # Custom Excepted condition function
    def ec_is_expect_element_attr_value(
        self,
        locator: tuple,
        attr: str,
        expected_values: Union[set, tuple, list, Any]
    ) -> Callable:
        """
        Wrap self._is_expected_element_attr_value for applying WebDriverWait.until invoked.
        Check the attribute of element equal to expected value or not
        """
        def _predicate(driver: WebDriver):
            return self._is_expected_element_attr_value(driver, locator, attr, expected_values)
        return _predicate

    def wait_for_is_expect_element_attr_value(
        self,
        locator: tuple,
        attr: str,
        expected_values: Union[set, tuple, list, Any]
    ) -> bool:
        """
        Wait for specific the attribute of element is expected value
        """
        try:
            result = WebDriverWait(self.driver, self.timeout).until(
                self.ec_is_expect_element_attr_value(locator, attr, expected_values)
            )
        except TimeoutException as e:
            self.log.error(
                "The attribute of element doesn't equal to excepted value. "
                f"current_page: {self.PAGE_NAME}, locator: {locator}, "
                f"element_attr: {attr}, expected_value: {expected_values}"
            )
            raise e
        except Exception as e:
            self.log.error(
                "Occur unexpected errors case, the attribute of element doesn't "
                "equal to excepted value. "
                f"current_page: {self.PAGE_NAME}, locator: {locator}, "
                f"element_attr: {attr}, expected_value: {expected_values}"
            )
            raise e
        return result

    def scroll_down_percent(
        self,
        percent: float,
        locator: Union[tuple, list] = None
    ):
        # Scroll screen to down
        size_info = self.get_scroll_size()
        move_y_axis = size_info['height'] * percent
        self.scroll_to(0, move_y_axis, locator)

    @staticmethod
    def to_boolean(value: str) -> bool:
        if value == 'true':
            return True
        elif value == 'false':
            return False
        else:
            raise ValueError(f'Unknown {value} value to convert boolean')

    @staticmethod
    def style_attrs_processor(style_attrs: str) -> dict:
        style_attrs = style_attrs.rstrip(';').split(';')
        style_dict = dict()
        for style_attr in style_attrs:
            style_type, value = style_attr.split(':')
            style_dict[style_type.strip()] = value.strip()
        return style_dict

    @staticmethod
    def split_attr(str_: str, split_pat: str = r'\s+') -> list:
        split_pat = re.compile(split_pat)
        split_result = split_pat.split(str_)
        result = [r.strip() for r in split_result]
        return result

    def reload(self):
        """
        Reload the page
        """
        self.log.debug(
            f"Reload the page_name: {self.PAGE_NAME}, "
            f"url: {self.driver.current_url}"
        )
        return self.driver.refresh()

    def clear_input_text(
        self,
        element: Optional[WebElement] = None,
        locator: Optional[Tuple] = None
    ):
        """
        Clear the input text

        Args:
            element (Optional[WebElement], optional): Defaults to None.
            locator (Optional[Tuple], optional): Defaults to None.
        """
        if self.driver.caps['platformName'] in ('mac os x',):
            key_ = Keys.COMMAND
        else:
            key_ = Keys.CONTROL

        if element is None and locator is None:
            raise ValueError(
                "The element and locator can be None at the same time."
            )
        if element is None:
            element: WebElement = self.find_element_wait(
                EC.presence_of_element_located,
                *locator
            )
        element.send_keys(key_, "a")
        element.send_keys(Keys.BACKSPACE)
        if locator:
            self.log.debug(f"Clear the element text, and the locator: {locator}")


def bypass_login(
    login_page_name: str = 'login',
    no_bypass_names: Optional[Set[str]] = None,
    cookies_path: Optional[str] = None,
    addition_info_key: Optional[str] = None
) -> Callable:
    """
    The function is used for decorating the login function and adding the bypass feature.
    Note that the decorated module needs to define `PAGE_NAME` and `url_route`,
    and the attribute `ROUTE_MAP` of the `url_route` must have the login page endpoint.

    Args:
        login_page_name (str, optional): Defaults to 'login'.
        no_bypass_names (Optional[Set[str]], optional): Define pages which don't need to bypass login. Defaults to None.
        cookies_path (Optional[str], optional): The cookies file path which is used for bypass login. Defaults to None.
        addition_info_key (Optional[str], optional): The key that you want to use to extract to the data from args and kwargs,
        then save it with cookies. Defaults to None.
    """
    if no_bypass_names is not None:
        if not isinstance(no_bypass_names, set):
            raise TypeError((
                f"The data type of no_bypass_names "
                f"can be None or `set`, "
                f"but it can't be {type(no_bypass_names)}"
            ))
    no_bypass_names = no_bypass_names or {'login'}
    no_bypass_names.add(login_page_name)

    def decorate_login(login_func: Callable) -> Callable:
        @wraps(login_func)
        def wrapper_login(self: BasePage, *args, **kwargs):
            if not hasattr(self, 'url_router'):
                raise AttributeError((
                    f"The {self.__class__.__name__} module doesn't have "
                    f"`url_router` attribute"
                ))
            # If cookies_path is None, it will hash args as file name
            # It must use nonlocal to refer the outer argument,
            # and it must copy the argument to save original value
            nonlocal cookies_path
            proxy_cookies_path = deepcopy(cookies_path)
            if proxy_cookies_path is None:
                login_args = {
                    'args': args,
                    'kwargs': kwargs,
                }
                hash_args = HelperFuncs.hash_dict(login_args)
                proxy_cookies_path = os.path.join(
                    CookiesManagement.DIR,
                    f'{hash_args}.{CookiesManagement.FILE_EXT}'
                )
            # Get addition info from args or kwargs
            addition_info = None
            if addition_info_key is not None:
                addition_info = HelperFuncs.get_data_from_args(addition_info_key, args, kwargs)

            use_bypass = False
            record_cookies = True
            if self.PAGE_NAME in no_bypass_names:
                # The current page doesn't need bypass-login
                login_func(self, *args, **kwargs)
                self.log.info((
                    f"The page name of the module {self.__class__.__name__} "
                    f"is {self.PAGE_NAME}, it's the same as the parameter "
                    f"no_bypass_names: {no_bypass_names} of the function bypass_login, "
                    f"so it don't use bypass-login."
                ))
            else:
                cookies_management = CookiesManagement()
                cookies = cookies_management.load_cookies(proxy_cookies_path)
                page_url = self.url_router.get_page_url(self.PAGE_NAME)
                if cookies:
                    self.log.info("Get cookies and prepare using bypass login")
                    # Use bypass-login
                    # Need to visit the page that the domain needs to be the same as the cookies.
                    self.driver.get(self.url_router.base_url)
                    try:
                        self.driver.add_cookie(cookies)
                    except (InvalidArgumentException, InvalidCookieDomainException):
                        # login again and need to save cookies
                        self.log.error(
                            "The cookies doesn't conform the web needed, "
                            "so it needs to login and record cookie again. "
                            f"The page_url: {page_url}, module: {self.__class__.__name__},\n"
                            f"Invalid cookie: {cookies}"
                        )
                        login_func(self, *args, **kwargs)
                    else:
                        # Use bypass login
                        use_bypass = True
                        self.log.debug(f"Using cookies:\n {cookies}")
                        self.driver.get(page_url)
                        # Get login url for checking whether bypass-login success
                        # If not, then login in regular way.
                        login_url = self.url_router.get_page_url(login_page_name)

                    time.sleep(1)
                    load_page_success = False
                    if self.is_login_page_loading_success():
                        load_page_success = True

                    # Check current url whether equal to expected url
                    if  load_page_success and self.is_page_url(self.driver.current_url):
                        if use_bypass:
                            # bypass-login success, so it don't need to write or update cookies.
                            record_cookies = False
                            self.log.info((
                                f"Successfully use bypass-login to {self.PAGE_NAME} page, "
                                f"and page_url: {page_url}, module: {self.__class__.__name__}"
                            ))
                    else:
                        self.log.info((
                            f"login_url: {login_url}, expected_url: {page_url}, and "
                            f"current_url: {self.driver.current_url}. "
                            f"The token is expired, or another reason that it can't be used. "
                            f"failed_cookies: {cookies}, page_url: {page_url}, "
                            f"module: {self.__class__.__name__}"
                        ))
                        self.log.info("Delete the current cookies, and login again to get a new one then save it.")
                        self.driver.delete_cookie('token')
                        # login again and need to save cookies
                        login_func(self, *args, **kwargs)
                else:
                    # The cookies doesn't exits, so login again and save new cookies
                    login_func(self, *args, **kwargs)
                    self.log.info((
                        f"The cookies doesn't exist, so using login page in a regular way. "
                        f"The page_url: {page_url}, module: {self.__class__.__name__}"
                    ))
            # Save cookies on the specific path
            if record_cookies:
                cookies = self.driver.get_cookie('token')
                try:
                    save_result = cookies_management.save_cookies(
                        cookies,
                        proxy_cookies_path,
                        addition_info
                    )
                except Exception as e:
                    self.log.error(e)
                    save_result = False
                if save_result:
                    self.log.info(f"Successfully save cookies.")
                else:
                    self.log.error((
                        "SaveCookiesFailed: Occur unexpected error "
                        "when saving the cookies."
                    ))
        return wrapper_login
    return decorate_login
