import os
import time
import allure

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from bo_shared.bo_shared_page import BoSharedPage
from bo_shared.bo_service_pages import BoService


class RebateLogIn(BoSharedPage):

    PAGE_NAME = BoService.Shared.LogIn

    def __init__(self, driver: WebDriver):
        super(RebateLogIn, self).__init__(driver)
