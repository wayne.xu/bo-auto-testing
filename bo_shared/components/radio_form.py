import allure

from typing import List

from bo_shared.components.basic import BasicComponent

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC


RADIO_FORM_INFO = {
    'release_method': {
        'form_data_test_id': 'releaseMethod',
        'options_data_test_id': 'releaseMethod',
    },
    'eligible_members': {
        'form_data_test_id': 'eligibleMember,type',
        'options_data_test_id': 'memberType',
    },
    'platform_rule': {
        'form_data_test_id': 'platformRule',
        'options_data_test_id': 'platformRule',
    }
}


class RadioForm(BasicComponent):

    check_form_hidden_xpath_format = (
        '(//div[@data-testid="{form_data_test_id}"])[1]'
    )
    label_xpath_format = (
        '//div[@data-testid="{form_data_test_id}"]//'
        'div[@class="ant-col ant-form-item-label"]/label'
    )
    options_xpath_format = (
        '//div[@data-testid="{form_data_test_id}"]'
        '/div[@class="ant-col ant-form-item-control"]'
        '//div[@data-testid="{options_data_test_id}"]/label'
    )
    check_selectable_attr = 'ant-radio-wrapper-checked'

    def __init__(
        self,
        driver: WebDriver,
        form_name: str,
        form_data_test_id: str,
        options_data_test_id: str
    ):
        super(RadioForm, self).__init__(driver)
        self.form_name = form_name
        self.form_data_test_id = form_data_test_id
        self.options_data_test_id = options_data_test_id
        # Format the xpath
        self.check_form_hidden_xpath = self.check_form_hidden_xpath_format.format(
            form_data_test_id=self.form_data_test_id
        )
        self.label_xpath = self.label_xpath_format.format(
            form_data_test_id=self.form_data_test_id
        )
        self.options_xpath = self.options_xpath_format.format(
            form_data_test_id=self.form_data_test_id,
            options_data_test_id=self.options_data_test_id
        )

    def is_form_hidden(self) -> bool:
        """
        Check the form which is hidden or not
        """
        check_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            self.check_form_hidden_xpath
        )
        attrs = check_element.get_attribute('class').strip(' ')
        is_hidden = True
        if 'hidden' not in attrs:
            is_hidden = False
        return is_hidden

    def get_form_options(self) -> dict:
        """
        Get the form options
        """
        option_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            self.options_xpath
        )
        all_options_info = dict()
        for option_element in option_elements:
            option_name = self.normalize_string(option_element.text)
            attrs = option_element.get_attribute('class').split(' ')
            input_element: WebElement = option_element.find_element(By.XPATH, 'span/input')
            data_test_id = input_element.get_attribute('data-testid')
            # Check the option is selectable or not
            is_selectable = True
            if self.check_selectable_attr in attrs:
                is_selectable = False

            all_options_info[option_name] = {
                'is_selectable': is_selectable,
                'element': input_element,
                'data_test_id': data_test_id,
            }
        return all_options_info

    def get_form_label(self) -> str:
        label_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            self.label_xpath
        )
        return label_element.text.strip()

    def get_default_options(self, return_info: bool = False) -> list:
        """
        Get the form default options

        Args:
            return_info (bool, optional): Defaults to False.
        """
        default_options = list()
        options_info = self.get_form_options()
        for _name, info in options_info.items():
            if info['is_selectable'] == False:
                if return_info:
                    default_options.append((_name, info))
                else:
                    default_options.append(_name)
        return options_info

    @allure.step("Select the radio from option")
    def select_option(self, option_name: str):
        """
        Select the given single option

        Args:
            option_name (str)
        """
        options_info = self.get_form_options()
        for _name, info in options_info.items():
            if _name != option_name.lower():
                continue

            if info['is_selectable']:
                self.action_chains.click(info['element']).perform()
                self.log.debug(
                    f"Select the {option_name} on "
                    f"{self.form_name} RadioForm"
                )
                return None

    def __repr__(self):
        return (
            f"< {self.form_name} RadioForm Component "
            f"label_xpath: '{self.label_xpath}', "
            f"options_xpath: '{self.options_xpath}' >"
        )
