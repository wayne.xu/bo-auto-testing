import os
import json

from typing import Union

from cryptography.fernet import Fernet


class Encryption():
    """
    The class is used for encrypting and decrypting data.
    Note that the variable `encrypt_key` need to set as environment variable or
    as input argument for class.

    Example:
        file_path = 'path_to_file'
        data = Encryption.load_json(file_path)
        encryption = Encryption()
        encrypted_data = encryption.encrypt_data(data)
        decrypted_data = encryption.decrypt_data(encrypted_data)
        print(
            f'Data before encrypting: {data}, data_type: {type(data)}',
            f'Encrypted data: {encrypted_data}, data_type: {type(encrypted_data)}'
            f'Decrypted data: {decrypted_data}, data_type: {type(decrypted_data)}',
            f'data_type: {type(Encryption.str_to_dict(decrypted_data))}'
            sep='\n'
        )
    """
    def __init__(self, encrypt_key=None):
        if not encrypt_key:
            encrypt_key = os.environ.get('ENCRYPT_KEY', None)

        if not encrypt_key:
            raise ValueError("Can't get variable `ENCRYPT_KEY` from environment variables")

        self.encrypt_key = encrypt_key
        self.f = Fernet(self.encrypt_key)

    @staticmethod
    def load_json(
            file_path: str,
            serialize: bool=True,
            *args,
            **kwargs
        ) -> Union[str, dict]:
        """
        Load json file from specific path.
        """
        with open(file_path, 'r') as rf:
            config = json.load(rf, *args, **kwargs)
        if serialize:
            config = json.dumps(config, ensure_ascii=True)
        return config

    @staticmethod
    def str_to_dict(string: str) -> Union[list, dict]:
        """
        Convert str to dict
        """
        _dict = json.loads(string, object_hook=dict)
        return _dict

    def encrypt_data(self, data: str, encoding: str='utf-8') -> str:
        """
        Encrypt data into meaningless data

        `data`: The data is used to encrypting
        `encoding`: Which encoding is used to converting str to bytes.
        """
        bytes_data = bytes(data, encoding=encoding)
        encrypted_data = self.f.encrypt(bytes_data)
        encrypted_data = encrypted_data.decode(encoding=encoding)
        return encrypted_data

    def decrypt_data(self, data: str, encoding: str='utf-8') -> str:
        """
        Decrypt data from meaningless to meaningful.

        `data`: The data is used to decrypting.
        `encoding`: Which encoding is used to converting str to bytes.
        """
        bytes_data = bytes(data, encoding=encoding)
        decrypted_data = self.f.decrypt(bytes_data)
        decrypted_data = decrypted_data.decode(encoding)
        return decrypted_data
