import time
import allure
import pytest_check
import pytest_check as check
import pytest

from bo_shared.bo_service_pages import BoService
from role_service.pages.role_management import RoleManagement
from utils.envs import Envs
from utils.decorators import execute_env


@allure.feature('Role Service Role test')
class TestRoleRole():

    # - **Use case Role page**
    #     - [ ]  **Use case8 - Create Team Role**
    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("case8 - Create Team Role")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-359', name="[Role][Role Page] Use Case 8 - Create Team Role")
    @allure.story("Create Team Role")
    def test_for_create_team_role(self, driver, test_account, test_role_data, test_role_breadcrumb):
        role = RoleManagement(driver)
        role.login(test_account)
        role.add_team_role()
        breadcrumb = role.get_current_breadcrumb_lv4()
        check.is_in(test_role_breadcrumb['add_role'], breadcrumb)
        random_role_item = role.create_random_role('case8')
        role.select_parent_team_for_create()
        role.select_all_role_policies()
        role.create_team_role()
        time.sleep(1)
        role.cancel_create_team_role()
        role.create_team_role()
        time.sleep(1)
        role.confirm_create_team_role()
        time.sleep(2)
        role.search_role_by_random_name_file('case8')
        time.sleep(1)
        role.click_search_btn()
        first_role_name = role.get_1st_role_name()
        check.equal(first_role_name, random_role_item)
        time.sleep(1)
        first_role_parent_team = role.get_1st_parent_team_list()
        check.equal(first_role_parent_team, test_role_data['first_parent_role'])
        role.click_the_1st_role_detail()
        time.sleep(1)
        role_detail_name = role.get_the_role_name_in_detail_page()
        check.equal(role_detail_name, random_role_item)
        time.sleep(1)
        parent_team_in_detail = role.get_the_parent_in_detail_page()
        check.equal(parent_team_in_detail, test_role_data['first_parent_role'])
        role.delete_first_teamrole()

    #     - [ ]  **Use case9 - Un-assign/Assign Users by Manually**
    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("case9 - Un-assign/Assign Users by Manually")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-360', name="[Role][Role Page] Use Case 9 - Manually Unassign/Assign Users")
    @allure.story("Assign Users by Manually")
    def test_for_manually_assign_user(self, driver, test_account, test_role_data):
        role = RoleManagement(driver)
        role.login(test_account)
        role.create_new_teamrole('case9')
        role.search_role_by_random_name_file('case9')
        time.sleep(2)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role.click_the_un_assign_by_role()
        time.sleep(1)
        role.manually_un_assign_by_role()
        time.sleep(1)
        role.input_username_to_assign(test_role_data['user_name'])
        time.sleep(1)
        role.select_the_checkbox_1st_assign_user()
        time.sleep(1)
        role.assign_user()
        time.sleep(1)
        role.apply_assign_user()
        time.sleep(3)
        assignee_user = role.get_the_assign_user_in_detail_page()
        time.sleep(1)
        check.is_in(test_role_data['user_0_email'], assignee_user)
        time.sleep(5)

    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("case9 - Un-assign/Assign Users by Manually")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-360', name="[Role][Role Page] Use Case 9 - Manually Unassign/Assign Users")
    @allure.story("Un-assign Users by Manually")
    def test_for_manually_un_assign_user(self, driver, test_account, test_role_data):
        role = RoleManagement(driver)
        role.login(test_account)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role.click_the_un_assign_by_role()
        time.sleep(1)
        role.manually_un_assign_by_role()
        time.sleep(1)
        role.input_username_to_un_assign(test_role_data['user_name'])
        time.sleep(1)
        role.select_the_checkbox_1st_un_assign_user()
        time.sleep(1)
        role.un_assign_user()
        time.sleep(1)
        role.apply_assign_user()
        time.sleep(1)
        role.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.RoleManagement
        )
        time.sleep(1)
        role.search_role_by_random_name_file('case9')
        time.sleep(2)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        un_assign_user = role.get_count_of_total_asgn_user()
        time.sleep(1)
        check.equal(un_assign_user, True)
        time.sleep(1)
        role.delete_first_teamrole()

    # @allure.epic("case9 - Un-assign/Assign Users by Manually")
    # @allure.severity(allure.severity_level.BLOCKER)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-360', name="[Role][Role Page] Use Case 9 - Manually Unassign/Assign Users")
    # @allure.story("Un-assign Users by Manually")
    # def test_for_manually_un_assign_user(self, driver, test_account, test_role_data):
    #     role = RoleManagement(driver)
    #     role.login(test_account)
    #     role.create_new_teamrole()
    #     role.click_the_1st_role_detail()
    #     time.sleep(1)
    #     role.click_the_un_assign_by_role()
    #     time.sleep(1)
    #     role.manually_un_assign_by_role()
    #     time.sleep(1)
    #     role.input_username_to_un_assign(test_role_data['user_name'])
    #     time.sleep(1)
    #     role.select_the_checkbox_1st_un_assign_user()
    #     time.sleep(1)
    #     role.un_assign_user()
    #     time.sleep(1)
    #     role.apply_assign_user()
    #     time.sleep(3)
    #     un_assign_user = role.get_the_assign_user_in_detail_page()
    #     check.is_not_in(test_role_data['user_0_email'], un_assign_user)
    #     time.sleep(5)
    #     role.delete_first_teamrole()

    # #     - [ ]  **Use case10 - Un-assign Users By BatchInput
    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("case10 - Un-assign/Assign Users by BatchInput")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-705', name="[Role] Main Function Case No. 41: Batch Assign Users")
    @allure.story("Un-assign Users by BatchInput")
    def test_for_batch_assign_user(self, driver, test_account, test_role_data):
        role = RoleManagement(driver)
        role.login(test_account)
        role.create_new_teamrole('case10')
        role.search_role_by_random_name_file('case10')
        time.sleep(1)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role.click_the_un_assign_by_role()
        time.sleep(1)
        role.batch_assign_by_role()
        time.sleep(1)
        role.input_email_to_batch_assign(test_role_data['user_1_email'], test_role_data['user_2_email'])
        time.sleep(1)
        role.submit_batch_assign()
        time.sleep(3)
        batch_assign_user = role.get_the_assign_user_in_detail_page()
        time.sleep(1)
        check.is_in(test_role_data['user_1_email'], batch_assign_user)
        check.is_in(test_role_data['user_2_email'], batch_assign_user)
        time.sleep(2)
        role.click_the_un_assign_by_role()
        time.sleep(1)
        role.manually_un_assign_by_role()
        time.sleep(1)
        role.select_all_users_to_un_assign()
        time.sleep(1)
        role.un_assign_user()
        time.sleep(1)
        role.apply_assign_user()
        time.sleep(2)
        role.delete_first_teamrole()


    # #     - [ ]  **Use case11 - SET-UP the Role’s Policies**
    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("case11 - SET-UP the Role’s Policies")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-362', name="[Role][Role Page] Use Case 11 - Set up Role's policies")
    @allure.story("SET-UP the Role’s Policies")
    def test_for_setup_role_policies(self, driver, test_account, test_role_data):
        role = RoleManagement(driver)
        role.login(test_account)
        role.create_new_teamrole('case11')
        role.search_role_by_random_name_file('case11')
        time.sleep(1)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role.edit_role()
        time.sleep(1)
        role.cancel_edit_role()
        time.sleep(1)
        role.edit_role()
        time.sleep(1)
        role.confirm_edit_role()
        time.sleep(1)
        breadcrumb = role.get_current_breadcrumb_lv4()
        time.sleep(1)
        random_role_item = role.get_random_role_name_by_file('case11')
        check.equal(random_role_item, breadcrumb)
        time.sleep(1)
        role.modify_role_name(test_role_data['modify_role_name'])
        time.sleep(2)
        role.modify_parent_team()
        time.sleep(2)
        role.update_team_role()
        time.sleep(2)
        role.search_role_by_modify_name(test_role_data['modify_role_name'])
        time.sleep(1)
        role.click_search_btn()
        time.sleep(1)
        first_role_name = role.get_1st_role_name()
        check.equal(first_role_name, test_role_data['modify_role_name'])
        time.sleep(1)
        first_role_parent_team = role.get_1st_parent_team_list()
        check.equal(first_role_parent_team, test_role_data['first_parent_role'])
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role_detail_name = role.get_the_role_name_in_detail_page()
        check.equal(role_detail_name, test_role_data['modify_role_name'])
        time.sleep(1)
        parent_team_in_detail = role.get_the_parent_in_detail_page()
        check.equal(parent_team_in_detail, test_role_data['first_parent_role'])
        time.sleep(1)
        role.delete_first_teamrole()

    # #     - [ ]  **Use case12 - Duplicate Role**
    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("Use case12 - Duplicate Role")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-363', name="[Role][Role Page] Use Case 12 - Duplicate Role")
    @allure.story("Duplicate Role")
    def test_for_duplicate_role(self, driver, test_account, test_role_data):
        role = RoleManagement(driver)
        # share = RoleSharedPage(driver)
        role.login(test_account)
        role.create_new_teamrole('case12')
        role.search_role_by_random_name_file('case12')
        time.sleep(1)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role.click_the_action_btn_in_detail()
        time.sleep(1)
        role.select_duplicate()
        time.sleep(1)
        role.input_duplicate_item_name(test_role_data['duplicate_role_name'])
        time.sleep(1)
        role.select_parent_team()
        time.sleep(1)
        role.select_all_role_policies()
        time.sleep(3)
        role.reselect_role_policies()
        time.sleep(1)
        role.create_team_role()
        time.sleep(1)
        role.confirm_create_team_role()
        time.sleep(1)
        role.search_role_by_duplicate_name(test_role_data['duplicate_role_name'])
        time.sleep(1)
        role.click_search_btn()
        first_role_name = role.get_1st_role_name()
        check.equal(first_role_name, test_role_data['duplicate_role_name'])
        time.sleep(1)
        first_role_parent_team = role.get_1st_parent_team_list()
        check.equal(first_role_parent_team, test_role_data['first_parent_role'])
        role.click_the_1st_role_detail()
        time.sleep(1)
        role_detail_name = role.get_the_role_name_in_detail_page()
        check.equal(role_detail_name, test_role_data['duplicate_role_name'])
        time.sleep(1)
        time.sleep(1)
        parent_team_in_detail = role.get_the_parent_in_detail_page()
        check.equal(parent_team_in_detail, test_role_data['first_parent_role'])
        role.delete_first_teamrole()
        time.sleep(1)
        role.search_role_by_random_name_file('case12')
        time.sleep(1)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role.delete_first_teamrole()
        time.sleep(1)

    # #     - [ ]  **Use case13 - Activate/Deactivate Role**
    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("Use case13 - Activate/Deactivate Role")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-364', name="[Role][Role Page] Use Case 13 - Activate/Deactivate Role")
    @allure.story("Deactivate Role")
    def test_for_deactivate_role(self, driver, test_account, test_role_data):
        role = RoleManagement(driver)
        role.login(test_account)
        role.create_new_teamrole('case13')
        role.search_role_by_random_name_file('case13')
        time.sleep(1)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role.click_the_action_btn_in_detail()
        time.sleep(1)
        role.select_deactivate()
        time.sleep(1)
        role.select_cancel_for_popup()
        time.sleep(1)
        role.click_the_action_btn_in_detail()
        time.sleep(1)
        role.select_deactivate()
        time.sleep(1)
        role.select_confirm_deactivate()
        time.sleep(1)
        detail_role_status = role.get_1st_status_in_detail_page()
        check.equal(detail_role_status, test_role_data['status_eq_deactivate'])
        time.sleep(2)
        role.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.RoleManagement
        )
        time.sleep(1)
        first_role_status = role.get_1st_role_status_list()
        check.equal(first_role_status, False)
        time.sleep(1)
        role.search_role_by_random_name_file('case13')
        time.sleep(1)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role.delete_first_teamrole()

    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("Use case13 - Activate/Deactivate Role")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-364', name="[Role][Role Page] Use Case 13 - Activate/Deactivate Role")
    @allure.story("Activate Role")
    def test_for_activate_role(self, driver, test_account, test_role_data):
        role = RoleManagement(driver)
        role.login(test_account)
        role.create_new_teamrole('case14')
        role.search_role_by_random_name_file('case14')
        time.sleep(1)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        # call SharePage page func.
        role.click_the_action_btn_in_detail()
        time.sleep(1)
        role.select_deactivate()
        time.sleep(1)
        role.select_confirm_deactivate()
        time.sleep(3)
        role.click_the_action_btn_in_detail()
        time.sleep(2)
        role.select_activate()
        time.sleep(1)
        detail_role_status = role.get_1st_status_in_detail_page()
        check.equal(detail_role_status, test_role_data['status_eq_activate'])
        time.sleep(2)
        role.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.RoleManagement
        )
        time.sleep(1)
        first_role_status = role.get_1st_role_status_list()
        check.equal(first_role_status, True)
        time.sleep(1)
        role.search_role_by_random_name_file('case14')
        time.sleep(1)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role.delete_first_teamrole()

    # #     - [ ]  **Use case14 - Delete Role**
    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("Use case14 - Delete Role")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-365', name="[Role][Role Page] Use Case 14 - Delete Role")
    @allure.story("Delete Role")
    def test_for_delete_role(self, driver, test_account, test_role_data):
        role = RoleManagement(driver)
        role.login(test_account)
        role.create_new_teamrole('case15')
        role.search_role_by_random_name_file('case15')
        time.sleep(1)
        role.click_search_btn()
        time.sleep(1)
        role.click_the_1st_role_detail()
        time.sleep(1)
        role.click_the_action_btn_in_detail()
        role.select_delete()
        time.sleep(1)
        role.select_cancel_for_popup()
        time.sleep(2)
        role.click_the_action_btn_in_detail()
        time.sleep(2)
        role.select_delete()
        time.sleep(2)
        role.select_confirm_delete()
        # role.select_role_page()
        time.sleep(1)
        table_content = role.get_role_table_content()
        random_role_name = role.get_random_role_name_by_file('case15')
        check.is_not_in(random_role_name, table_content)
        time.sleep(1)
