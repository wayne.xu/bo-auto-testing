import pytest

from utils.utils import TestDataLoader, SystemLogger

log = SystemLogger.get_logger()


# Define pytest.fixture for rebate_service testing use.

##############################################
##### Belows are UI testing sharing data #####
##############################################

@pytest.fixture()
def test_func_list() -> list:
    func_list = [
        'Dashboard',
        'Management',
        'Member Exclusion'
    ]
    return func_list


@pytest.fixture()
def test_account() -> dict:
    test_data = TestDataLoader()
    test_account = test_data['test_login_account']
    return {
        'email': test_account['email'],
        'password': test_account['password']
    }

@pytest.fixture()
def test_player() -> dict:
    test_data = TestDataLoader()
    test_player = test_data['test_player_membercode']
    return {
        'membercode': test_player['membercode']
    }


@pytest.fixture()
def test_add_rebate_data() -> dict:
    test_data = TestDataLoader()
    test_add_rebate_data = test_data['data_for_add_rebate']
    return {
        'rebate_name': test_add_rebate_data['rebateName'],
        'tnc_url': test_add_rebate_data['tncURL'],
        'mini_amount': test_add_rebate_data['MinimumAmount'],
        'rebate_given_per_member': test_add_rebate_data['MaximumRebateGivenPerMember'],
        'rebate_given_this_event': test_add_rebate_data['MaximumRebateGivenForThisEvent'],
        't1min': test_add_rebate_data['t1min'],
        't1max': test_add_rebate_data['t1max'],
        't1rebate': test_add_rebate_data['t1rebate'],
        't2min': test_add_rebate_data['t2min'],
        't2max': test_add_rebate_data['t2max'],
        't2rebate': test_add_rebate_data['t2rebate']
    }


@pytest.fixture()
def test_action_list_by_management() -> list:
    action_list = [
        "View Performance\nView Rules\nEdit Rules\nDuplicate\nDeactivate\nDelete\nChange Logs"
    ]
    return action_list


###############################################
##### Belows are API testing sharing data #####
###############################################

@pytest.fixture()
def request_params():
    return {
        'account_id': 'ID_01'
    }

###########################################################
##### Belows are both UI and API testing sharing data #####
###########################################################
