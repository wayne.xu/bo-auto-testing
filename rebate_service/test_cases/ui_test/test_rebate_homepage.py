import allure
import pytest
import pytest_check as check

from utils.envs import Envs
from utils.decorators import execute_env
from bo_shared.bo_service_pages import BoService
from rebate_service.pages.rebate_homepage import RebateHomePage


@pytest.fixture()
def test_func_list():
    return {
      "rebate": [
        'dashboard',
        'management',
        'member_exclusion',
        'member_exclusion/member_rebate_level',
        'member_exclusion/rebate_blacklist',
      ],
      "role": [
        'user_management',
        'role_management',
        'policy_management',
      ],
      "bonus": [
        'management',
        'report',
        'report/finance',
        'report/player',
        'unfinished_game_checklist',
      ],
      "member": [
        'csd',
        'player_list',
        'house_player_list',
        'export_member_data',
        'uploader',
      ]
    }


@pytest.fixture()
def service_names():
    return [
        'rebate',
        'bonus',
        'member',
        'role',
        'excluded_game'
    ]


def adjust_service_name(service_name: str) -> str:
    name_map = {
        BoService.Role.ServiceName: "role_service",
        BoService.Rebate.ServiceName: "rebate",
        BoService.Bonus.ServiceName: "bonus",
    }
    return name_map[service_name]


@allure.feature('Rebate Service homepage test')
class TestRebateHomePage():

    # Check the Lest side Menu is correct
    @execute_env(envs=Envs.ALL.value)
    @allure.story("Test rebate service list")
    def test_rebate_service_list(self, driver, test_account, test_func_list):
        func_list = test_func_list['rebate']
        homepage = RebateHomePage(driver)
        homepage.login(test_account)
        current_func_list = homepage.get_service_list(BoService.Rebate.ServiceName)
        # Check its length
        check.equal(len(current_func_list), len(func_list))
        # Check all of the contents whether are the same or not
        for current, test in zip(current_func_list, func_list):
            assert current == test

    @execute_env(envs=Envs.ALL.value)
    @allure.story("Test role service list")
    def test_role_service_list(self, driver, test_account, test_func_list):
        func_list = test_func_list['role']
        homepage = RebateHomePage(driver)
        homepage.login(test_account)
        current_func_list = homepage.get_service_list(BoService.Role.ServiceName)
        # Check its length
        check.equal(len(current_func_list), len(func_list))
        # Check all of the contents whether are the same or not
        for current, test in zip(current_func_list, func_list):
            assert current == test

    @execute_env(envs=Envs.ALL.value)
    @allure.story("Test bonus service list")
    def test_bonus_service_list(self, driver, test_account, test_func_list):
        func_list = test_func_list['bonus']
        homepage = RebateHomePage(driver)
        homepage.login(test_account)
        current_func_list = homepage.get_service_list(BoService.Bonus.ServiceName)
        # Check its length
        check.equal(len(current_func_list), len(func_list))
        # Check all of the contents whether are the same or not
        for current, test in zip(current_func_list, func_list):
            assert current == test

    @execute_env(envs=Envs.ALL.value)
    @allure.story("Test member service list")
    def test_member_service_list(self, driver, test_account, test_func_list):
        func_list = test_func_list['member']
        homepage = RebateHomePage(driver)
        homepage.login(test_account)
        current_func_list = homepage.get_service_list(BoService.Member.ServiceName)
        # Check its length
        check.equal(len(current_func_list), len(func_list))
        # Check all of the contents whether are the same or not
        for current, test in zip(current_func_list, func_list):
            assert current == test

    # Check the page name = current page name
    @execute_env(envs=Envs.ALL.value)
    @allure.story("Test for visiting home page")
    def test_visit_home_page(self, driver, test_account):
        homepage = RebateHomePage(driver)
        homepage.login(test_account)
        assert homepage.url_router.is_page_url(
            homepage.driver.current_url, 'home_page'
        )

    @execute_env(envs=Envs.ALL.value)
    @allure.story("Test left menu service names")
    def test_left_menu_service_names(self, driver, test_account, service_names):
        homepage = RebateHomePage(driver)
        homepage.login(test_account)
        current_service_names = homepage.get_all_service_names()
        for current_name, test_name in zip(current_service_names, service_names):
            check.equal(current_name, test_name)

    @execute_env(envs=Envs.ALL.value)
    @allure.story("Test for visiting home page")
    @pytest.mark.parametrize(
        'service_name, func_name',[
            [BoService.Role.ServiceName, BoService.Role.UserManagement],
            [BoService.Role.ServiceName, BoService.Role.RoleManagement],
            [BoService.Role.ServiceName, BoService.Role.PolicyManagement],
            [BoService.Rebate.ServiceName, BoService.Rebate.DashBoard],
            [BoService.Rebate.ServiceName, BoService.Rebate.Management],
            [BoService.ExcludedGame.ServiceName, ""],
            [BoService.Rebate.ServiceName, BoService.Rebate.RebateLevel],
            [BoService.Rebate.ServiceName, BoService.Rebate.RebateBlackList],
            [BoService.Bonus.ServiceName, BoService.Bonus.Finance],
            # Not yet
            # [BoService.Bonus.ServiceName, BoService.Bonus.Player],
        ]
    )
    def test_navigation_menu(self, driver, test_account, service_name, func_name):
        homepage = RebateHomePage(driver)
        homepage.login(test_account)
        if func_name:
            homepage.click_service_func_list(service_name, func_name)
            page_name = homepage.get_current_page_name(include_parents=True)
            page_name = homepage.normalize_string(page_name)
            expect_page_name = '/'.join([
                adjust_service_name(service_name),
                func_name
            ])
            check.equal(page_name, expect_page_name)
        else:
            homepage.click_service_name(service_name)
            page_name = homepage.get_current_page_name()
            page_name = homepage.normalize_string(page_name)
            check.equal(service_name, page_name)