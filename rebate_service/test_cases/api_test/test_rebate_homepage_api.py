import pytest
import allure

from utils.utils import SystemLogger
from rebate_service.pages_api.rebate_homepage_api import RebateHomePageAPI

log = SystemLogger.get_logger()


@allure.feature('Rebate Service home page api test')
class TestRebateHomePageAPI():

    pass
    # @allure.story("It's just a sample code")
    # def test_get_data(self, request_params ):
    #     rebate_homepage_api = RebateHomePageAPI()
    #     res = rebate_homepage_api.get_test_data(request_params)
    #     assert res.status_code == 200
    #     assert res.json() == {
    #         "account": "test_account_01",
    #         "password": "test_password_01"
    #     }