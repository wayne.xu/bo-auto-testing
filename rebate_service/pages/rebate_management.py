import re
import time

from typing import Tuple, List, Any, Optional
from datetime import datetime, timedelta

import allure

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import StaleElementReferenceException

from bo_shared.bo_service_pages import BoService
from bo_shared.components.ant_table import AntTable
from rebate_service.pages.rebate_homepage import RebateHomePage



class RebateManagement(RebateHomePage):

    PAGE_NAME = BoService.Rebate.Management

    page_shared_xpath = {
        "table_xpath": "//tr{table_row}/td{table_column}"
    }

    def __init__(self, driver: WebDriver):
        super(RebateManagement, self).__init__(driver)

    @allure.step('Click the rebate management tab list')
    def get_tab_list_element(self, tab_type: str) -> WebElement:
        """Manipulate the tab list Approved, Pending, Reject options
        """
        tabs_xpath_format = (
            '//div[@role="tablist"]'
            '//div[@class="ant-tabs-nav-list"]/div{tab_idx}/div'
        )
        tab_name_map = {
            'approved': 1,
            'pending': 2,
            'rejected': 3,
        }
        tab_type = tab_type.strip().lower()
        tab_idx = tab_name_map[tab_type]
        xpath = self.xpath_editor(
            tabs_xpath_format,
            tab_idx=tab_idx
        )
        tab_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            xpath
        )
        return tab_element

    @allure.step("Get current select tab")
    def get_current_selected_tab(self) -> str:
        """Get current selected tab
        """
        num_pat = self.shared_regex['number_pat']
        tabs_xpath = (
            '//div[@role="tablist"]'
            '//div[@class="ant-tabs-nav-list"]/div/div'
        )
        tab_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            tabs_xpath
        )
        for tab_element in tab_elements:
            is_selected = tab_element.get_attribute('aria-selected')
            is_selected = self.to_boolean(is_selected)
            if is_selected:
                selected_tab_name = num_pat.sub('', tab_element.text.strip())
                selected_tab_name = self.normalize_string(selected_tab_name.strip())
                return selected_tab_name

    @allure.step("Check the tab list is selected")
    def is_tab_list_selected(
            self,
            tab_type: str,
            return_element=False
        ) -> Tuple[bool, Optional[WebElement]]:
        """Check the tab whether it is be selected or not.

        Args:
            tab_type (str): approved, pending, rejected
            return_element (bool, optional): Defaults to False
        """
        tab_element = self.get_tab_list_element(tab_type)
        is_selected = tab_element.get_attribute('aria-selected')
        is_selected = self.to_boolean(is_selected)
        if return_element:
            return (is_selected, tab_element)
        else:
            return (is_selected, None)

    @allure.step('Click the tab list')
    def click_tab_list(self, tab_type: str) -> bool:
        is_selected, tab_element = self.is_tab_list_selected(tab_type, True)
        if is_selected == False:
            self.action_chains.move_to_element(tab_element).click().perform()
            self.log.debug(f"Click the {tab_type} tab list")

    @allure.step('Click filter manipulation button')
    def click_filter_manipulation_btn(self, manipulation_type: str):
        """Click filter manipulation button

        Args:
            manipulation_type (str): `expand`, `reset`, `search` and `customize`
        """
        manipulation_type = manipulation_type.strip().lower()
        manipulation_xpath_map = {
            'expand': '//button[@data-testid="actionExpand"]',
            'reset': '//button[@data-testid="actionReset"]',
            'search': '//button[@data-testid="actionSearch"]',
            'customize': '//button[@data-testid="actionCustomize"]',
        }
        manipulation_xpath = manipulation_xpath_map[manipulation_type]
        clickable_element: WebElement = self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            manipulation_xpath
        )
        clickable_element.click()
        self.log.debug(f'Click {manipulation_type} button')

    @allure.step("Fill the value into filter condition")
    def fill_input_element(self, input_label_name: str, fill_content: Any):
        """_summary_

        Args:
            input_label_name (str): `rebate_name`, `update_by`, `member_code`, `minimum_amount`

            fill_content (Any)
        """
        input_xpath_map = {
            # The same as the add rebate page
            'rebate_name': '//input[@data-testid="name"]',
            'update_by': '//input[@data-testid="updateBy"]',
            'member_code': '//input[@data-testid="applicantMemberCodes"]',
            'minimum_amount': '//input[@data-testid="minAmount"]'
        }
        input_label_name = input_label_name.strip().lower()
        input_xpath = input_xpath_map[input_label_name]
        input_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            input_xpath
        )
        input_element.send_keys(fill_content)
        self.log.debug(
            f"Send the {fill_content} to the "
            f"input element: {input_label_name}"
        )

    @allure.step("Click the form filter label")
    def click_form_filter_label(self, click_name: str):
        """Click form filter label for shrinking options

        Args:
            click_name (str): The filter form label name without brackets
        """
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            '//form[@data-testid="expandableFilter"]//label'
        )
        label_names = list()
        for element in elements:
            label_name = self.drop_brackets_words(element.text.strip())
            label_name = self.normalize_string(label_name)
            label_names.append(label_name)
            if click_name == label_name:
                self.action_chains.move_to_element(element).click().perform()
                return
        raise ValueError(
            f"Can't find the `{click_name}` in the {label_names}"
        )

    @allure.step("Click add rebate button")
    def click_add_rebate_btn(self):
        xpath = '//button[@data-testid="actionAddRebate"]'
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            xpath
        ).click()

    @allure.step("Get add rebate current step")
    def get_add_rebate_current_step(self) -> int:
        """Get add rebate page current step
        """
        step_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            '//div[contains(@data-testid, "step")]'
        )
        for idx, element in enumerate(step_elements):
            _class = element.get_attribute('class')
            attrs = _class.split(' ')
            if "ant-steps-item-active" in attrs:
                return idx + 1
        raise ValueError("Can't get add rebate page current step")

    @allure.step("Get table action elements")
    def get_table_action_elements(
            self,
            row: Optional[int] = None,
            return_element: bool = False
        ) -> List[List[Tuple[str, Optional[WebElement]]]]:
        """Get table action column element

        Args:
            row (Optional[int], optional): Defaults to None.
            return_element (bool, optional): Defaults to False.
        """
        time.sleep(3)
        ant_table = AntTable(self.driver)
        content_elements = ant_table.get_table_contents_with_column_name(
           column_name='action', row=row
        )
        all_action_info = list()
        for content_element in content_elements:
            button_elements: List[WebElement] = content_element.find_elements(
                By.XPATH,
                'div/div/button[@data-testid]'
            )
            row_action_info = list()
            for button_element in button_elements:
                if button_element.text:
                    button_element_name = self.normalize_string(button_element.text)
                else:
                    button_element_name = 'more'

                attrs = self.get_element_all_attrs(button_element)
                is_disable = True if 'disabled' in attrs else False

                if return_element:
                    row_action_info.append({
                        'name': button_element_name,
                        'element': button_element,
                        'is_disabled': is_disable,
                    })
                else:
                    row_action_info.append({
                        'name': button_element_name,
                        'is_disabled': is_disable,
                    })
            all_action_info.append(row_action_info)
        return all_action_info

    @allure.step("Get Add Rebate page required field names")
    def get_add_rebate_required_field_names(self) -> list:
        brackets_word_pat = re.compile(r'\([\w0-9\+]+\)')
        xpath = (
            '//form//*[@class="ant-form-item-explain-error"]'
            '/parent::div/parent::div/parent::div//label[@for]'
        )
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            xpath
        )
        names = list()
        for element in elements:
            name = element.text.split('\n')[0]
            name = brackets_word_pat.sub('', name)
            name = self.normalize_string(name.strip())
            names.append(name)
        return names

    @allure.step("Click the button on the Add Rebate page")
    def click_add_rebate_page_btn(self, btn_type: str):
        """Click button on add rebate page.

        Args:
            btn_type (str): discard, next
        """
        btn_map = {'discard': 1, 'next': 2}
        idx = btn_map[btn_type]
        btn_xpath_format = '//form[@id="step1"]//button[@data-testid]{idx}'
        btn_xpath = self.xpath_editor(
            btn_xpath_format,
            idx=idx
        )
        element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            btn_xpath
        )
        element.click()
        self.log.debug(f"Click the {btn_type} button")

    # 登入完成，展開sidebar (Rebate Service)
    @allure.step('Click function list button R')
    def click_functions_list_r(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '//*[@id="__next"]//div[1]/ul/li[2]/div'
        ).click()

    # 登入完成，展開sidebar (Member Exclusion)
    @allure.step('Click function list button M')
    def click_functions_list_m(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '/html/body/div[1]/section/aside/div/div/div[1]/ul/li[2]/ul/li[3]'
        ).click()

    # 登入完成，點擊Management
    @allure.step('Click the Management btn')
    def click_the_management_btn(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '/html/body/div/section/aside/div/div/div[1]/ul/li[2]/ul/li[2]/span'
        ).click()

    # 登入完成，點擊Export file
    @allure.step('Click the Management btn')
    def click_the_export_file_btn(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '//*[@id="__next"]//div[3]/ul/li/span/div[1]/button'
        ).click()

    # Click the View Performance button
    @allure.step('Click the View Performance button')
    def click_the_view_performance_btn(self, num_button: int):
        table_xpath = self.xpath_editor(
            self.page_shared_xpath['table_xpath'],
            table_row=None, table_column=12
        )
        performance_btn_xpath = self.join_xpath(table_xpath, '/div/div[1]')
        elements = WebDriverWait(self.driver, self.timeout).until(
            EC.visibility_of_all_elements_located(
                (By.XPATH, performance_btn_xpath)
            )
        )
        elements[num_button].click()

    # Click the BatchManage Btn
    @allure.step("Click the BatchManage Btn")
    def click_the_batch_manage_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '/html/body/div[1]/section/section/main/div/div[2]/div[2]/div/div[1]/div/button[1]'
        ).click()

    # Click the Upload button
    @allure.step("Click the Upload button Membercode")
    def click_the_upload_btn_membercode(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '/html/body/div[2]/div/div/ul/li[2]/span/div/div[2]'
        ).click()

    # Click the select file button
    @allure.step("Click the select file button")
    def click_the_select_file_button(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.CSS_SELECTOR,
            ".ant-btn-default > span:nth-child(2)"
        ).click()

        time.sleep(3)

    # Get the pang step = step1
    @allure.step("Get the pang step1")
    def get_the_page_step_1(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[2]/div/div[1]/div/div[2]/span'
        ).text
        return element

    # Get the pang step = step2
    @allure.step("Get the pang step2")
    def get_the_page_step_2(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[2]/div/div[2]/div/div[2]/span'
        ).text
        return element

    # Get the pang step = step3
    @allure.step("Get the pang step3")
    def get_the_page_step_3(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[2]/div/div[3]/div/div[2]/span'
        ).text
        return element

    # Add rebate > Step1 > input Name
    @allure.step("Add rebate and input name")
    def input_name_for_add_rebate_by_step1(self, rebate_name: str):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_name"]'
        ).send_keys(rebate_name)

    # Add rebate > Step1 > input tncURL
    @allure.step("Add rebate and input tncURL")
    def input_tncURL_for_add_rebate_by_step1(self, tnc_url: str):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_tncURL"]'
        ).send_keys(tnc_url)

    # Add rebate > Step1 > Platform selector > Desktop
    @allure.step("Add rebate and select platform_as_Desktop")
    def select_platform_D_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_platform"]/label[1]/span[1]'
        ).click()

    # Add rebate > Step1 > Platform selector > Mobile
    @allure.step("Add rebate and select platform_as_Mobile")
    def select_platform_M_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_platform"]/label[2]/span[1]'
        ).click()

    # Add rebate > Step1 > Rules selector > Align rules in 2 platforms
    @allure.step("Add rebate and select rules as Align rules")
    def select_rules_align_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_platformRules"]/label[1]/span[2]'
        ).click()

    # Add rebate > Step1 > Rules selector > Different rules in 2 platforms
    @allure.step("Add rebate and select rules as Different rules")
    def select_rules_different_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_platformRules"]/label[2]/span[2]'
        ).click()

    # Add rebate > Step1 > Period (GMT+8) > Date picker > Start Day to End Day
    @allure.step("Select the start day for Period")
    def select_the_day_for_period_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div/form/div[2]/div/div[1]/div[1]/div[2]/div/div/div/div[1]'
        ).click()
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//div[2]//div[2]/div/div[1]/div/div[2]/table/tbody/tr[5]/td[3]'
        ).click()
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//div[2]//div[2]/div/div[2]/div/div[2]/table/tbody/tr[5]/td[7]'
        ).click()

    # Add rebate > Step1 > Recurrence > No Repeat
    @allure.step("Add rebate and select Recurrence as No Repeat")
    def select_recurrence_norepeat_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//*[@id="__next"]//div/form/div[2]/div/div[1]/div[2]/div[2]'
        ).click()
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '/html/body/div[3]/div/div/div/div[2]/div[1]/div/div/div[1]'
        ).click()

    # Add rebate > Step1 > Recurrence > Monthly
    @allure.step("Add rebate and select Recurrence as Monthly")
    def select_recurrence_monthly_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//*[@id="__next"]//div/form/div[2]/div/div[1]/div[2]/div[2]'
        ).click()
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '/html/body/div[3]/div/div/div/div[2]/div[1]/div/div/div[2]'
        ).click()

    # Add rebate > Step1 > Eligible Member > All members
    @allure.step("Add rebate and select Eligible Member as All")
    def select_am_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_eligibleMember"]/label[1]'
        ).click()

    # Add rebate > Step1 > Eligible Member > Specific member level
    @allure.step("Add rebate and select Eligible Member as Specific member level")
    def select_sml_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_eligibleMember"]/label[2]'
        ).click()

    # Add rebate > Step1 > Eligible Member > Specific member
    @allure.step("Add rebate and select Eligible Member as Specific member")
    def select_sm_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_eligibleMember"]/label[3]'
        ).click()

    # Add rebate > Step1 > Member Level > VIP
    @allure.step("Add rebate and select Member Level as VIP")
    def select_memberlevel_vip_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//*[@id="__next"]//div/form/div[2]/div/div[1]/div[4]/div[2]'
        ).click()
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//div[4]/div/div/div/div[2]/div[1]/div/div/div[1]'
        ).click()

    # Add rebate > Step1 > Member Level > General
    @allure.step("Add rebate and select Member Level as General")
    def select_memberlevel_general_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//*[@id="__next"]//div/form/div[2]/div/div[1]/div[4]/div[2]'
        ).click()
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//div[4]/div/div/div/div[2]/div[1]/div/div/div[2]'
        ).click()

    # Add rebate > Step1 > Member Level > Level 1
    @allure.step("Add rebate and select Member Level as Level 1")
    def select_memberlevel_Level1_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//*[@id="__next"]//div/form/div[2]/div/div[1]/div[4]/div[2]'
        ).click()
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//div[4]/div/div/div/div[2]/div[1]/div/div/div[3]'
        ).click()

    # Add rebate > Step1 > Member Level > Level 2
    @allure.step("Add rebate and select Member Level as Level 2")
    def select_memberlevel_Level2_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//*[@id="__next"]//div/form/div[2]/div/div[1]/div[4]/div[2]'
        ).click()
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//div[4]/div/div/div/div[2]/div[1]/div/div/div[4]'
        ).click()

    # Add rebate > Step1 > Member Level > Level 3
    @allure.step("Add rebate and select Member Level as Level 3")
    def select_memberlevel_Level3_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//*[@id="__next"]//div/form/div[2]/div/div[1]/div[4]/div[2]'
        ).click()
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//div[4]/div/div/div/div[2]/div[1]/div/div/div[5]'
        ).click()

    # Add rebate > Step1 > Member Level > Level 4
    @allure.step("Add rebate and select Member Level as Level 4")
    def select_memberlevel_Level4_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//*[@id="__next"]//div/form/div[2]/div/div[1]/div[4]/div[2]'
        ).click()
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//div[4]/div/div/div/div[2]/div[1]/div/div/div[6]'
        ).click()

    # Add rebate > Step1 > Rebate Sending Frequency > Every day
    @allure.step("Add rebate and select Rebate Sending Frequency as Every day")
    def select_rebatesending_everyday_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_rebateSendingFrequency"]/label[1]'
        ).click()

    # Add rebate > Step1 > Rebate Sending Frequency > Every week
    @allure.step("Add rebate and select Rebate Sending Frequency as Every week")
    def select_rebatesending_everyweek_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_rebateSendingFrequency"]/label[2]'
        ).click()

    # Add rebate > Step1 > Rebate Sending Frequency > Every month
    @allure.step("Add rebate and select Rebate Sending Frequency as Every month")
    def select_rebatesending_everymonth_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_rebateSendingFrequency"]/label[3]'
        ).click()

    # Add rebate > Step1 > Product selector > Lucy
    @allure.step("Add rebate and select Product")
    def select_product_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_toProduct"]'
        ).click()
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_toProduct"]'
        ).send_keys(Keys.DOWN, Keys.ENTER)

    # Add rebate > Step1 > Accumulate selector
    @allure.step("Add rebate and select Accumulate")
    def select_accumulate_for_add_rebate_by_step1(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_accumulate_type"]'
        ).click()

    # Accumulate selector > select items
    @allure.step("Select one of Accumulate")
    def select_one_accumulate(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_accumulate_type"]'
        ).send_keys(Keys.DOWN, Keys.DOWN, Keys.DOWN, Keys.ENTER)

    # Add rebate > Step1 > Minimum Amount
    @allure.step("Add rebate and input Minimum Amount")
    def input_min_amount_for_add_rebate_by_step1(self, mini_amount: int):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1_accumulate_min"]'
        ).send_keys(mini_amount)

    # Add rebate > Step1 > click next btn to step2
    @allure.step("click next btn to step2")
    def click_the_next_btn_to_step2(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step1"]/div[3]/button'
        ).click()

    # Add rebate > Step2 > select the Maximum rebate given/per member > No limitation
    @allure.step("Select the Maximum rebate given per member with No limitation")
    def select_the_maximum_rebate_given_molimit_pm(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_rebateAmountPerMember_isLimited"]/label[1]'
        ).click()

    # Add rebate > Step2 > select the Maximum rebate given/per member > Maximum rebate given
    @allure.step("Select the Maximum rebate given per member with Maximum rebate given")
    def select_the_maximum_rebate_given_havelimit_pm(self, rebate_given_per_member: int):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_rebateAmountPerMember_isLimited"]/label[2]'
        ).click()
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_rebateAmountPerMember_maximum"]'
        ).send_keys(rebate_given_per_member)
        return element

    # Add rebate > Step2 > select the Maximum rebate given/this event > No limitation
    @allure.step("Select the Maximum rebate given per member with No limitation")
    def select_the_maximum_rebate_given_molimit_te(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_rebateAmountPerEvent_isLimited"]/label[1]'
        ).click()

    # Add rebate > Step2 > select the Maximum rebate given/this event > Maximum rebate given
    @allure.step("Select the Maximum rebate given per member with Maximum rebate given")
    def select_the_maximum_rebate_given_havelimit_te(self, rebate_given_this_event: int):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_rebateAmountPerEvent_isLimited"]/label[2]'
        ).click()
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_rebateAmountPerEvent_maximum"]'
        ).send_keys(rebate_given_this_event)
        return element

    # Add Tier
    @allure.step("Click the Add Tier Button")
    def click_the_add_tier_btn(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//*[@id="step2"]/div[1]/div/div/button'
        ).click()

    # Add rebate > Step2 > Rebate tier setup > level 1
    @allure.step("Setup the Rebate tier 1")
    def setup_the_rebate_tier_1(self, t1min: int, t1max: int, t1rebate: int):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_tiers_0_min"]'
        ).send_keys(t1min)
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_tiers_0_max"]'
        ).send_keys(t1max)
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_tiers_0_rebate"]'
        ).send_keys(t1rebate)

    # Add rebate > Step2 > Rebate tier setup > level 2
    @allure.step("Setup the Rebate tier 2")
    def setup_the_rebate_tier_2(self, t2min: int, t2max: int, t2rebate: int):
        # self.find_element_wait(
        #     EC.presence_of_element_located,
        #     By.XPATH,
        #     '//*[@id="step2_tiers_1_min"]'
        # ).send_keys(t2min)
        # self.find_element_wait(
        #     EC.presence_of_element_located,
        #     By.XPATH,
        #     '//*[@id="step2_tiers_1_max"]'
        # ).send_keys(t2max)
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_tiers_1_rebate"]'
        ).send_keys(t2rebate)

    # Add rebate > Step2 > click next btn to step3
    @allure.step("click next btn to step3")
    def click_the_next_btn_to_step3(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2"]/div[2]/div[2]/button'
        ).click()

    # Get the tier 2 minimum rebate
    @allure.step("Get the tier 2 minimum rebate")
    def tier_add(self, t1max: int):
        t2min = t1max + 0.01
        return t2min

    # Get the Maximum rebate given per member
    @allure.step("Get the Maximum rebate given per member")
    def get_max_rebate_given_per_member(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_rebateAmountPerMember_maximum"]'
        ).get_attribute("value")
        mgpm = float(element)
        return mgpm

    # Get the Maximum rebate given for this event
    @allure.step("Get the Maximum rebate given for this event")
    def get_max_rebate_given_this_event(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_rebateAmountPerEvent_maximum"]'
        ).get_attribute("value")
        mgte = float(element)
        return mgte

    # Get the Min. deposit amount in tier2.
    @allure.step("Get the Min. deposit amount in tier2")
    def get_min_deposit_tier2(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="step2_tiers_1_min"]'
        ).get_attribute("value")
        mdt2 = float(element)
        return mdt2

    # Get rebate name in step3.
    @allure.step("Get rebate name in step3")
    def get_rebate_name_in_step3(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[5]/div/div/div[1]/div/div/table/tbody/tr[1]/td/div/span[2]').text
        return element

    # Get rebate TnC URL in step3.
    @allure.step("Get rebate TnC URL in step3")
    def get_tnc_url_in_step3(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[5]/div/div/div[1]/div/div/table/tbody/tr[2]/td/div/span[2]').text
        return element

    # Get rebate Platform in step3.
    @allure.step("Get rebate Platform in step3")
    def get_platform_in_step3(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[5]/div/div/div[1]/div/div/table/tbody/tr[3]/td/div/span[2]').text
        return element

    # Get rebate Rules in step3.
    @allure.step("Get rebate Rules in step3")
    def get_rules_in_step3(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[5]/div/div/div[1]/div/div/table/tbody/tr[4]/td/div/span[2]').text
        return element

    # Get rebate Period (GMT+8) in step3.
    @allure.step("Get rebate Period (GMT+8) in step3")
    def get_period_in_step3(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[5]/div/div/div[1]/div/div/table/tbody/tr[5]/td/div/span[2]').text
        return element

    # Get rebate Repetition in step3.
    @allure.step("Get rebate Repetition in step3")
    def get_repetition_in_step3(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[5]/div/div/div[1]/div/div/table/tbody/tr[6]/td/div/span[2]').text
        return element

    # Get rebate Tier 1 range in step3..Desktop
    @allure.step("Get rebate Tier 1.range in step3.Desktop")
    def get_tier1_range_desktop(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[5]//div[5]//div[1]/div/div/div[3]/div/table/tbody/tr[1]').text
        element_split = element.split("\n")
        return element_split[1]

    # Reorg rebate Tier 1 range in step3..Desktop
    @allure.step("Reorg rebate Tier 1.range in step3.Desktop")
    def reorg_tier1_range_desktop(self, t1min: int, t1max: int):
        str_t1min = str(t1min)
        tr_t1max = str(t1max)
        result = str_t1min + " " + "to" + tr_t1max + " " + "CNY"
        return result

    # Get rebate Tier 1 Rate ..Desktop
    @allure.step("Get rebate Tier 1 Rate")
    def get_tier1_rate_desktop(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[5]//div[5]//div[1]/div/div/div[3]/div/table/tbody/tr[1]').text
        element_split = element.split("\n")
        return element_split[2]

    # Reorg rebate Tier 1 rate ..Desktop
    @allure.step("Reorg rebate Tier 1.Rate")
    def reorg_tier1_rate_desktop(self, t1rebate: int):
        str_t1rebate = str(t1rebate)
        result = str_t1rebate + "%" + " " + "rebate"
        return result

    # Get rebate Tier 2.range in step3..Desktop
    @allure.step("Get rebate Tier 2 range in step3.Desktop")
    def get_tier2_range_desktop(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[5]//div[5]//div[1]/div/div/div[3]/div/table/tbody/tr[2]').text
        element_split = element.split("\n")
        return element_split[1]

    # Reorg rebate Tier 2 range ..Desktop
    @allure.step("Reorg rebate Tier 2 range in step3.Desktop")
    def reorg_tier2_range_desktop(self, t1max: int):
        t2min = t1max + 0.01
        str_t2min = str(t2min)
        result = str_t2min + " " + "toand" + " " + "aboveee"
        return result

    # Get rebate Tier 2 Rate ..Desktop
    @allure.step("Get rebate Tier 2 Rate")
    def get_tier2_rate_desktop(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[5]//div[5]//div[1]/div/div/div[3]/div/table/tbody/tr[2]').text
        element_split = element.split("\n")
        return element_split[2]

    # Reorg rebate Tier 2 rate ..Desktop
    @allure.step("Reorg rebate Tier 2 Rate")
    def reorg_tier2_rate_desktop(self, t2rebate: int):
        str_t1rebate = str(t2rebate)
        result = str_t1rebate + "%" + " " + "rebateee"
        return result

    # check the viewDetail btn
    @allure.step("Check the page title equal record")
    def click_the_view_detail_first(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[3]/ul/li/span/div[2]//table/tbody/tr[2]/td[10]/div/div[1]'
        ).click()

    # get the page title of rebate detail
    @allure.step("Click the view_Detail_first")
    def check_the_view_detail_first1(self):
        title_of_view_rebate_detail = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div/div[2]/div[1]/h1'
        )
        return title_of_view_rebate_detail.text.strip()

    # get the title of rebate records
    @allure.step("Check the page title equal record")
    def check_the_page_title_equal_record(self):
        element_rebate_record = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[3]/ul/li/span/div[2]/div/div/div/div/div/table/tbody/tr[2]/td[2]/div/span[2]'
        )
        return element_rebate_record.text.strip()

    # Click the Action button (order1)
    @allure.step("Click the Action Btn")
    def click_the_action_list_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//tbody/tr[2]/td[10]/div/div[2]/button'
        ).click()

    # Click the Change Logs button (order1)
    @allure.step("Click the Change Logs Btn")
    def click_the_change_logs_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "(//a[normalize-space()='Change Logs'])[1]"
        ).click()

    # Click the Deactivate button (order1)
    @allure.step("Click the Deactivate Btn")
    def click_the_deactivate_logs_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "(//span[normalize-space()='Deactivate'])[1]"
        ).click()

    # Open the Data picker and input date range for deactivate(order1)
    @allure.step("Open the Data picker for deactivate")
    def open_the_dp_for_deactivate(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//div[3]/div/div[2]/div/div[2]/div[2]/form/div/div/div/div/div/div/input"
        ).send_keys(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        # format example 2022-04-05 00:03:00

    # submit date range for deactivate(order1)
    @allure.step("Open the Data picker for deactivate")
    def submit_date_for_deactivate(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//div[4]/div/div/div/div/div[2]/ul/li[2]/button"
        ).click()

    # submit deactivate(order1)
    @allure.step("Open the Data picker for deactivate")
    def submit_deactivate(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//div[3]/div/div[2]/div/div[2]/div[3]/button[2]"
        ).click()

    # Get current Breadcrumb Link lv4
    @allure.step("Get current Breadcrumbs")
    def get_current_breadcrumb_lv4(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]/section/section/main/div/div/div[1]/span[4]').text
        return element

    # Get current Breadcrumb Link lv3
    @allure.step("Get current Breadcrumbs")
    def get_current_breadcrumb_lv3(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]/section/section/main/div/div/div[1]/span[3]').text
        return element

    # Get the Rebate ID in Performance page (order 1)
    @allure.step("Get the Rebate ID in Performance page")
    def get_the_rebate_id_in_performance_page(self, num_copy_button: int) -> str:
        table_xpath = self.xpath_editor(
            self.page_shared_xpath['table_xpath'],
            table_row=None, table_column=6
        )
        all_rebate_id_xpath = self.join_xpath(table_xpath, '/span')
        elements = WebDriverWait(self.driver, self.timeout).until(
            EC.visibility_of_all_elements_located(
                (By.XPATH, all_rebate_id_xpath)
            )
        )
        return elements[num_copy_button].text

    # 取得 Action Btn清單
    @allure.step('Get Action list after clicking button')
    def get_Action_list_by_management(self) -> List:
        elements = WebDriverWait(self.driver, self.timeout).until(
            EC.presence_of_all_elements_located((By.XPATH, '/html/body/div[2]/div/div'))
        )
        elements = [element.text for element in elements]
        return elements

    # 取得 Detail page的頁面元素包含 字串 = Total Turnover(CNY)
    @allure.step('Get the page elemt = Total Turnover')
    def get_the_str_total_turnover(self) -> List:
        elements = WebDriverWait(self.driver, self.timeout).until(
            EC.presence_of_all_elements_located(
                (By.XPATH, '//*[@id="__next"]//div[3]/div[2]/div[2]/div[1]/div/div/div[1]/div/div/div[1]/div/div[1]'))
        )
        elements = [element.text for element in elements]
        return elements

    # 取得 Detail page的頁面元素 包含 字串 = Total Rebate Given(CNY)
    @allure.step('Get the page elemt = Total Rebate Given')
    def get_the_str_total_rebate_given(self) -> List:
        elements = WebDriverWait(self.driver, self.timeout).until(
            EC.presence_of_all_elements_located(
                (By.XPATH, '//*[@id="__next"]//div[3]/div[2]/div[2]/div[1]/div/div/div[1]/div/div/div[2]/div/div[1]'))
        )
        elements = [element.text for element in elements]
        return elements

    # Copy Button for Rebate ID
    @allure.step('Copy Button for Rebate ID')
    def click_the_copy_btn_for_rebate_id(self, num_copy_button: int):
        table_xpath = self.xpath_editor(
            self.page_shared_xpath['table_xpath'],
            table_row=0, table_column=6
        )
        copy_btn_xpath = self.join_xpath(table_xpath, '/span/div')
        elements = WebDriverWait(self.driver, self.timeout).until(
            EC.visibility_of_all_elements_located(
                (By.XPATH, copy_btn_xpath)
            )
        )
        elements[num_copy_button].click()

    # 點擊 Pending Approved Request
    @allure.step('Click the Approved Request btn')
    def click_the_approved_request_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//div[3]/div[1]/div[2]/div[1]/div[1]/div/div[1]'
        ).click()

    # 點擊 Reset button
    @allure.step('Click the Reset btn')
    def click_the_reset_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//div[2]/div[2]/div/div[2]/div[2]/div[1]'
        ).click()

    # 點擊 Search button
    @allure.step('Click the Reset btn')
    def click_the_reset_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//div[2]/div[2]/div/div[2]/div[2]/div[2]'
        ).click()

    # 點擊 Option button
    @allure.step('Click the Option btn')
    def click_the_option_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//div[2]/div[2]/div/div[2]/div[2]/div[3]'
        ).click()

    # 點擊 Export button
    @allure.step('Click the Export btn')
    def click_the_export_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//div[2]/ul/li/span/div[1]/button'
        ).click()

    # 點擊 Active Days by Start Day
    @allure.step('Click the Active Days by Start Day')
    def click_the_active_date_start(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//div[2]/div[2]/div/div[1]/form/div/div[2]/div/div/div/div[1]'
        ).click()

    # 點擊 Active Days by End Day
    @allure.step('Click the Active Days by End Day')
    def click_the_active_date_end(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//div[2]/div[2]/div/div[1]/form/div/div[2]/div/div/div/div[3]'
        ).click()

    # 點擊 View Rules in Management > Detail
    @allure.step('Click the View Rules in Management > Detail')
    def click_the_view_rules_by_detail(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//main/div[1]/div[2]/div/div[1]'
        ).click()

    # 點擊 Kabob button in Management > Detail
    @allure.step('Click the Kabob button in Management > Detail')
    def click_the_kabob_by_detail(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//main/div[1]/div[2]/div/div[2]'
        ).click()

    # 點擊 Desktop button in Management > Detail
    @allure.step('Click the Desktop button in Management > Detail')
    def click_the_desktop_by_detail(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//div[2]/div[1]/div[2]/div[1]/div[1]/div/div[1]'
        ).click()

    # 點擊 Mobile button in Management > Detail
    @allure.step('Click the Mobile button in Management > Detail')
    def click_the_mobile_by_detail(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//div[2]/div[1]/div[2]/div[1]/div[1]/div/div[2]'
        ).click()

    # 點擊 Retry ALL button in Management
    @allure.step('Click the Retry ALL button in Management > Detail')
    def click_the_retry_all_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, "(//button[@class='ant-btn ant-btn-primary'])[1]"
        ).click()

    # get the default selection item status
    @allure.step("Get the default selection item status")
    def get_the_default_selected_status(self):
        status = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="rc-tabs-0-tab-approved"]').get_attribute("aria-selected")
        if status == 'true':
            return True
        else:
            return False

    # # get the Action button element
    # @allure.step("Get the Action button element")
    # def get_the_action_btn_element(self):
    #     action_element = self.find_element_wait(
    #         EC.presence_of_element_located,
    #         By.XPATH, "(//div[@class='ant-space-item'])[11]").text
    #     print(action_element)
    #     return action_element

    @allure.step("Scroll down add rebate page")
    def scroll_down_add_rebate_page(self):
        doc_size = self.get_doc_body_client_size()
        self.scroll_to(
            0, doc_size['height'],
            (By.XPATH, '//*[@id="__next"]/section/section/main/div')
        )
        self.log.info("Successfully scroll down...")

    @allure.step("Click add rebate accumulate list button")
    def click_add_rebate_accumulate_list(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '//*[@id="step1"]/div[2]/div/div[3]/div[3]/div[1]/div/div[2]/div/div/div'
        ).click()

    @allure.step("Get add rebate accumulate list")
    def get_add_rebate_accumulate_list(self) -> list:
        elements = WebDriverWait(self.driver, self.timeout).until(
            EC.presence_of_all_elements_located(
                (By.XPATH, '//div[2]/div/div/div/div[2]/div[1]/div/div/div/div')
            )
        )
        accumulate_list = [element.text for element in elements]
        return accumulate_list
