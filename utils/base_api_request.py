import os
import requests

from utils.utils import SystemLogger
from utils.url_router import URLRouter

class BaseAPIRequest():

    DEPLOYMENT_ENV = os.environ.get("DEPLOYMENT_ENV", "dev")

    def __init__(self, url_router: URLRouter):
        self.log = SystemLogger.get_logger()
        self.requests = requests
        self.init_url_router(url_router)

    def init_url_router(self, url_router: URLRouter):
        """
        Initialize the url router, and set two attributes
        `url_router` and `URL`
        """
        if issubclass(url_router, URLRouter):
            self.url_router = url_router()
        else:
            raise ValueError((
                "The `url_router` must be the subclass of `URLRouter`, "
                f"so initialize the {self.__class__.__name__} failed."
            ))
