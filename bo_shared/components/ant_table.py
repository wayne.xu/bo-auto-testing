from typing import Optional, List, Tuple, Union

from bo_shared.components.basic import BasicComponent

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import StaleElementReferenceException


class AntTable(BasicComponent):

    table_xpath = '//div[@class="ant-table-content"]/table'
    thead_xpath = table_xpath + '//th'
    table_content_xpath_format = table_xpath +\
        "//tr[@data-row-key]{row}/td{column}"

    def __init__(self, driver: WebDriver):
        super(AntTable, self).__init__(driver)

    @property
    def table_heads_map(self) -> dict:
        """
        Get table heads
        """
        heads = self.get_heads()
        heads_maps = dict()
        for idx, head_name in enumerate(heads):
            head_name = self.normalize_string(head_name)
            head_name = self.drop_brackets_words(head_name)
            heads_maps[head_name] = idx + 1
        return heads_maps

    def get_heads(self, return_elements=False) -> list:
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            self.thead_xpath
        )
        if return_elements:
            return [(element.text.strip(), element) for element in elements]
        else:
            return [element.text.strip() for element in elements]

    def get_table_contents(
        self,
        row: Optional[int] = None,
        column: Optional[int] = None
    ) -> List[WebElement]:
        """
        Get the given row and column content of the table

        Args:
            row (Optional[int], optional): Defaults to None(Get all rows).
            column (Optional[int], optional): Defaults to None(Get all columns).
        """
        xpath = self.xpath_editor(
            self.table_content_xpath_format,
            row=row,
            column=column
        )
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            xpath
        )
        return elements

    def get_table_contents_with_column_name(
        self,
        column_name: str,
        row: Optional[int] = None,
    ) -> List[WebElement]:
        """
        Get the given column name and row of table content

        Args:
            column_name (str)
            row (Optional[int], optional): Defaults to None.
        """
        try:
            idx = self.table_heads_map[column_name]
        except KeyError:
            raise KeyError(
                f"Can't find the column_name: {column_name} on "
                f"{self.__class__.__name__} heads_map: {self.table_heads_map}"
            )
        content_elements: List[WebElement] = self.get_table_contents(row=row, column=idx)
        return content_elements
