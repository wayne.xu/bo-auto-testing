
import time

import allure
import pytest_check as check

from rebate_service.pages.rebate_member_blacklist import RebateMemberBlackList


@allure.feature('Rebate Service memberExclusion test')
class TestRebateMemberBlackList():
    pass

#     @allure.story("Test Set-up the MemberCode Permission")
#     def test_setup_the_membercode_permission(self, driver, test_account, test_player):
#         rebate_memberexclusion = RebateMemberBlackList(driver)
#         rebate_memberexclusion.login(test_account)
#         raise NotImplementedError

    # Use case Manage Blacklist
    # Case 1 Import invalidate data (by csv file)
    # Case 2 Set-up 1st member permission to VIP1~VIP7 (by csv file)
    # Case 3 Set-up all member permission to VIP1~VIP7 (by csv file)
    # Case 4 Set-up 2ed member permission to General (by csv file)
    # Case 5 Set-up all member permission to General (by csv file)
    # Case 6 Manage Blacklist by select member code
    # Case 7 Manage Blacklist by check box (one)
    # Case 8 Manage Blacklist by check box (multiple select)

    # Use case Filter > Last Updated
    # Case 9 Last Updated > By default, select the past 3 months data
    # Case 10 Last Updated > Select more than past 1 year
    # Case 11 Last Updated > Select from past 1 year till now
    # Case 12 Last Updated > Select more than maximum 3 months
    # Use case Filter > Member Code
    # Case 13 13 By default, no select
    # Case 14 Multiple Select all items
    # Use case Filter > Last Updated By
    # Case 15 By default, no select
    # Case 16 Select Desktop
    # Case 17 Select Mobile
    # Case 18 Multiple Select all items
    # Use case Sort
    # Case 19 Descending by Last Updated By
    # Case 20 Ascending by Last Updated By
    # Case 21 Customize filter
    # Case 22 Pagination
    # Case 23 Export search result
    # Universal test
    # Case 24 Assert the page name == current page name
