import re
import time
import allure

from typing import List, Union, Optional

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webelement import WebElement
from selenium.common.exceptions import TimeoutException, NoSuchElementException

from bo_shared.bo_service_pages import BoService
from bo_shared.bo_router import BoPageRouter
from utils.base_page import BasePage, bypass_login
from utils.decorators import retry_func


class ServiceIndex():
    """
    The class is used for indexing the Services Xpath.
    """
    ALL = 0
    RebateService = 1
    BonusService = 2
    MemberService = 3
    RoleService = 4
    ExcludedGame = 5

    map = {
        "ALL": ALL,
        BoService.Rebate.ServiceName: RebateService,
        BoService.Bonus.ServiceName: BonusService,
        BoService.Member.ServiceName: MemberService,
        BoService.Role.ServiceName: RoleService,
        BoService.ExcludedGame.ServiceName: ExcludedGame,
    }


class BoSharedPage(BasePage):

    PAGE_NAME = BoService.Shared.Shared
    shared_xpath = {
        'service_name': '//ul[@data-testid="navigationMenu"]/li[@data-testid]{service_index}',
        'service_list': '//ul[@data-testid="navigationMenu"]/li[@data-testid]{service_index}//li[@data-testid]'
    }

    def __init__(self, driver: WebDriver):
        super(BoSharedPage, self).__init__(driver, BoPageRouter)

    @allure.step('Click initial page button for visiting the login form')
    def click_continue(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.TAG_NAME, "button"
        ).click()
        self.log.info(f"Click continue button to typing email and password page.")

    @allure.step('Input login email')
    def input_login_email(self, email: str):
        self.find_element_wait(
            EC.visibility_of_element_located,
            By.NAME, "loginfmt"
        ).send_keys(email)
        self.click_submit_button()
        self.log.info(f"Type login email and submit")

    @allure.step('Input login password')
    def input_login_password(self, password: str):
        self.find_element_wait(
            EC.visibility_of_element_located,
            By.NAME, "passwd"
        ).send_keys(password)
        self.click_submit_button()
        self.log.info(f"Type password and submit")

    @allure.step('Click submit button')
    def click_submit_button(self):
        button_locator = (By.XPATH, '//*[@id="idSIButton9"]')
        self.find_element_wait(
            EC.element_to_be_clickable,
            *button_locator
        ).click()

    @retry_func(
        exceptions=(TimeoutException, ),
        retry_times=3,
        retry_time_interval=1
    )
    def is_login_page_loading_success(self) -> bool:
        # Note:
        # Use invisible element also can implement it, but it will hang a lot of time
        result = self.wait_for_is_expect_element_attr_value(
            (By.XPATH, '//div[@id="__next"]//div/div'),
            attr='aria-busy',
            expected_values=None
        )
        return result

    @retry_func(
        exceptions=(TimeoutException,),
        retry_times=5,
        retry_time_interval=1
    )
    @allure.step("Wait for page loading")
    def is_page_loading_success(self) -> bool:
        result = self.wait_for_is_expect_element_attr_value(
            (By.XPATH, '//div[@class="ant-spin-nested-loading"]/div[1]/div'),
            attr='aria-busy',
            expected_values=None
        )
        return result

    @bypass_login(addition_info_key='email')
    def login(self, test_account: dict):
        '''
        Wrap all login steps
        '''
        ignore_visit_pages = {
            BoService.Shared.HomePage,
            BoService.Shared.LogIn,
            BoService.Shared.Shared,
        }
        checked_keys = ['email', 'password']
        for check_key in checked_keys:
            if check_key not in test_account:
                raise KeyError(
                    f"Test account doesn't have {check_key}"
                )
        self.visit_page()
        time.sleep(3)
        self.click_continue()
        time.sleep(1)
        self.input_login_email(test_account['email'])
        time.sleep(1)
        self.input_login_password(test_account['password'])
        time.sleep(1)
        self.click_submit_button()
        if self.PAGE_NAME not in ignore_visit_pages:
            self.visit_page()
        try:
            self.wait_for_is_page_url()
        except TimeoutException as e:
            expected_url = self.url_router.get_page_url(self.PAGE_NAME)
            self.log.error(
                f"Direct to {self.PAGE_NAME} page error, "
                f"the expected_url: {expected_url}, "
                f"but current_url: {self.driver.current_url}"
            )
            raise e

    def click_menu_fold_button(self):
        """
        Click the menu fold button for expanding the service names
        """
        time.sleep(3)
        btn_xpath = '//button[@data-testid="actionCollapse"]'
        btn_element: WebElement = self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            btn_xpath
        )
        check_element: WebElement = btn_element.find_element(By.XPATH, 'span')
        check_attr = check_element.get_attribute('aria-label').strip()
        if check_attr == 'menu-unfold':
            btn_element.click()
            self.log.debug("Click the button to expand service names")

    # Below code is for manipulating the left sidebar
    def click_service_name(self, service_name: str):
        """
        Click the service name which located at the left list bar.

        Ex:
            from bo_shared.bo_service_pages import BoService
            click_service_name(BoService.Role.ServiceName)
        """
        self.click_menu_fold_button()
        if service_name == 'ALL':
            raise ValueError(
                "It only supports click one service at the mean time."
            )
        service_idx = ServiceIndex.map[service_name]
        service_name_xpath = self.xpath_editor(
            self.shared_xpath['service_name'],
            service_index=service_idx
        )
        server_element: WebElement = self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            service_name_xpath
        )
        # Get attribute to check the menu is close or not
        attr = server_element.get_attribute('class')
        attr_content = self.split_attr(attr)
        if 'ant-menu-submenu-open' not in attr_content:
            server_element.click()
            self.log.debug(f'The menu is close, click {service_name} button')

    def get_service_list(
        self,
        service_name: str,
        return_element: bool = False
    ) -> Union[List[str], dict]:
        """
        Get the rebate service function list
        """
        if service_name in (BoService.ExcludedGame.ServiceName, ):
            raise ValueError(f"{service_name} doesn't have service list.")

        self.click_service_name(service_name)
        service_index = ServiceIndex.map[service_name]
        list_xpath = self.xpath_editor(
            self.shared_xpath['service_list'],
            service_index=service_index
        )
        list_elements: List[WebElement] = self.find_element_wait(
            EC.visibility_of_all_elements_located,
            By.XPATH,
            list_xpath
        )
        service_info = dict()
        for element in list_elements:
            element_name = self.normalize_string(element.text.strip())
            element_role = element.get_attribute('role')
            service_info[element_name] = element
            # When the element role is none, it indicate that the element has sub items
            if element_role != 'none':
                continue
            # Check the sub item
            is_open_attr = element.get_attribute('class')
            is_open_attr = self.split_attr(is_open_attr)
            if 'ant-menu-submenu-open' not in is_open_attr:
                element.click()
            time.sleep(1)
            sub_menu_elements: List[WebElement] = element.find_elements(
                By.XPATH, 'ul/li[@data-testid]'
            )
            for sub_element in sub_menu_elements:
                sub_item_name = sub_element.get_attribute('data-testid')
                # Get the last two elements
                sub_item_name = '/'.join(sub_item_name.split('/')[-2:])
                sub_item_name = sub_item_name.replace('-', '_')
                service_info[sub_item_name] = sub_element

        if return_element:
            return service_info
        else:
            return list(service_info.keys())

    def click_service_func_list(
        self,
        service_name: str,
        sub_item_name: Optional[str] = None
    ):
        """
        Click the specific service function

        Ex:
            click_service_func_list(BoService.Role.ServiceName, BoService.Role.UserManagement)
        """
        self.click_service_name(service_name)
        if sub_item_name is None:
            return
        # Only when `sub_item_name` is not Noe, then it can execute the below.
        service_info = self.get_service_list(service_name, True)
        try:
            target_element: WebElement = service_info[sub_item_name]
        except KeyError:
            raise KeyError(
                f"Unknown sub_item_name: {sub_item_name}, the {service_name} "
                f"only has {list(service_info.keys())} functions."
            )
        target_element.click()
        self.log.debug(f"Successfully click {sub_item_name} in the {service_name}")

    def get_all_service_names(
        self,
        return_element: bool = False
    ) -> Union[List[WebElement], List[str]]:
        """
        Get all service names
        """
        self.click_menu_fold_button()
        service_names_xpath = self.xpath_editor(
            self.shared_xpath['service_name'],
            service_index=ServiceIndex.ALL
        )
        service_eles: List[WebElement] = self.find_element_wait(
            EC.visibility_of_all_elements_located,
            By.XPATH,
            service_names_xpath
        )
        if return_element:
            return service_eles
        else:
            service_names = [
                self.normalize_string(ele.text)\
                for ele in service_eles
            ]
            return service_names

    def get_current_page_name(self, include_parents: bool = False) -> str:
        """
        Get current page name.

        Note that the home page is empty string
        """
        page_name_xpath = '//nav[@data-testid="breadcrumb"]/ol'
        page_name_element: WebElement = self.find_element_wait(
            EC.visibility_of_element_located,
            By.XPATH,
            page_name_xpath
        )
        new_line_pat = self.shared_regex['new_line_pat']
        page_name = new_line_pat.sub('', page_name_element.text)
        if include_parents:
            page_name = page_name.strip()
        else:
            page_name = page_name.split('/')[-1].strip()

        return page_name

    @retry_func(
        exceptions=(
            TimeoutException,
            NoSuchElementException,
        ),
        retry_times=3
    )
    def logout(self):
        """
        Logout the current account
        """
        setting_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionSetting"]'
        )
        self.action_chains.move_to_element(setting_element).perform()
        self.log.debug("Move to the setting button")

        check_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@data-testid="signOut"]/parent::ul/parent::div'
        )
        class_attrs = check_element.get_attribute('class')
        class_attrs = self.split_attr(class_attrs)

        if 'ant-dropdown-hidden' not in class_attrs:
            sign_out_element: WebElement = self.find_element_wait(
                EC.presence_of_element_located,
                By.XPATH,
                '//li[@data-testid="signOut"]'
            )
            sign_out_element.click()
            self.driver.delete_all_cookies()
            self.log.debug("Click the sign out element and delete all cookies")
        else:
            raise NoSuchElementException("The setting button is still hidden")