#!/usr/bin/env python3

import os
import math
import click
import pytest

from dotenv import load_dotenv

from utils.report import AllureReport,  HTMLReport
from utils.utils import SystemLogger

log = SystemLogger.get_logger()

load_dotenv()

@click.command()
@click.argument('file-dir', nargs=-1, type=click.Path())
@click.option(
    '-p', '--execute-pat', 'execute_pat', type=str, default=None,
    help='The pattern of invoking pytest execute test suit. Ex: -m `test`'
)
@click.option(
    '--html-report', 'html_report', type=click.Path(),
    default=None, help='The path of the html report'
)
@click.option(
    '--allure-report', 'allure_report', type=click.Path(),
    default=None, help='The path of the allure report'
)
@click.option(
    '-p', '--parallel', 'parallel', type=bool, default=False,
    help='Whether using multiple-process to execute test suit'
)
@click.option(
    '-n', '--num-process', 'num_process', type=str, default='auto',
    help='Number of process that you want to use, the default value is auto'
)
@click.option(
    '-notification', '--invoke-setting-notification', 'notification',
    type=bool, default=False, help='whether invoking the notification'
)
def execute_testing(
        file_dir,
        execute_pat,
        html_report,
        allure_report,
        parallel,
        num_process,
        notification
    ):
    if not (file_dir or execute_pat):
        raise ValueError(
            f"Both of `file_dir` and `execute_pat` are empty. "
            f"One of `file_dir` and `execute_pat` must have value"
        )

    # Default execute command line
    exe_cli = ['--verbose', '--setup-show']
    if file_dir:
        exe_cli.extend(list(file_dir))

    if execute_pat:
        exe_cli.extend(execute_pat)

    if allure_report is None:
        allure_report = AllureReport.ALLURE_REPORT_DIR
    exe_cli.extend([
        f'--alluredir={allure_report}',
        '--clean-alluredir'
    ])

    if html_report is None:
        html_report = HTMLReport()
        html_report_name = html_report.get_report_output_full_path()
    exe_cli.extend([
        f'--html={html_report_name}',
        '--self-contained-html',
    ])

    if parallel:
        num_cpu = os.cpu_count()
        max_worker = math.ceil(num_cpu/2)
        xdist_args = [
            '--dist', 'loadgroup',
            '-n', str(num_process),
            '--max-worker-restart', str(max_worker)
        ]
        exe_cli.extend(xdist_args)

    if notification:
        invoke_notification = ['--invoke-setting-notification', str(True)]
        exe_cli.extend(invoke_notification)

    log.debug(f"Start executing cli:\n pytest {' '.join(exe_cli)}")
    pytest.main(exe_cli)

if __name__ == '__main__':
    execute_testing()

# Ex:
# Run test_setup all test
# python main.py ./test_setup/ -p True -n 3 -notification True