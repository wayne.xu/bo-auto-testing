from typing import List
from copy import deepcopy

import allure
from bo_shared.components.radio_form import RadioForm
from selenium.webdriver.remote.webdriver import WebDriver


CHECK_BOX_INFO = {
    'platform': {
        'form_data_test_id': 'platforms',
        'options_data_test_id': 'platforms',
    },
}


class CheckBoxForm(RadioForm):

    check_selectable_attr = "ant-checkbox-wrapper-checked"

    def __init__(
        self,
        driver: WebDriver,
        form_name: str,
        form_data_test_id: str,
        options_data_test_id: str
    ):
        super(CheckBoxForm, self).__init__(
            driver,
            form_name,
            form_data_test_id,
            options_data_test_id
        )

    @allure.step("Select CheckBoxForm options")
    def select_options(self, option_names: List[str]):
        """
        Select the multiple options

        Args:
            option_names (List[str])
        """
        options_info = self.get_form_options()
        wait_for_select_names = deepcopy(option_names)
        for option_name in option_names:
            if option_name not in options_info:
                raise ValueError(
                    f"option_name: {option_name} doesn't exist "
                    f"in {list(options_info.keys())}"
                )
            info = options_info[option_name]
            if info['is_selectable']:
                self.action_chains.click(info['element']).perform()
                wait_for_select_names.remove(option_name)
                self.log.debug(
                    f"Select the option: {option_name} on "
                    f"{self.__class__.__name__}"
                )

            if len(wait_for_select_names) == 0:
                return None