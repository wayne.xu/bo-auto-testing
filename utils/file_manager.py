import os
import json
import time
import uuid
import base64
import shutil

from pathlib import Path
from datetime import datetime
from functools import wraps
from typing import Optional, Any, List, Callable

from utils.decorators import lock_file
from exceptions.errors import GetTokenError
from utils.utils import (
    SystemLogger, HelperFuncs,
    SingletonMeta, ZipTools, S3Bucket
)


class BaseFileManagement(metaclass=SingletonMeta):

    log = SystemLogger.get_logger()

    @classmethod
    def create_dir(cls, _dir: Path):
        path_ = Path(_dir)
        if path_.exists() == False:
            path_.mkdir(parents=True, exist_ok=True)

    @classmethod
    def remove_dir(cls, _dir: Path, forced_delete: bool = False):
        """
        Remove the given directory

        Args:
            _dir (Path): The path of the directory
            force_delete (bool, optional): If it's True,
            it will delete the directory no mater it contains files or not. Defaults to False.
        """
        dir_files = os.listdir(_dir)
        if dir_files and forced_delete:
            shutil.rmtree(_dir)
            cls.log.debug(
                f"Delete directory: {_dir} and "
                f"it contains {len(dir_files)} files"
            )
        else:
            os.rmdir(_dir)
            cls.log.debug(f"Remove the empty directory: {_dir}")

    @staticmethod
    def delete_dir_files_by_time(
        _dir: Path,
        exceed_time: int,
        delete_file_exts: Optional[set] = None,
        use_modify_time: bool = True
    ) -> List[Path]:
        if not os.path.exists(_dir):
            raise OSError(f"The directory: {_dir} doesn't exist")

        if use_modify_time:
            get_time_fuc = os.path.getmtime
        else:
            get_time_fuc = os.path.getctime

        current_time = datetime.now()
        file_names = os.listdir(_dir)
        removed_files = []
        for file_name in file_names:
            _, ext = os.path.splitext(file_name)
            if ext not in delete_file_exts and\
                delete_file_exts is not None:
                continue
            check_file_path = os.path.join(_dir, file_name)
            ftime = get_time_fuc(check_file_path)
            ftime = datetime.fromtimestamp(ftime)
            diff_time = (current_time - ftime).seconds
            if diff_time >= exceed_time:
                os.remove(check_file_path)
                removed_files.append(check_file_path)
        return removed_files

    @classmethod
    def upload_dir_to_s3(
        cls,
        dir_path: str,
        zip_output: Optional[str] = None,
        upload_bucket_name: Optional[str] = None,
        upload_delete: bool = True
    ) -> str:
        """
        Upload the directory to S3 after compressing the given directory

        Args:
            dir_path (Path): The path of directory
            upload_bucket_name (Optional[str], optional): Defaults to None.

        Returns:
            str: object_url
        """
        object_url = ""
        if not os.listdir(dir_path):
            os.rmdir(dir_path)
            cls.log.debug(f"Remove the empty directory, path: {dir_path}")
            return object_url

        if (os.path.exists(dir_path) and os.path.isdir(dir_path)) == False:
            raise FileExistsError(
                f"Given path must be a directory and exists. "
                f"error dir_path: {dir_path}"
            )
        output_path = ZipTools.zip_directory(dir_path, zip_output)
        s3_bucket = S3Bucket()
        object_url = s3_bucket.upload_file(output_path, bucket_name=upload_bucket_name)
        if upload_delete:
            # Delete the directory no matter it has files or not
            if os.path.exists(dir_path):
                cls.remove_dir(dir_path, True)
            # Delete the compression file
            if os.path.exists(output_path):
                os.remove(output_path)
                cls.log.debug(
                    "Delete the zip file after uploading dir to s3 "
                    f"delete file path: {output_path}"
                )
        return object_url

    @classmethod
    def _convert_qualname_to_fname(cls, qualname: str, file_ext: str) -> str:
        """
        Convert function __qualname__ to file name

        Args:
            qualname (str): function __qualname__ string
        """
        split_result = qualname.split('.')
        fname = '_'.join(split_result)
        fname = f"{fname}.{file_ext.strip('.')}"
        return fname


class CookiesManagement(BaseFileManagement):
    """
    The class is used for loading and saving cookies.
    """
    FILE_EXT = 'json'
    DIR = os.path.join(os.getcwd(), 'cookies')
    COOKIES_PATH = os.path.join(DIR, f'cookies.{FILE_EXT}')

    def __init__(self):
        self.create_dir(self.DIR)

    # Maybe need redis instead of it.
    def load_cookies(
        self,
        cookies_path: Optional[str] = None
    ) -> dict:
        if cookies_path is None:
            cookies_path = self.COOKIES_PATH
        cookies = dict()
        if os.path.exists(cookies_path):
            try:
                with open(cookies_path, 'r') as rf:
                    cookies = json.load(rf)
                cookies = cookies['cookies']
            except Exception as e:
                self.log.error(f"ReadCookiesError:\n {e}")
        return cookies

    @lock_file(timeout=30, use_args_hash=True)
    def save_cookies(
        self,
        cookies: dict,
        save_path: Optional[str] = None,
        addition_info: Optional[str] = None
    ) -> bool:
        if save_path is None:
            save_path = self.COOKIES_PATH
        result = True
        save_data = {
            'cookies': cookies,
            'save_timestamp': int(time.time()),
            'addition_info': addition_info,
        }
        try:
            with open(save_path, 'w') as wf:
                json.dump(save_data, wf)
        except Exception as e:
            self.log.error(f"SaveCookiesError:\n {e}")
            result = False
        return result


class TokenManagement(BaseFileManagement):
    """
    The class is used for loading and saving tokens.
    """
    FILE_EXT = 'json'
    DIR = os.path.join(os.getcwd(), 'tokens')

    def __init__(self):
        self.create_dir(self.DIR)

    def load_token_info(self, token_file_path: str) -> str:
        """
        Load token information by `token_file_path`

        Args:
            token_file_path (str)
        """
        if os.path.exists(token_file_path) == False:
            raise FileExistsError(
                f"The file: {token_file_path} doesn't exist"
            )
        token = ""
        token_info = dict()
        try:
            with open(token_file_path, 'r') as rf:
                token_info = json.load(rf)
        except Exception as e:
            self.log.error(f"ReadTokenError:\n {e}")
        if token_info:
            token = token_info['token']
        return token

    @lock_file(timeout=10, use_args_hash=True)
    def save_token(
        self,
        token: str,
        save_path: str,
        addition_info: Optional[Any] = None
    ) -> bool:
        """
        Save token to `save_path`

        Args:
            token (str)
            save_path (str)
            addition_info (Optional[Any], optional). Defaults to None.
        """
        save_data = {
            'token': token,
            'save_timestamp': int(datetime.utcnow().timestamp()),
            'addition_info': addition_info,
        }
        save_result = True
        try:
            with open(save_path, 'w') as wf:
                json.dump(save_data, wf)
            self.log.debug(
                f"Successfully save token_info: {save_data} "
                f"into {save_path}"
            )
        except Exception as e:
            self.log.error(f"SaveTokenError:\n {e}")
            save_result = False
        return save_result

    @classmethod
    def get_file_path_by_hash_data(cls, hash_data: dict) -> str:
        """
        Get token file by data(The data before hashing)

        Args:
            hash_data (dict)
        """
        hash_identifier = HelperFuncs.hash_dict(hash_data)
        return cls.get_file_path_by_hash(hash_identifier)

    @classmethod
    def get_file_path_by_hash(cls, hash_identifier: str) -> str:
        """
        Get file path by hash identifier

        Args:
            hash_identifier (str)
        """
        file_name = f'{hash_identifier}.{cls.FILE_EXT}'
        file_path = os.path.join(cls.DIR, file_name)
        return file_path

    def get_token_by_hash(self, hash_identifier: str) -> str:
        """
        Get token by hash_identifier

        Args:
            hash_identifier (str)
        """
        file_path = self.get_file_path_by_hash(hash_identifier)
        token = self.load_token_info(file_path)
        if not token:
            raise GetTokenError(
                f'Getting token occurs errors, the `hash_identifier`: {hash_identifier}, '
                f'and file_path: {file_path}'
            )
        return token

    def get_token_by_hash_data(self, hash_data: dict) -> str:
        """
        Get token by hash_data(The data before hashing)

        Args:
            hash_data (dict)
        """
        hash_identifier = HelperFuncs.hash_dict(hash_data)
        token = self.get_token_by_hash(hash_identifier)
        if not token:
            raise GetTokenError(
                f"Getting token occurs errors, the hash_data: {hash_data}"
            )
        return token

    @staticmethod
    def is_token_expired(file_path: str, expired_time: int) -> bool:
        current_time = int(time.time())
        file_mtime = int(os.path.getmtime(file_path))
        if (current_time - file_mtime) >= expired_time:
            return True
        else:
            return False


class BrowserLogManagement():
    """
    This is outdate class. It needs to modify
    """

    FILE_EXTENSION = '.json'
    ALLOW_LOG_TYPE = {'performance', 'browser', 'driver'}
    DIR_PATH = os.path.join(os.getcwd(), 'browser_logs')

    log = SystemLogger.get_logger()

    @classmethod
    def create_log_dir(cls):
        """
        Create a log directory if it doesn't exist
        """
        Path(cls.DIR_PATH).mkdir(parents=True, exist_ok=True)

    @classmethod
    def get_fname(cls, log_type: str) -> str:
        """Get the file name

        Args:
            log_type (str): browser, client, driver, performance, server
        """
        _uuid = str(uuid.uuid4()).replace('-', '')
        t = datetime.utcnow().strftime("%Y%m%d_%H%M%S")
        return f"{log_type}_{t}_{_uuid}{cls.FILE_EXTENSION}"

    @classmethod
    @lock_file(use_args_hash=True)
    def output_json(cls, data: dict, output_path: str):
        """Write log to specific path

        Args:
            data (dict): The data that you want to output to json
            output_path (str): Output file path
        """
        try:
            serialize_logs = json.dumps(data)
            with open(output_path, 'w') as wf:
                wf.write(serialize_logs)
            cls.log.debug(f"Successfully output log to {output_path}")
        except Exception as e:
            cls.log.error(f"Can't output the data to {output_path}")
            raise e

    @classmethod
    def export_log(cls, log_type: str, data: dict, output_path: str = None):
        cls.create_log_dir()
        if output_path is None:
            fname = cls.get_fname(log_type)
            output_path = os.path.join(cls.DIR_PATH, fname)
        else:
            _, ext = os.path.splitext(output_path)
            if ext != cls.FILE_EXTENSION:
                raise ValueError(
                    "The file type of output file must be json format."
                )
        cls.output_json(data, output_path)

    @classmethod
    def log_exporter(
            cls,
            log_type: str = "performance",
            output_path: Optional[str] = None,
        ) -> Callable:
        """The decorator is used for exporting the browser log
         while executing the selenium. But when the error is occurring,
         the programming will jump to conftest.py driver function.
         So you can use this function to record the log when the error is occurring.

        Args:
            log_type (str, optional): Defaults to "performance".
            output_path (Optional[str], optional): Output file path, Defaults to None.
            export_on_error (bool, optional): . Defaults to False.
        """
        cls.create_log_dir()
        if log_type not in cls.ALLOW_LOG_TYPE:
            raise ValueError(
                f"Invalid `{log_type}` log_type, "
                f"it must be one of the {cls.ALLOW_LOG_TYPE}"
            )
        if output_path is None:
            fname = cls.get_fname(log_type)
            output_path = os.path.join(cls.DIR_PATH, fname)
        else:
            _, ext = os.path.splitext(output_path)
            if ext != cls.FILE_EXTENSION:
                raise ValueError(
                    "The file type of `output_path` must be json format."
                )
        def decorate_func(func: Callable) -> Callable:
            @wraps(func)
            def wrapper_func(self, *args, **kwargs):
                if not hasattr(self, 'driver'):
                    raise AttributeError(
                        "It doesn't have driver attribute to export the logs"
                    )
                try:
                    result = func(self, *args, **kwargs)
                    logs = self.driver.get_log(log_type)
                    cls.output_json(logs, output_path)
                except Exception as e:
                    raise e
                return result
            return wrapper_func
        return decorate_func
