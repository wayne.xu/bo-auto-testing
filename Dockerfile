FROM python:3.8-slim-buster AS base
LABEL maintainer="developer" vesion=1.0

# Never prompt the user for choices on installation/configuration of packages
ENV DEBIAN_FRONTEND noninteractive
ENV TERM linux

# Define en_US.
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LC_CTYPE en_US.UTF-8
ENV LC_MESSAGES en_US.UTF-8

RUN set -ex \
    && buildDeps=' \
        freetds-dev \
        libkrb5-dev \
        libsasl2-dev \
        libssl-dev \
        libffi-dev \
        libpq-dev \
        git \
    ' \
    && apt-get update -yqq \
    && apt-get upgrade -yqq \
    && apt-get install -yqq --no-install-recommends \
        $buildDeps \
        freetds-bin \
        build-essential \
        default-libmysqlclient-dev \
        apt-utils \
        unzip\
        curl \
        rsync \
        netcat \
        locales \
        wget\
        gnupg2\
        gnupg\
        nano\
    && sed -i 's/^# en_US.UTF-8 UTF-8$/en_US.UTF-8 UTF-8/g' /etc/locale.gen \
    && locale-gen \
    && update-locale LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8 \
    # Clean redundant files
    && apt-get purge --auto-remove -yqq $buildDeps \
    && apt-get autoremove -yqq --purge \
    && apt-get clean \
    && rm -rf \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/* \
        /usr/share/man \
        /usr/share/doc \
        /usr/share/doc-base

# Define Args
ARG BO_AUTO_TESTING_HOME=/opt/bo-auto-testing
ENV BO_AUTO_TESTING_HOME=${BO_AUTO_TESTING_HOME}

# Upgrade pip to the latest vesion and install requirement packages
COPY . ${BO_AUTO_TESTING_HOME}/.

RUN pip install --upgrade pip\
    && pip install -r ${BO_AUTO_TESTING_HOME}/requirements.txt

WORKDIR ${BO_AUTO_TESTING_HOME}

CMD [ "python3" ]
