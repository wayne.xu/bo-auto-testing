import allure

from typing import List, Union
from datetime import datetime

from bo_shared.components.basic import BasicComponent

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC


DATE_PICKER_INFO = {
    # Dashboard Page
    'chart_filter_date': {
        'data_test_id': 'dateFrom-dateTo',
    },
    # Management Page
    'active_dates': {
        'data_test_id': 'activeDates'
    },
    'last_calculation_time': {
        'data_test_id': 'lastCalculationTime'
    },
    'last_updated_time': {
        'data_test_id': 'lastUpdatedTime'
    },
    # Add rebate page
    'period': {
        'data_test_id': 'period'
    },
}


class DatePicker(BasicComponent):

    start_date_xpath_format = (
        '(//div[@data-testid="{data_test_id}"]//'
        'div[@class="ant-picker-input ant-picker-input-active" '
        'or @class="ant-picker-input"]/input)[1]'
    )
    end_date_xpath_format = (
        '(//div[@data-testid="{data_test_id}"]//'
        'div[@class="ant-picker-input ant-picker-input-active" '
        'or @class="ant-picker-input"]/input)[2]'
    )
    clear_input_xpath_format = (
        '//div[@data-testid="{data_test_id}"]'
        '//div[@class="ant-form-item-control-input-content"]/div/span'
    )
    check_date_picker_expand_xpath_format = (
        '//div[@data-testid="{data_test_id}"]'
        '//div[@class="ant-form-item-control-input-content"]/div'
    )
    date_picker_header_xpath_format = (
        '(//div[@class="ant-picker-panels"]){index}'
        '//div[@class="ant-picker-date-panel"]'
        '//button[not(@style)]'
    )
    date_picker_content_xpath_format = (
        '(//div[@class="ant-picker-panels"]){index}'
        '//table[@class="ant-picker-content"]//tr/td[@title]'
    )
    # Get the xpath of half side content
    date_pick_half_side_content_xpath_format = (
        '((//div[@class="ant-picker-panels"]){index}'
        '//table[@class="ant-picker-content"]){left_or_right}'
        '//tr/td[@title]'
    )
    month_panel_header_xpath_format = (
        '(//div[@class="ant-picker-month-panel"]){index}'
        '/div[@class="ant-picker-header"]//button'
    )
    month_panel_content_xpath_format = (
        '(//div[@class="ant-picker-month-panel"]){index}'
        '//table[@class="ant-picker-content"]//tr/td[@title]'
    )
    year_panel_header_xpath_format = (
        '(//div[@class="ant-picker-year-panel"]){index}'
        '/div[@class="ant-picker-header"]//button'
    )
    year_panel_content_xpath_format = (
        '(//div[@class="ant-picker-year-panel"]){index}'
        '//table[@class="ant-picker-content"]//tr/td[@title]'
    )
    header_btn_type_map = {
        'ant-picker-header-super-prev-btn': 'preview_year',
        'ant-picker-header-prev-btn': 'preview_month',
        'ant-picker-month-btn': 'month',
        'ant-picker-year-btn': 'year',
        'ant-picker-header-next-btn': 'next_month',
        'ant-picker-header-super-next-btn': 'next_year',
    }

    def __init__(
        self,
        driver: WebDriver,
        date_picker_name: str,
        data_test_id: str,
        index: int = 0
    ):
        super(DatePicker, self).__init__(driver)
        self.date_picker_name = date_picker_name
        self.data_test_id = data_test_id
        self.index = index
        self._format_all_xpath()

    def _format_all_xpath(self):
        self.start_date_xpath = self.start_date_xpath_format.format(
            data_test_id=self.data_test_id
        )
        self.end_date_xpath = self.end_date_xpath_format.format(
            data_test_id=self.data_test_id
        )
        self.clear_input_xpath = self.clear_input_xpath_format.format(
            data_test_id=self.data_test_id
        )
        self.check_date_picker_expand_xpath =\
            self.check_date_picker_expand_xpath_format.format(
            data_test_id=self.data_test_id
        )
        self.date_picker_header_xpath = self.xpath_editor(
            self.date_picker_header_xpath_format,
            index=self.index
        )
        self.date_picker_content_xpath = self.xpath_editor(
            self.date_picker_content_xpath_format,
            index=self.index
        )
        self.month_panel_content_xpath = self.xpath_editor(
            self.month_panel_content_xpath_format,
            index=self.index
        )
        self.month_panel_header_xpath = self.xpath_editor(
            self.month_panel_header_xpath_format,
            index=self.index
        )
        self.year_panel_header_xpath = self.xpath_editor(
            self.year_panel_header_xpath_format,
            index=self.index
        )
        self.year_panel_content_xpath = self.xpath_editor(
            self.year_panel_content_xpath_format,
            index=self.index
        )
        self.date_pick_start_date_side_content_xpath = self.xpath_editor(
            self.date_pick_half_side_content_xpath_format,
            index=self.index,
            left_or_right=1
        )
        self.date_pick_end_date_side_content_xpath = self.xpath_editor(
            self.date_pick_half_side_content_xpath_format,
            index=self.index,
            left_or_right=2
        )

    @allure.step("Check date picker is expanded or not")
    def is_date_picker_expanded(self) -> bool:
        check_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            self.check_date_picker_expand_xpath
        )
        class_attrs = check_element.get_attribute('class').split(' ')
        if 'ant-picker-focused' in class_attrs:
            return True
        else:
            return False

    def _get_target_elements(self, xpath: str) -> List[WebElement]:
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            xpath
        )
        return elements

    def _click_target_element(self, xpath: str):
        """
        Click the given xpath element

        Args:
            xpath (_type_)
        """
        element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            xpath
        )
        element.click()

    def __get_date_input_value(self, xpath: str):
        element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            xpath
        )
        date_ = element.get_attribute('value').strip()
        return date_

    @allure.step("Get the start date input value")
    def get_start_date_value(
        self,
        to_datetime_type: False,
        date_format: str = '%Y-%m-%d'
    ) -> Union[str, datetime]:
        _date = self.__get_date_input_value(self.start_date_xpath)
        if to_datetime_type:
            _date = datetime.strptime(_date, date_format)
        return _date

    @allure.step("Get the end date input value")
    def get_end_date_value(
        self,
        to_datetime_type: False,
        date_format: str = '%Y-%m-%d'
    )-> Union[str, datetime]:
        _date = self.__get_date_input_value(self.end_date_xpath)
        if to_datetime_type:
            _date = datetime.strptime(_date, date_format)
        return _date

    @allure.step("Click the start date input")
    def click_start_date_input(self):
        if self.is_date_picker_expanded() == False:
            self._click_target_element(self.start_date_xpath)

    @allure.step("Click the end date input")
    def click_end_date_input(self):
        if self.is_date_picker_expanded() == False:
            self._click_target_element(self.end_date_xpath)

    @allure.step("Click the clear button")
    def click_clear_btn(self):
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            self.clear_input_xpath
        )
        if len(elements) == 2:
            elements[-1].click()
        else:
            self.log.debug(
                "The start date and end date elements haven't been selected"
            )

    def check_current_panel_type(self) -> str:
        """
        Check the type of panel

        Returns:
            str: `date`, `month`, `year`
        """
        check_xpath = '(//div[@class="ant-picker-panels"]/div/div)[1]'
        element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            check_xpath
        )
        type_ = element.get_attribute('class').split('-')[-2]
        return type_

    def __check_process(self, record_list: list, _data: str) -> list:
        if _data == 'year':
            record_list[0] = 1
        elif _data == 'month':
            record_list[1] = 1
        return record_list

    def _get_date_panel_header_btn(self) -> dict:
        header_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            self.date_picker_header_xpath
        )
        date_panel_headers = dict()
        prefix = 'start'
        record_list = [0, 0]
        for element in header_elements:
            attr = element.get_attribute('class').strip()
            btn_type = self.header_btn_type_map[attr]
            if btn_type in ('month', 'year'):
                date_panel_headers[f'{prefix}_{btn_type}'] = element
            else:
                date_panel_headers[btn_type] = element

            record_list = self.__check_process(record_list, btn_type)
            # When the elements of the list all equal to 1, then it convert prefix value.
            if all(record_list):
                record_list = [0, 0]
                prefix = 'end'
        return date_panel_headers

    @allure.step("Click the date panel header")
    def click_date_panel_header(self, btn_type: str):
        """
        Click the date panel headers

        Args:
            btn_type (str): `preview_year`, `preview_month`, `start_month`,
            `start_year`, `end_month`, `end_year`, `next_month`, `next_year`
        """
        type_ = self.check_current_panel_type()
        if type_ != 'date':
            raise TypeError("Current panel type isn't `date`")
        date_panels_headers = self._get_date_panel_header_btn()
        try:
            element: WebElement = date_panels_headers[btn_type]
            element.click()
            self.log.debug(f"Click the {btn_type} button on date panel header")
        except KeyError:
            raise KeyError(
                f"The {btn_type} button doesn't exist in "
                f"date panel headers: {list(date_panels_headers.keys())}"
            )

    def get_date_picker_all_elements(self) -> List[WebElement]:
        """
        Get all date picker all elements
        """
        content_elements = self._get_target_elements(
            self.date_picker_content_xpath
        )
        return content_elements

    def get_date_picker_start_side_elements(self) -> List[WebElement]:
        content_elements = self._get_target_elements(
            self.date_pick_start_date_side_content_xpath
        )
        return content_elements

    def get_date_picker_end_side_elements(self) -> List[WebElement]:
        content_elements = self._get_target_elements(
            self.date_pick_end_date_side_content_xpath
        )
        return content_elements

    def _get_selectable_content(self, content_elements: List[WebElement]) -> dict:
        all_contents = dict()
        for content_element in content_elements:
            attrs = content_element.get_attribute('class').split(' ')
            if 'ant-picker-cell-disabled' not in attrs:
                title = content_element.get_attribute('title')
                all_contents[title] = content_element
        return all_contents

    @allure.step("Get date picker selectable content")
    def get_date_picker_selectable_content(self) -> dict:
        """
        Get date picker all selectable content
        """
        content_elements = self.get_date_picker_all_elements()
        all_content = self._get_selectable_content(content_elements)
        return all_content

    @allure.step("Get date picker start date side selectable content")
    def get_date_picker_start_side_selectable_content(self) -> dict:
        """
        Get date picker start side selectable content
        """
        content_elements = self.get_date_picker_start_side_elements()()
        all_content = self._get_selectable_content(content_elements)
        return all_content

    @allure.step("Get date picker end date side selectable content")
    def get_date_picker_end_side_selectable_content(self) -> dict:
        """
        Get date picker end side selectable content
        """
        content_elements = self.get_date_picker_end_side_elements()
        all_content = self._get_selectable_content(content_elements)
        return all_content

    @allure.step("Get the date picker current date")
    def get_date_picker_current_date(self) -> dict:
        """
        Get date picker current date
        """
        content_elements = self.get_date_picker_all_elements()
        current_date = dict()
        for content_element in content_elements:
            attrs = content_element.get_attribute('class').split(' ')
            if 'ant-picker-cell-today' in attrs:
                title = content_element.get_attribute('title')
                current_date = {
                    title: content_element
                }
                return current_date
        return current_date

    @allure.step("Select start date and end date on the date picker")
    def select_date_picker(self, start_date: str, end_date: str):
        """
        Select the date picker data, and the input with `%Y-%m-%d` format

        Args:
            start_date (str)
            end_date (str)
        """
        date_format = '%Y-%m-%d'
        if (
            datetime.strptime(start_date, date_format) >
             datetime.strptime(end_date, date_format)
        ):
            raise ValueError(
                f"The start_date: {start_date} must be "
                f"smaller than end_date: {end_date}."
            )
        type_ = self.check_current_panel_type()
        if type_ != 'date':
            raise TypeError("Current panel type isn't `date`")

        all_dates = self.get_date_picker_selectable_content()
        click_dates = [
            ('start_date', start_date),
            ('end_date', end_date),
        ]
        for key, value in click_dates:
            try:
                element: WebElement = all_dates[value]
                element.click()
                self.log.debug(f"Click the {key}: {value}")
            except KeyError:
                raise KeyError(
                    f"The {key}: {value} doesn't exist in "
                    f"{list(all_dates.keys())}"
                )

    def get_month_panel_all_elements(self) -> dict:
        """
        Get month panel elements without unselectable.
        """
        content_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            self.month_panel_content_xpath
        )
        all_elements = dict()
        for content_element in content_elements:
            attrs = content_element.get_attribute('class')
            if 'ant-picker-cell-in-view' in attrs:
                title = content_element.get_attribute('title')
                all_elements[title] = content_element
        return all_elements

    @allure.step("Select target year month on month panel")
    def select_month_panel(self, year_month: str):
        """
        Select the `year-month` on month panel

        Args:
            year_month (str)
        """
        year_month_format = '%Y-%m'
        try:
            datetime.strptime(year_month, year_month_format)
        except ValueError:
            raise ValueError(
                f"Unknown year_month='{year_month}' format. It must "
                f"conform the `{year_month_format}` format"
            )
        type_ = self.check_current_panel_type()
        if type_ != 'month':
            raise TypeError("Current panel type isn't `month`")

        all_elements = self.get_month_panel_all_elements()
        try:
            element: WebElement = all_elements[year_month]
            element.click()
            self.log.debug(f"Click the month panel: {year_month}")
        except KeyError:
            raise KeyError(
                f"Input year_month: {year_month} doesn't exist in "
                f"{list(all_elements.keys())}"
            )

    def _get_month_panel_header_btn(self) -> dict:
        all_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            self.month_panel_header_xpath
        )
        month_panel_headers = dict()
        for element in all_elements:
            attr = element.get_attribute('class').strip()
            btn_type = self.header_btn_type_map[attr]
            month_panel_headers[btn_type] = element
        return month_panel_headers

    @allure.step("Click the month panel header button")
    def click_month_panel_header_btn(self, btn_type: str):
        type_ = self.check_current_panel_type()
        if type_ != 'month':
            raise TypeError("Current panel type isn't `month`")

        month_headers = self._get_month_panel_header_btn()
        try:
            element: WebElement = month_headers[btn_type]
            element.click()
            self.log.debug(f"Click the month panel header: {btn_type}")
        except KeyError:
            raise KeyError(
                f"Can't find the {btn_type} in the "
                f"month panel header: {list(month_headers.keys())}"
            )

    def _get_year_panel_header_btn(self) -> dict:
        all_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            self.year_panel_header_xpath
        )
        year_panel_headers = dict()
        for element in all_elements:
            attr = element.get_attribute('class').strip()
            btn_type = self.header_btn_type_map[attr]
            year_panel_headers[btn_type] = element
        return year_panel_headers

    @allure.step("Click the year panel header button")
    def click_year_panel_header_btn(self, btn_type: str):
        type_ = self.check_current_panel_type()
        if type_ != 'year':
            raise TypeError("Current panel type isn't `year`")

        year_headers = self._get_year_panel_header_btn()
        try:
            element: WebElement =  year_headers[btn_type]
            element.click()
            self.log.debug(f"Click the year panel header: {btn_type}")
        except KeyError:
            raise KeyError(
                f"Can't find the {btn_type} in the "
                f"year panel header: {list(year_headers.keys())}"
            )

    def get_year_panel_selectable_all_elements(self) -> dict:
        content_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            self.year_panel_content_xpath
        )
        all_elements = dict()
        for content_element in content_elements:
            attrs = content_element.get_attribute('class')
            if 'ant-picker-cell-in-view' in attrs:
                title = content_element.get_attribute('title')
                all_elements[title] = content_element
        return all_elements

    @allure.step('Select the target year on year panel')
    def select_year_panel(self, year: str):
        type_ = self.check_current_panel_type()
        if type_ != 'year':
            raise TypeError("Current panel type isn't `year`")

        all_elements = self.get_year_panel_selectable_all_elements()
        try:
            element: WebElement = all_elements[year]
            element.click()
            self.log.debug(f"Click the year panel: {year}")
        except KeyError:
            raise KeyError(
                f"Input year_month: {year} doesn't exist in "
                f"{list(all_elements.keys())}"
            )

    def __repr__(self):
        return (
            f"< {self.date_picker_name} DatePicker Component, "
            f"data_test_id: {self.data_test_id}, {self.index} >"
        )
