import time

import allure

from typing import List

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC

from bo_shared.bo_service_pages import BoService
from rebate_service.pages.rebate_homepage import RebateHomePage


class RebateMEShare(RebateHomePage):
    """
    The RebateMEShare is used for share common method for member-exclusion,
    because the sub-pages of the member-exclusion have the similar design.
    """
    PAGE_NAME = BoService.Shared.Shared

    def __init__(self, driver: WebDriver):
        super(RebateMEShare, self).__init__(driver)

    @allure.step("Click BatchManage button")
    def hover_batch_manage_button(self):
        element: WebElement = self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//button[@data-testid="actionBatchEdit"]'
        )
        self.action_chains.move_to_element(element).perform()
        self.log.debug("Click BatchManage button")

    @allure.step("Click upload spreadsheet button")
    def click_upload_spreadsheet_button(self):
        element = self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '//li[@data-testid="actionOpenModalUpload"]'
        )
        self.action_chains.click(element).perform()
        self.log.debug("Click the upload spreadsheet button")

    @allure.step("Send upload file path to input button")
    def send_upload_file_to_input(self, upload_file: str):
        """
        Fill the upload data path into input tag (csv)
        """
        input_xpath = '//button[@data-testid="actionSelectFile"]/parent::span/input'
        input_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            input_xpath
        )
        input_element.send_keys(upload_file)
        self.log.debug(f"Select the {upload_file} to upload.")

    def _get_footer_buttons(self) -> dict:
        button_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            '//button[@data-testid="actionCloseModalUpload"]'
        )
        element_info = {element.text.strip().lower(): element for element in button_elements}
        return element_info

    @allure.step("Click upload file button")
    def click_file_upload_button(self) -> bool:
        btn_type = 'upload'
        button_elements = self._get_footer_buttons()
        if btn_type not in button_elements:
            raise KeyError(
                f"The {btn_type} doesn't exist in "
                f"{list(button_elements.keys())}"
            )
        btn_element: WebElement = button_elements[btn_type]
        attrs = self.get_element_all_attrs(btn_element)
        is_available = True
        if 'disabled' not in attrs:
            btn_element.click()
        else:
            is_available = False
            self.log.debug("Need to select the file first")
        return is_available

    def upload_spreadsheet_file(self, upload_file: str) -> bool:
        """
        Upload member code csv file to server
        """
        self.hover_batch_manage_button()
        self.click_upload_spreadsheet_button()
        self.send_upload_file_to_input(upload_file)
        time.sleep(2)
        is_available = self.click_file_upload_button()
        return is_available