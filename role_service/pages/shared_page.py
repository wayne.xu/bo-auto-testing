import allure

from bo_shared.bo_service_pages import BoService
from typing import List

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from rebate_service.pages.rebate_login import RebateLogIn
from selenium.webdriver.common.keys import Keys
from bo_shared.bo_service_pages import BoService
from bo_shared.bo_shared_page import BoSharedPage


# This module is used for sharing the common functions within the Role Service

class RoleSharedPage(BoSharedPage):

    PAGE_NAME = BoService.Shared.Shared

    def __init__(self, driver: WebDriver):
        super(RoleSharedPage, self).__init__(driver)

    # 登入完成，展開sidebar (Role Service)
    @allure.step('Click function list button Role')
    def open_functions_list_by_role(self):
        click_element = self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '//*[@data-testid="/role-service"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Click the Action button in Detail page
    @allure.step("Click the Action button in Detail page")
    def click_the_action_btn_in_detail(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionMoreAction"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Cancel Deactivate Policy
    @allure.step("Cancel Deactivate Policy")
    def select_cancel_for_popup(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-body"]//button[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Get the 1st policy Status by management Page
    @allure.step("Get the 1st policy Status by management Page")
    def get_1st_status_in_main_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@data-testid="containerPolicyManagement"]//tr[1]//td[4]'
        ).text
        return element

    # Get the 1st policy Status by Detail Page
    @allure.step("Get the 1st policy Status by Detail Page")
    def get_1st_status_in_detail_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-card-body"]//span[2]//span[2]'
        ).text
        return element

    # Get the Total Assign User Count in Detail Page
    @allure.step("Get the Total Assign User Count in Detail Page")
    def get_count_of_total_asgn_user(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-card-body"]//tr[1]//td[2]//span[2]'
        ).text
        if str(element) is '0':
            print("--" * 15, element)
            return True
        else:
            return False

    # Get the Email Address by Detail Page
    @allure.step("Get the Email Address by Detail Page")
    def get_1st_email_in_detail_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@data-testid="containerBasicInformation"]//tr[3]/td[1]/div/span[2]'
        ).text
        return element

    # Get the Team Role by Detail Page
    @allure.step("Get the Team Role by Detail Page")
    def get_team_role_in_detail_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@data-testid="containerBasicInformation"]//tr[2]/td[1]/div[1]/span[2]/span[1]'
        ).text
        element_split = element.split()
        print(element_split)
        return element_split

    # Get the team role for target user
    @allure.step("Get the team role for target user")
    def get_team_role_for_trgt_user(self):
        element_main = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            # tooltip
            '//*[@id="__next"]/section/main/div/div[2]/div/div[2]//td[2]/span[1]'
        ).text
        element_tooltip = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            # tooltip
            '/html/body/div[2]/div/div/div/div[2]'
        ).text
        element_tooltip_split = element_tooltip.split(",")
        element = element_main + element_tooltip_split
        print(element)
        return element

    # Get the Username by Detail Page
    @allure.step("Get the Username by Detail Page")
    def get_1st_username_in_detail_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//h1[@class="ant-typography !mb-0"]'
        ).text
        return element

    # Confirm Delete
    @allure.step("Confirm Delete")
    def select_confirm_delete(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-content"]//button[2]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # select Policy page
    @allure.step('select Policy page')
    def select_policy_page(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '//li[@data-testid="/role-service/policy-management"]'
        ).click()

    # select Role page
    @allure.step('select Role page')
    def select_role_page(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '//li[@data-testid="/role-service/role-management"]'
        ).click()

    # select User page
    @allure.step('select User page')
    def select_user_page(self):
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '//li[@data-testid="/role-service/user-management"]'
        ).click()

    # isElementExist (Add Team Role)
    @allure.step('isElementExist (Add Team Role)')
    def atr_btn_exist(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@data-testid="actionAddTeamRole"]').text
        print("Find " + element + " Button")
        if len(element) == 0:
            return False
        else:
            print("Add Team Role Button Exist")
            return True

    # isElementExist (Role Detail)
    @allure.step('isElementExist (Role Detail)')
    def role_detail_btn_exist(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//button[@data-testid="actionViewDetail"]').text
        print("Find " + element + " Button")
        if len(element) == 0:
            return False
        else:
            print("Role Detail Button Exist")
            return True

    # isElementExist (Policy Detail)
    @allure.step('isElementExist (Policy Detail)')
    def policy_detail_btn_exist(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@data-testid="actionViewDetail"]').text
        print("Find " + element + " Button")
        if len(element) == 0:
            return False
        else:
            print("Policy Detail Button Exist")
            return True

    # isElementExist (System Role)
    @allure.step('isElementExist (System Role)')
    def system_role_btn_exist(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, "//div[@role='tab' and ./text()='System Role']").text
        print("Find " + element + " Button")
        if len(element) == 0:
            return False
        else:
            print("System Role Button Exist")
            return True

    # isElementExist (Team Role)
    @allure.step('isElementExist (Team Role)')
    def team_role_btn_exist(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, "//div[@role='tab' and ./text()='Team Role']").text
        print("Find " + element + " Button")
        if element:
            print("Team Role Button Exist")
            return True
        else:
            return False

    # Click System Role button
    @allure.step('Click System Role button')
    def click_system_role_btn(self):
        click_element = self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, "//div[@role='tab' and ./text()='System Role']"
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # isElementExist (System Role Detail)
    @allure.step('isElementExist (System Role Detail)')
    def system_role_detail_btn_exist(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//a[@data-testid="actionViewDetail"]'
        ).text
        print("Find System Role " + element + " Button")
        if element:
            print("System Role Detail Button Exist")
            return True
        else:
            return False

    # isElementExist (Edit Button)
    @allure.step('isElementExist (Edit Button)')
    def edit_btn_exist(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//button[@data-testid="actionEdit"]').text
        print("Find " + element + " Button")
        if element:
            print("Edit Button Exist")
            return True
        else:
            return False

    # isElementExist (Edit Role Button)
    @allure.step('isElementExist (Edit Button)')
    def edit_role_btn_exist(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//button[@data-testid="actionEditRole"]').text
        print("Find " + element + " Button")
        if element:
            return True
        else:
            return False

    # Get Action btn Menu By Role
    @allure.step("Get Action btn Menu By Role")
    def get_action_btn_menu_role(self):
        elements = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            "//ul/li//span[@class='ant-dropdown-menu-title-content']"
        )
        elements = [element.text.strip() for element in elements]
        if elements == ['Duplicate Role', 'Deactivate Role', 'Delete Role']:
            print("Target Button Exist")
            return True
        else:
            return False

    # Get Action btn Menu By Policy
    @allure.step("Get Action btn Menu By Policy")
    def get_action_btn_menu_policy(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//ul/li//span[@class='ant-dropdown-menu-title-content']"
        ).text
        element_split = element.split("\n")
        print(element_split)
        if element_split == ['Duplicate Policy', 'Deactivate Policy', 'Delete Policy']:
            print(element_split)
            return True
        else:
            return False

    # Get Policy Table Items
    @allure.step("Get Policy Table Items")
    def get_policy_table_items(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//thead[@class="ant-table-thead"]').text
        element_split = element.split("\n")
        print(element_split)
        if element_split == ['Policy Name', 'No. of Permissions', 'Last Updated (GMT+8)', 'Status', 'Action']:
            print("Target Items Exist")
            return True
        else:
            return False

    # isElementExist (Un/assign Users)
    @allure.step('isElementExist (Un/assign Users)')
    def un_assign_user_btn_exist(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//button[@class="ant-btn ant-btn-default ant-dropdown-trigger"]').text
        print("Find " + element + " Button")
        if len(element) == 0:
            return False
        else:
            print("Un/assign Button Exist")
            return True

    # isElementExist (Edit System Role)
    @allure.step("isElementExist (Edit System Role)")
    def edit_sys_role_btn_exist(self):
        status = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//button[@data-testid="actionOpenEditUserSystemRoleModal"]/span').get_attribute("aria-label")
        if status == 'edit':
            return True
        else:
            return False

    # isElementExist (Edit Team Role)
    @allure.step("isElementExist (Edit Team Role)")
    def edit_team_role_btn_exist(self):
        status = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//a[@data-testid="actionUpdateTeamRole"]/span').get_attribute("aria-label")
        if status == 'edit':
            return True
        else:
            return False

    # Visit Pages
    @allure.step("Visit Pages")
    def is_selection_list_expanded(self, list_type: str) -> bool:
        list_map = {
            'user': '//li[@data-testid="/role-service/user-management"]',
            'role': '//li[@data-testid="/role-service/role-management"]',
            'policy': '//li[@data-testid="/role-service/policy-management"]'
        }
        is_expanded_xpath = list_map[list_type]
        attr_name = 'aria-expanded'
        expected_value = 'true'
        is_expected = self.is_expected_element_attr_value(
            (By.XPATH, is_expanded_xpath),
            attr_name,
            expected_value
        )
        if is_expected:
            self.log.debug(f"{list_type} list is expanded")
        else:
            self.log.debug(f"{list_type} list isn't expanded")
        return is_expected
