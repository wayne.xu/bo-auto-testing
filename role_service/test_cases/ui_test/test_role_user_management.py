import time
import allure
import pytest_check
import pytest_check as check
import pytest

from bo_shared.bo_service_pages import BoService
from role_service.pages.user_management import RoleUserManagement
from role_service.pages.role_management import RoleManagement
from role_service.pages.policy_management import RolePolicyManagement

from utils.envs import Envs
from utils.decorators import execute_env


@allure.feature('Role Service User test')
class TestRoleUser():

    # **Use case User page**
    # **Use case15 - RE/Organize users by SYSTEM ROLE**
    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("case15 - RE/Organize users by SYSTEM ROLE")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-366', name="[Role][User Page] Use Case 15 - RE/Organize users by System Role")
    @allure.story("RE/Organize users by SYSTEM ROLE : Role Manager's")
    def test_for_org_user_role_manager(self, driver, test_account, test_role_data):
        user = RoleUserManagement(driver)
        role = RoleManagement(driver)
        user.login(test_account)
        """操作目標用戶"""
        user.input_user_name(test_role_data['user_name'])
        user.submit_search()
        time.sleep(5)
        user_name = user.get_table_element('name')
        check.equal(user_name, test_role_data['user_name'] + "\n" + test_role_data['user_0_email'])
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        user.click_system_role_edit()
        time.sleep(1)
        user.clear_system_role()
        user.open_system_role_selector()
        """變更System Role : Role Manager's"""
        user.select_role_manager()
        user.open_system_role_selector()
        user.click_apply_btn()
        time.sleep(3)
        user.login(test_account)
        time.sleep(1)
        # Try to RE-direct to Role page
        # bo_shared = BoSharedPage(driver)
        # bo_shared.click_service_name(service_name['role_management'])
        """檢查權限"""
        """從列表查看所有Team Role"""
        # Permission #1 View all created team-roles in the list view
        # user.select_role_page()
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.RoleManagement
        )
        time.sleep(1)
        atr_btn_exist = user.atr_btn_exist()
        check.equal(atr_btn_exist, True)
        time.sleep(1)
        """查看任一Team Role的詳情頁面"""
        # Permission #2 View all team-roles' detail page
        detail_btn_exist = user.role_detail_btn_exist()
        check.equal(detail_btn_exist, True)
        """能查看System Roles"""
        # Permission #3 View the pre-defined system-roles
        system_role_btn_exist = user.system_role_btn_exist()
        check.equal(system_role_btn_exist, True)
        """能查看System Roles的詳情頁面"""
        # Permission #4 View the pre-defined system-roles' detail page
        user.click_system_role_btn()
        time.sleep(1)
        sys_detail_btn_exist = user.system_role_detail_btn_exist()
        check.equal(sys_detail_btn_exist, True)
        """檢查Action Button的清單"""
        # Permission #5 Add new, duplicate, edit, deactivate and reactivate team-roles
        # Permission #6 Delete created team-roles
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.RoleManagement
        )
        time.sleep(1)
        role.click_the_1st_role_detail()
        edit_role_btn_exist = user.edit_role_btn_exist()
        check.equal(edit_role_btn_exist, True)
        user.click_the_action_btn_in_detail()
        time.sleep(1)
        action_menu = user.get_action_btn_menu_role()
        check.equal(action_menu, True)

    @execute_env(envs=(Envs.DEV.value, ))
    @allure.story("RE/Organize users by SYSTEM ROLE : Role Assigner's")
    def test_for_org_user_role_assign(self, driver, test_account, test_role_data):
        user = RoleUserManagement(driver)
        role = RoleManagement(driver)
        user.login(test_account)
        """操作目標用戶"""
        user.input_user_name(test_role_data['user_name'])
        user.submit_search()
        time.sleep(2)
        user_name = user.get_table_element('name')
        check.equal(user_name, test_role_data['user_name'] + "\n" + test_role_data['user_0_email'])
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        user.click_system_role_edit()
        time.sleep(1)
        user.clear_system_role()
        user.open_system_role_selector()
        """變更System Role : Role Assigner's"""
        user.select_role_assigner()
        user.open_system_role_selector()
        user.click_apply_btn()
        time.sleep(3)
        user.login(test_account)
        time.sleep(1)
        """檢查權限"""
        """查看外部匯入的第三方帳號"""
        # View all the imported users from the third party's directory
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.UserManagement
        )
        user.input_user_name(test_role_data['user_name'])
        time.sleep(1)
        user.submit_search()
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        tp_account = user.get_1st_email_in_detail_page()
        check.equal(tp_account, test_role_data['user_0_email'])
        """查看外部匯入的第三方帳號詳情頁面"""
        # View all the imported users' detail page
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.UserManagement
        )
        user.input_user_name(test_role_data['user_name'])
        time.sleep(1)
        user.submit_search()
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        tp_account = user.get_1st_email_in_detail_page()
        check.equal(tp_account, test_role_data['user_0_email'])
        trgt_user = user.get_1st_username_in_detail_page()
        check.equal(trgt_user, test_role_data['user_name'])
        """從role management可操作Un-assign/Assign Users"""
        # Assign & un-assign role(s) to/from a user
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.RoleManagement
        )
        # Into the 1st team role detail page
        role.click_the_1st_role_detail()
        un_assign_btn_exist = user.un_assign_user_btn_exist()
        check.equal(un_assign_btn_exist, True)
        time.sleep(1)
        # Into the 1st System role detail page
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.RoleManagement
        )
        user.click_system_role_btn()
        role.click_the_1st_role_detail()
        un_assign_btn_exist = user.un_assign_user_btn_exist()
        check.equal(un_assign_btn_exist, True)
        """列表可查看所有的team role"""
        # View all created team-roles in the list view
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.RoleManagement
        )
        team_role_btn_exist = user.team_role_btn_exist()
        check.equal(team_role_btn_exist, True)
        """可查看任一team role的詳情頁面"""
        # View all team-roles' detail page
        detail_btn_exist = user.role_detail_btn_exist()
        check.equal(detail_btn_exist, True)
        """能查看System Roles"""
        # View the pre-defined system-roles
        system_role_btn_exist = user.system_role_btn_exist()
        check.equal(system_role_btn_exist, True)
        """能查看System Role的詳情頁面"""
        # View the pre-defined system-roles' detail page
        user.click_system_role_btn()
        time.sleep(1)
        sys_detail_btn_exist = user.system_role_detail_btn_exist()
        check.equal(sys_detail_btn_exist, True)
        """從user management可操作system role & team role"""
        # Assign & un-assign user(s) to/from an existing team-role or an existing system-role
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.UserManagement
        )
        user.input_user_name(test_role_data['user_name'])
        time.sleep(1)
        user.submit_search()
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        edit_sys_role = user.edit_sys_role_btn_exist()
        check.equal(edit_sys_role, True)
        edit_team_role = user.edit_team_role_btn_exist()
        check.equal(edit_team_role, True)

    @execute_env(envs=(Envs.DEV.value, ))
    @allure.story("RE/Organize users by SYSTEM ROLE : Policy Maker's")
    def test_for_org_user_policy_maker(self, driver, test_account, test_role_data):
        user = RoleUserManagement(driver)
        policy = RolePolicyManagement(driver)
        user.login(test_account)
        """操作目標用戶"""
        user.input_user_name(test_role_data['user_name'])
        user.submit_search()
        time.sleep(2)
        user_name = user.get_table_element('name')
        check.equal(user_name, test_role_data['user_name'] + "\n" + test_role_data['user_0_email'])
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        user.click_system_role_edit()
        time.sleep(1)
        user.clear_system_role()
        user.open_system_role_selector()
        """變更System Role : Role Assigner's"""
        user.select_policy_maker()
        user.open_system_role_selector()
        user.click_apply_btn()
        time.sleep(3)
        user.login(test_account)
        time.sleep(1)
        """檢查權限"""
        """查看All Policies"""
        # View all created policies
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.PolicyManagement
        )
        time.sleep(3)
        policy_table_exist = user.get_policy_table_items()
        check.equal(policy_table_exist, True)
        """查看All Policies詳情頁面"""
        # View all created policies' detail page
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.PolicyManagement
        )
        policy_detail_exist = user.policy_detail_btn_exist()
        check.equal(policy_detail_exist, True)
        """查看Policy Action Button"""
        # Add new, duplicate, edit, deactivate and reactivate policies
        # Delete created policies
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.PolicyManagement
        )
        time.sleep(1)
        policy.click_the_1st_policy_detail()
        time.sleep(1)
        edit_btn_exist = user.edit_btn_exist()
        check.equal(edit_btn_exist, True)
        time.sleep(1)
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.PolicyManagement
        )
        time.sleep(1)
        policy.click_the_1st_policy_detail()
        time.sleep(1)
        user.click_the_action_btn_in_detail()
        time.sleep(1)
        action_menu_policy = user.get_action_btn_menu_policy()
        check.equal(action_menu_policy, True)

    @execute_env(envs=(Envs.DEV.value, ))
    @allure.story("RE/Organize users by SYSTEM ROLE : Viewer's")
    def test_for_org_user_viewer(self, driver, test_account, test_role_data):
        user = RoleUserManagement(driver)
        user.login(test_account)
        """操作目標用戶"""
        user.input_user_name(test_role_data['user_name'])
        user.submit_search()
        time.sleep(2)
        user_name = user.get_table_element('name')
        check.equal(user_name, test_role_data['user_name'] + "\n" + test_role_data['user_0_email'])
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        user.click_system_role_edit()
        time.sleep(1)
        user.clear_system_role()
        user.open_system_role_selector()
        time.sleep(2)
        """變更System Role : Viewer"""
        user.select_viewer()
        time.sleep(2)
        user.open_system_role_selector()
        user.click_apply_btn()
        time.sleep(3)
        user.login(test_account)
        time.sleep(1)
        """檢查權限"""
        """查看外部匯入的第三方帳號"""
        # View all the imported users from the third party's directory
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.UserManagement
        )
        user.input_user_name(test_role_data['user_name'])
        time.sleep(1)
        user.submit_search()
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        tp_account = user.get_1st_email_in_detail_page()
        check.equal(tp_account, test_role_data['user_0_email'])
        """查看外部匯入的第三方帳號詳情頁面"""
        # View all the imported users' detail page
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.UserManagement
        )
        user.input_user_name(test_role_data['user_name'])
        time.sleep(1)
        user.submit_search()
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        tp_account = user.get_1st_email_in_detail_page()
        check.equal(tp_account, test_role_data['user_0_email'])
        trgt_user = user.get_1st_username_in_detail_page()
        check.equal(trgt_user, test_role_data['user_name'])
        """列表可查看所有的team role"""
        # View all created team-roles in the list view
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.RoleManagement
        )
        team_role_btn_exist = user.team_role_btn_exist()
        check.equal(team_role_btn_exist, True)
        """可查看任一team role的詳情頁面"""
        # View all team-roles' detail page
        detail_btn_exist = user.role_detail_btn_exist()
        check.equal(detail_btn_exist, True)
        """能查看System Roles"""
        # Permission #3 View the pre-defined system-roles
        system_role_btn_exist = user.system_role_btn_exist()
        check.equal(system_role_btn_exist, True)
        """能查看System Roles的詳情頁面"""
        # Permission #4 View the pre-defined system-roles' detail page
        user.click_system_role_btn()
        time.sleep(1)
        sys_detail_btn_exist = user.system_role_detail_btn_exist()
        check.equal(sys_detail_btn_exist, True)
        """查看All Policies"""
        # View all created policies
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.PolicyManagement
        )
        time.sleep(3)
        policy_table_exist = user.get_policy_table_items()
        check.equal(policy_table_exist, True)
        """查看All Policies詳情頁面"""
        # View all created policies' detail page
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.PolicyManagement
        )
        policy_detail_exist = user.policy_detail_btn_exist()
        check.equal(policy_detail_exist, True)

    # **Use case User page**
    # **Use case16 - RE/Organize users by Team ROLE**
    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("Use case16 - RE/Organize users by Team ROLE")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-367', name="[Role][User Page] Use Case 16 - RE/Organize users by Team Role")
    @allure.story("RE/Organize users by Team ROLE")
    def test_for_org_user_team_role(self, driver, test_account, test_role_data):
        user = RoleUserManagement(driver)
        user.login(test_account)
        """操作目標用戶"""
        user.input_user_name(test_role_data['user_name'])
        user.submit_search()
        time.sleep(3)
        user.click_the_1st_user_detail()
        time.sleep(1)
        """進入Team Role編輯面板"""
        user.click_team_role_edit()
        time.sleep(1)
        """輸入過濾條件，並提交"""
        user.input_team_role_name(test_role_data['first_parent_role'])
        user.select_target_team_role_to_user()
        time.sleep(1)
        user.click_the_update_btn()
        time.sleep(1)
        """在 Detail page 確認變更結果"""
        team_role_to_user = user.get_team_role_in_detail_page()
        check.is_in(test_role_data['first_parent_role'], team_role_to_user)

    # **Use case User page**
    # **Use case17 - Activate/Deactivate User**
    @execute_env(envs=(Envs.DEV.value, ))
    @allure.epic("Use case17 - Activate/Deactivate User")
    @allure.severity(allure.severity_level.BLOCKER)
    @allure.link('https://hermes.devgambit.net/browse/PSS-368', name="[Role][User Page] Use Case 17 - Activate/Deactivate User")
    @allure.story("Deactivate User")
    def test_for_deactivate_user(self, driver, test_account, test_role_data):
        user = RoleUserManagement(driver)
        role = RoleManagement(driver)
        user.login(test_account)
        """操作目標用戶"""
        user.input_user_name(test_role_data['user_name'])
        user.submit_search()
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        """禁用User"""
        user.deact_user()
        time.sleep(2)
        user.select_confirm_deactivate()
        time.sleep(3)
        detail_user_status = user.get_1st_status_in_detail_page()
        check.equal(detail_user_status, test_role_data['status_eq_deactivate'])
        time.sleep(1)
        """Check te target user status in management page"""
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.UserManagement
        )
        user.input_user_name(test_role_data['user_name'])
        user.submit_search()
        time.sleep(3)
        first_user_status = user.get_table_element('status')
        check.equal(first_user_status, test_role_data['status_eq_deactivate'])

    @execute_env(envs=(Envs.DEV.value, ))
    @allure.story("Activate User")
    def test_for_activate_user(self, driver, test_account, test_role_data):
        user = RoleUserManagement(driver)
        role = RoleManagement(driver)
        user.login(test_account)
        """操作目標用戶"""
        user.input_user_name(test_role_data['user_name'])
        user.submit_search()
        time.sleep(1)
        user.click_the_1st_user_detail()
        time.sleep(1)
        """啟用User"""
        user.act_user()
        time.sleep(3)
        detail_user_status = user.get_1st_status_in_detail_page()
        check.equal(detail_user_status, test_role_data['status_eq_activate'])
        time.sleep(1)
        """Check te target user status in management page"""
        user.click_service_func_list(
            BoService.Role.ServiceName,
            BoService.Role.UserManagement
        )
        user.input_user_name(test_role_data['user_name'])
        user.submit_search()
        time.sleep(3)
        first_user_status = user.get_table_element('status')
        check.equal(first_user_status, test_role_data['status_eq_activate'])
