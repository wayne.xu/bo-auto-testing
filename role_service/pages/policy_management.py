import allure
import time
import random
import string
import uuid
import pytest

from bo_shared.bo_service_pages import BoService
from typing import List
from role_service.pages.shared_page import RoleSharedPage
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from utils.utils import ZipTools
from utils.utils import TestDataLoader, SystemLogger


@pytest.fixture()
def file_name() -> dict:
    test_data = TestDataLoader()
    case_file_name = test_data['file_name']
    return {
        'case1': case_file_name['case1'],
        'case2': case_file_name['case2'],
        'case3': case_file_name['case3'],
        'case4': case_file_name['case4'],
        'case5': case_file_name['case5'],
        'case6': case_file_name['case6'],
        'case7': case_file_name['case7'],
        'case8': case_file_name['case8'],
        'case9': case_file_name['case9'],
        'case10': case_file_name['case10']
    }


class RolePolicyManagement(RoleSharedPage):
    PAGE_NAME = BoService.Role.PolicyManagement

    def __init__(self, driver: WebDriver):
        super(RolePolicyManagement, self).__init__(driver)

    # Add new Policy
    @allure.step('Add new Policy')
    def add_new_policy(self):
        click_element = self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '//a[@data-testid="actionCreate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Get current Breadcrumb Link lv3
    @allure.step("Get current Breadcrumbs")
    def get_current_breadcrumb_lv3(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//nav[@data-testid="breadcrumb"]//li[3]').text
        return element

    # Input Policy Name
    @allure.step("Input Policy Name")
    def input_policy_name(self, new_policy_name: str):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="name"]'
        ).send_keys(new_policy_name)

    @allure.step("Create .txt file Name")
    def create_file_name(self):
        _uuid = str(uuid.uuid4()).replace('-', '')
        file_name = f"{_uuid}_RandomPolicyName"
        return file_name

    @allure.step("Create Random Name")
    def create_random_item_name(self):
        random_str = ''.join(random.sample(string.ascii_letters + string.digits, 25))
        return random_str

    @allure.step("Create .txt file for case1")
    def create_file_for_test_case(self, file_name: str):
        path = './temp_item_name/policy/'
        random_item_name = self.create_random_item_name()
        with open(path + file_name + '.txt', 'w') as f:
            f.write(random_item_name)
        return random_item_name

    @allure.step("Get the Random policy name by File")
    def get_random_policy_name_by_file(self, file_name: str):
        path = './temp_item_name/policy/'
        with open(path + file_name + '.txt', 'r') as f:
            data = f.read()
        return data

    # Input Random Policy Name
    @allure.step("Input Random Policy Name")
    def create_random_policy(self, file_name: str):
        random_policy_name = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="name"]'
        )
        random_str = self.create_file_for_test_case(file_name)
        random_policy_name.send_keys(random_str)
        return random_str

    # Filter Policy by Name
    @allure.step("Filter Policy by Name")
    def search_policy_by_random_name_file(self, file_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="userPolicyName"]'
        )
        data = self.get_random_policy_name_by_file(file_name)
        element.send_keys(data)
        element.send_keys(Keys.ENTER)

    # Select Policy's Permissions
    @allure.step("Select Policy's Permissions")
    def select_policy_permission(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "(//input[@type='checkbox'])[1]"
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Create Policy
    @allure.step("Create Policy")
    def create_policy(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionCreate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Filter Modify Policy
    @allure.step("Filter Modify Policy")
    def search_policy_by_modify_name(self, modify_policy_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="userPolicyName"]'
        )
        element.send_keys(modify_policy_name)
        element.send_keys(Keys.ENTER)

    # Filter Duplicate Policy
    @allure.step("Filter Duplicate Policy")
    def search_policy_by_duplicate_name(self, duplicate_policy_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="userPolicyName"]'
        )
        element.send_keys(duplicate_policy_name)
        element.send_keys(Keys.ENTER)

    # Click Search Button
    @allure.step("Click Search Button")
    def click_search_btn(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionSearch"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Confirm Create Policy
    @allure.step("Confirm Create Policy")
    def confirm_create_policy(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '(//button[@class="ant-btn ant-btn-primary"])[2]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Get the 1st policy name by List
    @allure.step("Get the 1st policy name by List")
    def get_1st_policy_name(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-table-content"]//tr[1]/td[1]'
        ).text
        return element

    # Get the 1st policy Status by List
    @allure.step("Get the 1st policy Status by List")
    def get_1st_policy_status_list(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-table-content"]//tr[1]/td[4]'
        ).text
        return element

    # Get the 1st policy Status by Detail Page
    @allure.step("Get the 1st policy Status by Detail Page")
    def get_1st_status_in_detail_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-descriptions-view"]//span[2]//span[2]'
        ).text
        return element

    # Get the Policy Name in Detail Page
    @allure.step("Get the Policy Name in Detail Page")
    def get_the_policy_name_in_detail_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//h1[@class="ant-typography !mb-0"]'
        ).text
        return element

    # Click the Detail button for 1st policy
    @allure.step("Click the Detail button for 1st policy")
    def click_the_1st_policy_detail(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '(//button[@data-testid="actionViewDetail"])[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Click the Action button in Detail page
    @allure.step("Click the Action button in Detail page")
    def click_the_action_btn_in_detail(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionMoreAction"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select Duplicate
    @allure.step("Select Duplicate")
    def select_duplicate(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@data-testid="duplicate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Deactivate Policy
    @allure.step("Deactivate Policy")
    def select_deactivate(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@data-testid="deactivate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Activate Policy
    @allure.step("Activate Policy")
    def select_activate(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@data-testid="activate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Delete Policy
    @allure.step("Delete Policy")
    def select_delete(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@data-testid="delete"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Confirm Delete
    @allure.step("Confirm Delete")
    def select_confirm_delete(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-content"]//button[2]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Get Policy Table Content
    @allure.step("Get Policy Table Content")
    def get_policy_table_content(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-table-content"]'
        ).text
        element_split = element.split()
        return element_split

    # Confirm Deactivate Policy
    @allure.step("Confirm Deactivate Policy")
    def select_confirm_deactivate(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-content"]//button[2]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Cancel Deactivate Policy
    @allure.step("Cancel Deactivate Policy")
    def select_cancel_for_popup(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-content"]//button[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Input the Duplicate Policy Name
    @allure.step("Modify the Policy Name")
    def input_duplicate_item_name(self, duplicate_policy_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="name"]'
        )
        element.send_keys(Keys.COMMAND, "a")
        element.send_keys(Keys.BACKSPACE)
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="name"]'
        ).send_keys(duplicate_policy_name)

    # Editing Policy
    @allure.step("Editing Policy")
    def edit_policy(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionEdit"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Confirm Editing Policy
    @allure.step("Confirm Editing Policy")
    def confirm_edit_policy(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-content"]//button[2]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Modify the Policy Name
    @allure.step("Modify the Policy Name")
    def modify_policy_name(self, modify_policy_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="name"]'
        )
        element.send_keys(Keys.COMMAND, "a")
        element.send_keys(Keys.BACKSPACE)
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="name"]'
        ).send_keys(modify_policy_name)

    # Un-Select Policy's Permissions
    @allure.step("Un-Select Policy's Permissions")
    def unselect_policy_permission(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "(//input[@type='checkbox'])[1]"
        )
        self.action_chains.move_to_element(click_element).click().perform()
        self.action_chains.move_to_element(click_element).click().perform()

    # Re-Select Policy's Permissions
    @allure.step("Re-Select Policy's Permissions")
    def reselect_policy_permission(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//button[@aria-label='Expand row']"
        )
        self.action_chains.move_to_element(click_element).click().perform()
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(text(),'View the latest overall rebate performance')]"
        )
        self.action_chains.move_to_element(click_element).click().perform()
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(text(),'Export filtered rebate list in all tabs')]"
        )
        self.action_chains.move_to_element(click_element).click().perform()
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[contains(text(),'Delete rebates in all tabs')]"
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Save Policy
    @allure.step("Save Policy")
    def save_policy(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionUpdate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Create a new Policy
    @allure.step("Create New Policy")
    def create_new_policy(self, file_name: str):
        self.add_new_policy()
        time.sleep(1)
        self.create_random_policy(file_name)
        time.sleep(1)
        self.select_policy_permission()
        time.sleep(1)
        self.create_policy()
        time.sleep(1)
        self.confirm_create_policy()

    # Delete First Policy
    @allure.step("Delete First Policy")
    def delete_first_policy(self):
        self.click_the_action_btn_in_detail()
        time.sleep(1)
        self.select_delete()
        time.sleep(1)
        self.select_confirm_delete()
        time.sleep(1)


