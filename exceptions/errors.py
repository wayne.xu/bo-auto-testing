# Define the custom Exception

class GetTokenError(Exception):
    """
    Exception raised when getting token errors
    """
    pass


class GetDeviceError(Exception):
    """
    Exception raised when requesting to use the device failed
    """
    pass