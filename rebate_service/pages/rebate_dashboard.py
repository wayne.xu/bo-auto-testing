from copy import deepcopy
import time

import allure

from typing import Optional, List, Tuple, Union
from datetime import datetime

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import (
    TimeoutException,
    NoSuchElementException,
    StaleElementReferenceException,
    ElementClickInterceptedException
)

from bo_shared.bo_service_pages import BoService
from rebate_service.pages.rebate_homepage import RebateHomePage
from utils.utils import HelperFuncs
from utils.decorators import retry_func


class RebateDashboard(RebateHomePage):

    PAGE_NAME = BoService.Rebate.DashBoard

    def __init__(self, driver: WebDriver):
        super(RebateDashboard, self).__init__(driver)

    # Get dashboard chart title('Total TO & Rebate Given')
    @allure.step('Get dashboard chart title')
    def get_dashboard_chart_title(self) -> str:
        element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@data-testid="containerStatistics"]//span/strong'
        )
        return element.text.strip()

    @allure.step("Click the specific selection")
    def click_dashboard_selection(self, selection_type: str):
        xpath_map = {
            'product': '//*[@data-testid="productOptions"]',
            'platform': '//*[@data-testid="platformOptions"]',
            'recurrence': '//*[@data-testid="recurrenceOptions"]',
        }
        xpath = xpath_map[selection_type]
        clickable_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            xpath
        )
        self.action_chains.move_to_element(clickable_element).click().perform()
        self.log.debug(
            f"Click {self.__class__.__name__} page "
            f"{selection_type} selection"
        )

    # Get the selection products from Platform
    @allure.step("Get selection products from platform")
    def get_product_selection(
            self,
            return_element: bool = False,
            get_default_value: bool = False
        ):
        """Get the product selection values, if it don't select, it will get the default value

        Args:
            return_element (bool, optional): Defaults to False.
        """
        if get_default_value:
            xpath = '//*[@data-testid="productOptions"]'
        else:
            xpath = '//*[@data-testid="productOptions"]/div/div/div'
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            xpath
        )
        plus_num_pat = self.shared_regex['plus_num_pat']
        selection_products = list()
        for element in elements:
            name = element.text.strip()
            if not name:
                continue
            if plus_num_pat.match(name) is None:
                if return_element:
                    selection_products.append((name, element))
                else:
                    selection_products.append(name)
        return selection_products

    def get_product_expanded_elements(self) -> List[Tuple[str, WebElement]]:
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            '//div[@class="ant-select-tree-list-holder-inner"]/div'
        )
        elem_dict = [(ele.text.strip(), ele) for ele in elements if ele.text.strip()]
        return elem_dict

    def is_selection_list_expanded(self, list_type: str) -> bool:
        list_map = {
            'product': '//*[@id="products"]',
            'platform': '//*[@id="platformIds"]',
            'recurrence': '//*[@id="recurrenceId"]'
        }
        is_expanded_xpath = list_map[list_type]
        attr_name = 'aria-expanded'
        expected_value = 'true'
        is_expected = self.is_expected_element_attr_value(
            (By.XPATH, is_expanded_xpath),
            attr_name,
            expected_value
        )
        if is_expected:
            self.log.debug(f"{list_type} list is expanded")
        else:
            self.log.debug(f"{list_type} list isn't expanded")
        return is_expected

    def _search_element_by_product_name(
            self,
            product_name: str,
            expanded_elements: List[Tuple[str, WebElement]]
        ) -> Tuple[str, WebElement]:
        for ele_tup in expanded_elements:
            pname, _ = ele_tup
            if product_name == pname:
                return ele_tup
        raise ValueError(
            f"Can't find element by {product_name}, "
            f"expanded_elements: {expanded_elements}"
        )

    @allure.step("Select the target products")
    def select_target_products(
            self,
            select_idx: Optional[Tuple[int]] = None,
            select_product_names: Optional[Tuple[str]] = None
        ) -> List[str]:
        """Select the product expanded list.
        It support two way to select elements.
        1. index
        2. product name
        It must choose one method to select the target elements.

        Args:
            select_idx (Optional[Tuple[int]], optional): Defaults to None.
            select_product_names (Optional[Tuple[str]], optional): Defaults to None.

        Raises:
            ValueError: Both of `select_idx` and `select_product_names` are None at the same time

        Returns:
            List[str]: product names that you choose

        Ex:
            test_rebate_dashboard.py::TestRebateDashboard::test_select_target_products
        """
        if select_idx is None and select_product_names is None:
            raise ValueError(
                "Both of `select_idx` and `select_product_names` are None at the same time"
            )
        _type = 'product'
        if self.is_selection_list_expanded(_type) == False:
            self.click_dashboard_selection(_type)

        expanded_elements = self.get_product_expanded_elements()

        select_names = list() # It's used for returning.
        if select_idx:
            for idx in select_idx:
                name, ele = expanded_elements[idx]
                ele.click()
                select_names.append(name)

        if select_product_names:
            for pname in select_product_names:
                expand_ele = self._search_element_by_product_name(pname, expanded_elements)
                name, ele = expand_ele
                ele.click()
                select_names.append(name)
        return select_names

    @allure.step("Clear production selections by name")
    def clear_product_selection_by_name(self, clear_names: Union[set, tuple, list]):
        exception_times = 0
        cancel_names = set()
        while True:
            if exception_times > 10:
                raise StaleElementReferenceException
            selection_products = self.get_product_selection(return_element=True)
            if len(selection_products) == 0 or cancel_names == set(clear_names):
                break
            for name, element in selection_products:
                if name in clear_names:
                    try:
                        cancel_ele = element.find_element(By.XPATH, '//span/span[2]')
                        self.action_chains.move_to_element(cancel_ele).click().perform()
                        cancel_names.add(name)
                    except StaleElementReferenceException:
                        exception_times += 1
                        self.log.info(f"Exception time: {exception_times}")
                        break
            if cancel_names == set(clear_names):
                break

    @allure.step("Clear target selections")
    def clear_all_selections(self, clear_target: str):
        """Clear all selection

        Args:
            clear_target (str): `product`, `platform` or recurrence
        """
        target_map = {
            'product': 2,
            'platform': 3,
            'recurrence': 4,
        }
        xpath_format = (
            '//form[@class="ant-form ant-form-vertical"]'
            '/div/div{target_idx}//div/span[@unselectable="on"]'
        )
        clear_xpath = self.xpath_editor(
            xpath_format,
            target_idx=target_map[clear_target]
        )
        clear_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            clear_xpath
        )
        # Use mouse to click the specific element
        self.action_chains.move_to_element(clear_element).click().perform()

    def _get_title_currency(self, statistic_title: str) -> tuple:
        space_pat = self.shared_regex['space_pat']
        *title, currency = space_pat.split(statistic_title)
        title = self.normalize_string(' '.join(title))
        brackets_pat = self.shared_regex['brackets_pat']
        currency = brackets_pat.sub('', currency)
        return title, currency

    @allure.step("Get statistic value")
    def get_statistic_values(self) -> dict:
        """Get dashboard statistic values

        Raises:
            ValueError: Occurs duplicate statistic title
        """
        time.sleep(3)
        xpath = '//div[@class="ant-space-item"]//div[@class="ant-statistic"]'
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            xpath
        )
        duplicate_title = list()
        error_flag = False
        all_statistic_values = dict()
        for element in elements:
            title = element.find_element(By.XPATH, 'div[@class="ant-statistic-title"]').text
            value = element.find_element(By.XPATH, 'div//span').text
            value = HelperFuncs.remove_punctuation(value)
            title, currency = self._get_title_currency(title)
            if title in all_statistic_values:
                error_flag = True
                duplicate_title.append(title)
            else:
                all_statistic_values[title] = {'currency': currency, 'value': value}

        if error_flag:
            raise ValueError(
                f"Get duplicate statistic title: {duplicate_title} "
                f"all_statistic_values: {all_statistic_values}"
            )

    @retry_func(
        exceptions=(TimeoutException, StaleElementReferenceException),
        retry_time_interval=1
    )
    @allure.step("Get calendar target day")
    def get_calendar_target_day(self) -> Tuple[str, WebElement]:
        """Get the current date which class attribute of html tag is `rbc-date-cell rbc-now`.
        It denote as current date

        Returns:
            Tuple[str, WebElement]
        """
        space_pat = self.shared_regex['space_pat']
        try:
            if self.is_calendar_loading_success():
                calendar_xpath_format = '//*[@class="rbc-month-row"]{row}/div[2]/div[1]/div{column}'
                all_date_xpath = self.xpath_editor(
                    calendar_xpath_format,
                    row=None,
                    column=None
                )
                date_elements: List[WebElement] = self.find_element_wait(
                    EC.presence_of_all_elements_located,
                    By.XPATH,
                    all_date_xpath
                )
                for element in date_elements:
                    _date = element.text.strip()
                    identify_attr = element.get_attribute('class')
                    if identify_attr is None:
                        raise ValueError(f"Can't get class from xpath: {all_date_xpath}")

                    identify_attr = space_pat.split(identify_attr)
                    if 'rbc-now' in identify_attr:
                        return (_date, element)

                raise NoSuchElementException(
                    "Can't find the element with specific class attribute, "
                    f"using xpath: {all_date_xpath}"
                )
            else:
                raise TimeoutException(
                    "Loading calendar timeout"
                )
        except Exception as e:
            raise e

    @retry_func(
        exceptions=(TimeoutException, StaleElementReferenceException),
        retry_time_interval=1
    )
    @allure.step("Get calendar pick year month value")
    def get_calendar_pick_year_month(self) -> str:
        """Get calendar pick year month, default value is current month
        """
        try:
            if self.is_calendar_loading_success():
                year_month_xpath = '//*[@data-testid="pickMonth"]'
                year_month_element: WebElement = self.find_element_wait(
                    EC.presence_of_element_located,
                    By.XPATH,
                    year_month_xpath
                )
                year_month = year_month_element.get_attribute('value').strip()
                return year_month
            else:
                raise TimeoutException(
                    "Loading calendar timeout"
                )
        except Exception as e:
            raise e

    def _convert_dateformat(self, year_month: str, day: str) -> str:
        if day.isdigit() == False:
            alphabet_pat = self.shared_regex['alphabet_pat']
            day = alphabet_pat.sub('', day).strip()
        year, month = year_month.split('-')
        d = datetime(int(year), int(month), int(day))
        return d.strftime('%Y-%m-%d')

    @allure.step("Get calendar current date")
    def get_calendar_current_date(self) -> str:
        day, _ = self.get_calendar_target_day()
        year_month = self.get_calendar_pick_year_month()
        convert_date = self._convert_dateformat(year_month, day)
        return convert_date

    @allure.step("Click calendar button")
    @retry_func(
        exceptions=(
            ElementClickInterceptedException,
            StaleElementReferenceException,
            TimeoutException,
        ),
        retry_time_interval=1
    )
    def click_calendar_btn(self, btn_type: str):
        """Click Rebate dashboard calendar

        Args:
            btn_type (str): which button do you click, here are four types of button.
            `today`, `preview_month`, `next_month`, and `pick_month`
        """
        try:
            if self.is_calendar_loading_success():
                btn_map = {
                    'today': '//button[@data-testid="actionToday"]',
                    'preview_month': '//button[@data-testid="actionPrev"]',
                    'next_month': '//button[@data-testid="actionNext"]',
                    'pick_month': '//*[@data-testid="pickMonth"]/parent::div/parent::div',
                }
                btn_xpath = btn_map[btn_type]
                btn_element: WebElement = self.find_element_wait(
                    EC.element_to_be_clickable,
                    By.XPATH,
                    btn_xpath
                )
                btn_element.click()
            else:
                raise TimeoutException(
                    "Unexpected reasons cause "
                )
        except Exception as e:
            raise e

    def is_calendar_loading_success(self) -> bool:
        """Wait for calendar loading success

        Returns:
            bool: True => It has been loaded
        """
        return self.is_page_loading_success()

    @allure.step("Select month by date picker")
    def select_month_by_date_picker(self, month: str) -> tuple:
        """Select month by date picker(dashboard calendar part)

        Args:
            month (str): The abbreviation of months,
            `Jan`, `Feb`, `Mar`, `Apr`, `May`, `Jun`,
            `Jul`, `Aug`, `Sep`, `Oct`, `Nov`, `Dec`
        """
        month_map = {
            'Jan': {'row': 1, 'column': 1},
            'Feb': {'row': 1, 'column': 2},
            'Mar': {'row': 1, 'column': 3},
            'Apr': {'row': 2, 'column': 1},
            'May': {'row': 2, 'column': 2},
            'Jun': {'row': 2, 'column': 3},
            'Jul': {'row': 3, 'column': 1},
            'Aug': {'row': 3, 'column': 2},
            'Sep': {'row': 3, 'column': 3},
            'Oct': {'row': 4, 'column': 1},
            'Nov': {'row': 4, 'column': 2},
            'Dec': {'row': 4, 'column': 3},
        }
        date_picker_xpath_format =(
            '//*[@class="ant-picker-month-panel"]/div[2]'
            '/table[@class="ant-picker-content"]//tr{row}/td{column}'
        )
        coordinate = month_map[month]
        target_month_xpath = self.xpath_editor(date_picker_xpath_format, **coordinate)
        month_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            target_month_xpath
        )
        is_clickable = True
        click_date = month_element.get_attribute('title')
        try:
            month_element.click()
            time.sleep(3)
        except ElementClickInterceptedException:
            is_clickable = False
            self.log.error(
                f"Input {month} month can't be clicked. "
                f"element_date: {click_date} "
                f"month_xpath: {target_month_xpath}"
            )
        self.log.debug(
            f"Click calendar date picker month: {month}, "
            f"and its return is is_clickable: {is_clickable}, "
            f"click_date: {click_date}"
        )
        return is_clickable, click_date

    @allure.step("Click calendar date picker year manipulation button")
    def calendar_year_manipulation_btn(self, _type: str):
        """Manipulate the calendar, to get next or preview year
        Before using this method, it need to call `self.click_calendar_btn('pick_month')`

        Args:
            _type (str): `next` or `preview`
        """
        xpath_format = '//*[@class="ant-picker-month-panel"]/div[1]/button{type_idx}'
        type_map = {
            'preview': 1,
            'next': 2
        }
        type_idx = type_map[_type]
        click_xpath = self.xpath_editor(xpath_format, type_idx=type_idx)
        self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            click_xpath
        ).click()
        self.log.debug(f"Click calendar {_type} year button")

    @allure.step("Get event types")
    def get_event_types(self, normalize_string: bool = True) -> dict:
        """Get the map of the event-type
        """
        types_xpath = '//span[contains(@data-testid, "recurrence")]'
        type_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            types_xpath
        )
        types_info = dict()
        for type_element in type_elements:
            # Get the type name
            type_name = type_element.text.strip()
            if normalize_string:
                type_name = self.normalize_string(type_name)
            # Get event type id
            event_type_id = type_element.get_attribute('data-testid').strip()
            event_type_id = event_type_id.split('-')[-1]

            if event_type_id not in types_info:
                types_info[event_type_id] = type_name

        return types_info

    def get_select_year_month_events(self) -> list:
        """Only get events which belong in the selected year month
        """
        year_month = self.get_calendar_pick_year_month()
        year, month = year_month.split('-')
        all_events = self.get_all_events()
        other_events = 0
        filter_events = list()
        for event in all_events:
            if HelperFuncs.is_ts_in_year_month(
                int(year),
                int(month),
                event['data-event-last_calculate_at']
            ):
                filter_events.append(event)
            else:
                other_events += 1
                _date = HelperFuncs.convert_ts_format(event['data-event-last_calculate_at'])
                self.log.debug(
                    f"Doesn't belong in {year_month} "
                    f"event_name: {event['event_name']}, "
                    f"event_date: {_date}, timestamp: {event['data-event-last_calculate_at']}, "
                )
        self.log.debug(
            f'The {year_month} calendar has {len(all_events)} events, and '
            f'there are {len(filter_events)} events belong in {year_month}, '
            f"count: {other_events}"
        )
        return filter_events

    def get_all_events(self):
        """Get all events from calendar
        """
        events_xpath_format = (
            '//*[@class="rbc-month-row"]{row}'
            '/div[@class="rbc-row-content"]/div[position()>1]/div/*'
        )
        events_xpath = self.xpath_editor(
            events_xpath_format,
            row=None
        )
        elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            events_xpath
        )
        all_events_info = list()
        for element in elements:
            if element.tag_name == 'button':
                more_events = self._get_more_events(element)
                all_events_info.extend(more_events)
            else:
                attrs = self.get_element_all_attrs(element)
                attrs['data-event-last_calculate_at'] =\
                    HelperFuncs.convert_milliseconds_ts(
                        attrs['data-event-last_calculate_at']
                    )
                attrs['event_name'] = element.text.strip()
                all_events_info.append(attrs)
        return all_events_info

    def _get_more_events(self, more_event_pop_element: WebElement) -> list:
        """Get events from more events button

        Args:
            more_event_pop_element (WebElement):
        """
        more_events_xpath = '//*[@data-testid="moreEvents"]/div'
        self.action_chains.move_to_element(more_event_pop_element).click().perform()
        event_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            more_events_xpath
        )
        more_events = list()
        for event_element in event_elements:
            attrs = self.get_element_all_attrs(event_element)
            attrs['event_name'] = event_element.text.strip()
            attrs['data-event-last_calculate_at'] =\
                    HelperFuncs.convert_milliseconds_ts(
                        attrs['data-event-last_calculate_at']
                    )
            more_events.append(attrs)
        more_event_pop_element.click()
        return more_events

    # --------- Below code hasn't been refactored ---------------- #

    # Set up the calendar time by send key
    @allure.step("Set up the calendar time by send key")
    def set_up_time_for_calendar(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "(//div[normalize-space()='Mar'])[1]"
        ).click()

    # Select some weekly event
    @allure.step("Select some weekly event")
    def select_some_event_by_weekly(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "(//div[contains(@class,'rbc-event-content')][normalize-space()='Some Weekly Event'])[1]"
        ).click()

    # View event
    @allure.step("Viewing event")
    def click_the_view_event(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//*/div/div/div/div[2]/div[1]/div/div/div[1]/button'
        ).click()

    # Duplicate event
    @allure.step("Edit event")
    def click_the_edit_event(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//*/div/div/div/div[2]/div[1]/div/div/div[2]/button'
        ).click()

    # Duplicate event
    @allure.step("Duplicate event")
    def click_the_duplicate_event(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//*/div/div/div/div[2]/div[1]/div/div/div[3]/button'
        ).click()

    # Close event
    @allure.step("Close event")
    def click_the_close_event(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//*/div/div/div/div[2]/div[1]/div/div/div[4]/button'
        ).click()

    # Get the selection item by Rebate Name
    @allure.step("Get the selection item by Rebate Name")
    def get_the_selection_item_by_rebate_name(self):
        elements = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH, '//*[@id="__next"]//div[2]/div[1]/div/div[2]/form/div/div[5]').text
        return elements

    # Click the btn for next month on calendar
    @allure.step("Click the next month_btn")
    def click_the_next_month_btn(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[2]/div/div[2]/div/div/div/div/div[1]/div[1]/div[3]/button'
        ).click()

    # Click the btn for last month on calendar
    @allure.step("Click the last month_btn")
    def click_the_last_month_btn(self):
        # for n in range(10):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="__next"]//div[2]/div/div[2]/div/div/div/div/div[1]/div[1]/div[2]/button'
        ).click()
