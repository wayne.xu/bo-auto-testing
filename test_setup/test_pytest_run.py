import pytest
import allure
import pytest_check as check

# @pytest.fixture
# def test_num():
#     return {'key': 1}


# @allure.epic("Example epic1")
# @allure.severity(allure.severity_level.TRIVIAL)
# @pytest.mark.parametrize('arg1, arg2', [(1, 2), (1, 4)])
# @pytest.mark.xdist_group(name='group1')
# def test_example(test_num, arg1, arg2):
#     check.equal(test_num['key'], arg1)
#     check.is_in(test_num['key'], [arg1, arg2], "Is 1 in the list")
#     assert test_num['key'] == arg1


# @allure.title('test_data: {test_num}')
# @allure.severity(allure.severity_level.TRIVIAL)
# @allure.link('http://54.178.162.35/index.php?/cases/view/212577&group_by=cases:section_id&group_order=asc&group_id=23690', name='testRail test-case')
# @pytest.mark.xdist_group(name='group2')
# @pytest.mark.parametrize('arg1, arg2', [(1, 2), (1, 4)])
# def test_func1(test_num, arg1, arg2):
#     print('test_func1')
#     assert test_num['key'] == arg1


# @allure.epic("Example epic2")
# @allure.severity(allure.severity_level.BLOCKER)
# @allure.description("Just test fail test case")
# @pytest.mark.parametrize('arg1, arg2', [(1, 2), (1, 4)])
# @pytest.mark.xdist_group(name='group2')
# def test_func2(test_num, arg1, arg2):
#     print('test_func2')
#     assert test_num['key'] == arg1


# @allure.issue('140', 'bug issue link')
# @allure.severity(allure.severity_level.CRITICAL)
# @pytest.mark.parametrize('arg1, arg2', [(1, 2), (1, 4)])
# @pytest.mark.xdist_group(name='group1')
# def test_func3(test_num, arg1, arg2):
#     assert test_num['key'] == arg1
