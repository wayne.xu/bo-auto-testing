from enum import Enum

from utils.utils import EnumMixIn

class ProductOptions(EnumMixIn, Enum):

    _DEFAULT = "Product Group"
    KENO = "Keno"
    LIVE_DEALER = 'Live dealer'
    P2P = 'P2P'
    RNG = "RNG"
    SPORTSBOOK = "Sportsbook"


class PlatFormOptions(EnumMixIn, Enum):

    _DEFAULT = 'Platform'
    DESKTOP = 'Desktop'
    MOBILE = 'Mobile'


class RecurrenceOptions(EnumMixIn, Enum):

    _DEFAULT = 'Recurrence'
    NO_RECURRENCE = "No Recurrence"
    DAILY = "Daily"
    WEEKLY = "Weekly"
    MONTHLY = "Monthly"


class ReleaseMethodOptions(EnumMixIn, Enum):

    _DEFAULT = 'Release Method'
    AUTO_RELEASE = 'Auto Release'
    MANUAL_RELEASE = 'Manual Release'


class StatusOptions(EnumMixIn, Enum):

    _DEFAULT = 'Status'
    NOT_STARTED = 'Not Started'
    IN_PROGRESS = 'In Progress'
    FINISHED = 'Finished'
    DEACTIVATE = 'Deactivate'


class RebateCalculationFrequencyOptions(EnumMixIn, Enum):

    _DEFAULT = 'Rebate Calculation Frequency'
    EVERY_HOUR = 'Every Hour'
