import os
import time
import json
import glob
import traceback

from copy import deepcopy
from pathlib import Path
from datetime import datetime
from concurrent.futures import Future
from typing import Set, List, Union, Optional

import pytest
import pymsteams
import requests

from _pytest.terminal import TerminalReporter
from requests.exceptions import RequestException
from jinja2 import Environment, FileSystemLoader

from utils.url_router import URLRouter
from utils.utils import (
    SystemLogger, ZipTools,
    S3Bucket, SingletonMeta,
    PoolExecutor, SynchronizedLock
)


class TestStatus():

    SUCCESS = "success"
    FAILED = "failed"


class ReportUtils():
    """
    This class is used for generating reports.
    """
    REPORT_IMG_DIR = os.path.join(os.getcwd(), 'report_imgs')
    SAVE_EXT = '.png'

    @classmethod
    def nodeid_to_save_info(cls, node_id: str) -> tuple:
        """
        Convert node_id to folder_name and file_name

        Ex:
            node_id = "test_06.py::TestDemo::test_case1"
        """
        folder_name, *file_name = node_id.split('::')
        folder_name = folder_name.replace('.py', '')
        file_name = '_'.join(file_name)
        # Use timestamp as file suffix
        file_suffix = int(time.time())
        file_name = f'{file_name}_{file_suffix}{cls.SAVE_EXT}'
        folder_path = os.path.join(cls.REPORT_IMG_DIR, folder_name)
        return folder_path, file_name

    @staticmethod
    def convert_nodeid_to_report_group(node_id: str):
        path, *_ = node_id.split('::')
        split_result = path.split('/')
        service_name = None
        if len(split_result) >= 2:
            service_name, module_name = split_result[0], split_result[-1]
        else:
            module_name = split_result[0]

        module_name = module_name.replace('.py', '')
        return service_name, module_name

    @staticmethod
    def summarize_parallel_result(session: pytest.Session) -> List[dict]:
        passed = session.testscollected - session.testsfailed
        report = [{
            'service': 'all',
            'module': 'all',
            'passed': passed,
            'failed': session.testsfailed,
            'skipped': 0,
            'total': session.testscollected,
            'pass_rate': round(passed/session.testscollected, 2),
        }]
        return report

    @staticmethod
    def summarize_test_result(items: List[pytest.Item]) -> List[dict]:
        """
        Briefly summarize the whole test result, calculate its passed, failed, and skipped.

        Args:
            items (List[pytest.Item])

        Returns:
            dict: The counting result
        """
        report_template = {
            'service': None,
            'module': None,
            'passed': 0,
            'failed': 0,
            'skipped': 0,
            'total': 0,
            'pass_rate': 0,
        }
        all_report_results = {
            'all': deepcopy(report_template)
        }
        all_report_results['all']['service'] = 'all'
        all_report_results['all']['module'] = 'all'
        for item in items:
            # For handle the test occurs unexpected error, it cause the item doesn't have report attribute
            if hasattr(item, 'report') == False:
                raise AttributeError(
                    "Item object doesn't have report attribute. "
                    f"node_id: {item.nodeid}"
                )

            service_name, module_name = ReportUtils.convert_nodeid_to_report_group(item.nodeid)
            if service_name is None:
                report_idx = 'other'
            else:
                report_idx = f'{service_name}-{module_name}'

            if report_idx not in all_report_results:
                all_report_results[report_idx] = deepcopy(report_template)
                all_report_results[report_idx]['service'] = service_name if report_idx != 'other' else 'other'
                all_report_results[report_idx]['module'] = module_name if report_idx != 'other' else 'other'

            if item.report.passed:
                all_report_results['all']['passed'] += 1
                all_report_results[report_idx]['passed'] += 1

            if item.report.failed:
                all_report_results['all']['failed'] += 1
                all_report_results[report_idx]['failed'] += 1

            if item.report.skipped:
                all_report_results['all']['skipped'] += 1
                all_report_results[report_idx]['skipped'] += 1

            all_report_results['all']['total'] += 1
            all_report_results[report_idx]['total'] += 1

        reports_list = list()
        for _, report in all_report_results.items():
            if report['total'] == 0:
                continue
            report['pass_rate'] = round(report['passed']/report['total'], 2)
            reports_list.append(report)

        return reports_list


class S3BaseReport():

    def __init__(
        self,
        report_dir_name: str,
        s3_report_path: str,
        report_ext: str
    ):
        self.log = SystemLogger.get_logger()
        self.report_dir_name = report_dir_name
        self.dir_path = os.path.join(os.getcwd(), self.report_dir_name)
        self.s3_report_path = s3_report_path
        self.report_ext = report_ext
        self.create_directory()

    def create_directory(self):
        """
        Create the given directory which is based on project work directory
        """
        p = Path(self.dir_path)
        if not p.exists():
            p.mkdir(parents=True, exist_ok=True)
            self.log.info(f"Create a new directory on {self.dir_path}")

    def get_report_name(self, report_prefix: str = "report") -> str:
        current_time = datetime.now().strftime("%Y%m%d_%H%M%S")
        report_name = f"{report_prefix}-{current_time}.{self.report_ext}"
        return report_name

    def get_report_output_full_path(self, report_prefix: str = "report") -> str:
        """
        Get the report output full path

        Args:
            report_prefix (str, optional): file name prefix. Defaults to "report".
        """
        report_name = self.get_report_name(report_prefix)
        report_full_path = os.path.join(self.dir_path, report_name)
        return report_full_path

    def upload_report(
        self,
        file_name: str,
        upload_delete: bool = True
    ) -> str:
        """
        Upload file to S3 bucket

        Args:
            file_name: which file is uploaded
            upload_delete: delete file after uploading success
        """
        file_base_name = os.path.basename(file_name)
        object_name = os.path.join(self.s3_report_path, file_base_name)
        s3_bucket = S3Bucket()
        obj_url = s3_bucket.upload_file(file_name, object_name=object_name)
        if obj_url and upload_delete:
            self.clean_dir_files()
        return obj_url

    def clean_dir_files(self, ext_pat: Optional[str] = None):
        if ext_pat is None:
            ext_pat = f'*.{self.report_ext}'
        file_pat = os.path.join(self.dir_path, ext_pat)
        files = glob.glob(file_pat)
        try:
            for file in files:
                os.remove(file)
        except Exception as e:
            raise e


class HTMLReport(S3BaseReport):
    """
    Process the report from Pytest-html.

    Generate report pipeline:
        compress the testing result -> upload to S3
    """
    REPORT_DIR_NAME = 'html-report'
    def __init__(self):
        super(HTMLReport, self).__init__(
            report_dir_name=self.REPORT_DIR_NAME,
            s3_report_path=self.REPORT_DIR_NAME,
            report_ext='html'
        )


class ErrorLogSaver(S3BaseReport):

    REPORT_DIR_NAME = 'err-logs'
    def __init__(self):
        super(ErrorLogSaver, self).__init__(
            report_dir_name=self.REPORT_DIR_NAME,
            s3_report_path=self.REPORT_DIR_NAME,
            report_ext='json'
        )

    def get_report_name(self, report_prefix: str = "err_log") -> str:
        return super().get_report_name(report_prefix)

    def write_to_json(self, err_msgs: dict) -> str:
        """
        Output err_msgs as json
        """
        file_name = self.get_report_output_full_path()
        with open(file_name, 'w') as wf:
            json.dump(err_msgs, wf)
        return file_name


class AllureReport():
    """
    Interact with Allure server.

    Generate report pipeline:
        compress testing result -> upload zip file -> generate report

    Args:
        host: If it is None, it will load `ALLURE_SERVER_HOST` from environment.
        port: If it is None, it will load `ALLURE_SERVER_PORT` from environment.
    """
    ALLURE_REPORT_DIR = os.path.join(os.getcwd(), 'allure-report')
    ALLURE_REPORT_BACKUP_DIR = os.path.join(os.getcwd(), 'allure-report-backup')
    ENDPOINT = {
        'result': 'api/result',
        'report': 'api/report',
    }
    REQUESTED_SUCCESS = 201

    def __init__(self, host: Optional[str] = None, port: Optional[str] = None):
        self.log = SystemLogger.get_logger()
        self.create_directories()
        URLRouter.route_map_setter(self.ENDPOINT)
        if not host:
            self.url_router = URLRouter('ALLURE_SERVER_HOST', 'ALLURE_SERVER_PORT')
        else:
            self.url_router = URLRouter(host, port, from_env=False)

    def create_directories(self):
        paths = [
            self.ALLURE_REPORT_DIR,
            self.ALLURE_REPORT_BACKUP_DIR
        ]
        for path in paths:
            p = Path(path)
            if not p.exists():
                p.mkdir(parents=True, exist_ok=True)
                self.log.info(f"Create a new directory on {path}")

    def get_api_url(self, endpoint: str) -> str:
        return self.url_router.get_page_url(endpoint)

    def upload_zip_file(self, upload_file: str) -> dict:
        """
        Send request for uploading zip file to Allure server

        Args:
            upload_file (str): The path of uploading zip file

        Raises:
            FileExistsError: The upload file doesn't exist
            RequestException: Occur the unexpected error while uploading the file to Allure server.

        Returns:
            dict: The response after uploading file to Allure server.
            Example response from allure server:
            {
                "fileName": "allure_20221205_161403.zip",
                "uuid": "dd244949-5e87-4957-994e-999b35763516",
            }
        """
        if not os.path.exists(upload_file):
            raise FileExistsError(f"The upload_file: {upload_file} doesn't exist")

        url = self.get_api_url(endpoint='result')
        file_basename = os.path.basename(upload_file)
        files = [
            ('allureResults',(file_basename, open(upload_file,'rb'), 'application/zip'))
        ]
        res = requests.post(url, files=files)
        if res.status_code != self.REQUESTED_SUCCESS:
            raise RequestException((
                "The request occurs unexpected error "
                f"request_method: POST, request_url: {res.url}, "
                f"request_form_data: {files}"
            ))
        return res.json()

    def generate_report_by_request(
        self,
        result_uuid: str,
        delete_result: bool = True
    ) -> dict:
        """
        Send request to Allure server to generate report

        Args:
            result_uuid (str): The uuid from the response `upload_zip_file`
            delete_result (bool, optional): Whether deleting the zip file after generating the report. Defaults to True.

        Raises:
            RequestException:
                Occur the unexpected error while sending the request to generate the report to Allure server
        Returns:
            dict: The response after generating the report on Allure server.
            Example response body:
            {
                "uuid": "584d3c22-0d2b-4bd9-b271-19a12950a85a",
                "path": "master/666",
                "url": "http://localhost:8080/allure/reports/584d3c22-0d2b-4bd9-b271-19a12950a85a/",
                "latest": "http://localhost:8080/reports/master/666"
            }
        """
        url = self.get_api_url(endpoint='report')
        payload = json.dumps(
            {
                "reportSpec": {
                "path": ["master" ,"666"],
                    "executorInfo": {
                        "buildName": "#666"
                    }
                },
                "results": [result_uuid],
                "deleteResults": delete_result
            }
        )
        headers = {'Content-Type': 'application/json'}
        res = requests.request("POST", url, headers=headers, data=payload)
        if res.status_code != self.REQUESTED_SUCCESS:
            raise RequestException((
                "The request occurs unexpected error "
                f"request_method: POST, request_url: {res.url}, "
                f"payload_data: {payload}"
            ))
        return res.json()

    @classmethod
    def get_allure_report_zipname(cls) -> str:
        """
        Get allure report zip file name.
        The file pattern => allure_`{current_time}`.zip

        Note: The zip file will be generated in `allure-report-backup` directory
        """
        current_time = datetime.now().strftime("%Y%m%d_%H%M%S")
        file_name = f"allure_{current_time}.zip"
        full_file_path = os.path.join(cls.ALLURE_REPORT_BACKUP_DIR, file_name)
        return full_file_path

    def upload_report(self, upload_dir: str, upload_delete: bool = True) -> dict:
        """
        Upload report to allure server and generate the report.
        Integrate `upload_zip_file` and `generate_report_by_request`

        Args:
            upload_dir (str): The path of uploading zip file
            upload_delete (bool, optional): Whether deleting the zip file after generating the report. Defaults to True.

        Raises:
            FileExistsError: The upload file doesn't exist

        Returns:
            dict: The response after generating the report on Allure server
            {
                'uuid': '584d3c22-0d2b-4bd9-b271-19a12950a85a',
                'path': 'master/666',
                'url': 'http://localhost:8080/allure/reports/584d3c22-0d2b-4bd9-b271-19a12950a85a/',
                'latest': 'http://localhost:8080/reports/master/666',
                'zip_file_path': 'Path to zip file',
            }
        """
        zip_output = self.get_allure_report_zipname()
        output_path = ZipTools.zip_directory(upload_dir, zip_output)
        if not output_path:
            raise FileExistsError((
                "Occur unexpected error when compressing the directory, "
                f"upload_dir: {upload_dir}"
            ))
        upload_result = self.upload_zip_file(zip_output)
        report_info = self.generate_report_by_request(upload_result['uuid'], False)
        if upload_delete:
            try:
                os.remove(zip_output)
                report_info['zip_file_path'] = None
            except Exception as e:
                report_info['zip_file_path'] = zip_output
        else:
            report_info['zip_file_path'] = zip_output
        return report_info


class TeamsReport():
    """
    Send teams notification to specific channel
    """
    TEMPLATE_DIR = os.path.join(os.getcwd(), 'report-templates')
    DEFAULT_TEMPLATE_NAME = 'report_template.html'

    def __init__(self):
        self.log = SystemLogger.get_logger()
        self.webhook_url = os.environ.get("TEAMS_WEBHOOK_URL", None)
        if self.webhook_url is None:
            raise ValueError(f"Invalidate TEAMS_WEBHOOK_URL: {self.webhook_url}")
        self.template_env = self.init_template_loader()

    def init_template_loader(self) -> Environment:
        file_loader = FileSystemLoader(self.TEMPLATE_DIR)
        template_env = Environment(loader=file_loader)
        return template_env

    def send_msg(
        self,
        render_kwargs: dict,
        template_name: Optional[str] = None,
    ):
        if template_name is None:
            template_name = self.DEFAULT_TEMPLATE_NAME
        report_template = self.template_env.get_template(template_name)
        output = report_template.render(**render_kwargs)
        self.log.info(
            "Start sending notification to teams, using "
            f"{template_name} as template, render_kwargs: {render_kwargs}, "
            f"and webhook_url: {self.webhook_url}"
        )
        teams_msg = pymsteams.connectorcard(self.webhook_url)
        teams_msg.text(output)
        teams_msg.summary('Test Result')
        teams_msg.send()


class ReportGenerator(metaclass=SingletonMeta):

    def __init__(self):
        self.log = SystemLogger.get_logger()
        self.register_cls = {}
        self.report_results = []
        self.err_msg = {}
        self.upload_status = TestStatus.SUCCESS

    def add_report(self, cls: object, register_name: str = None):
        self.register_cls[register_name] = cls
        self.log.info(f"The {cls.__name__} has been registered.")

    def register(self, register_name: str = None):
        def decorator(cls: object):
            self.add_report(cls, register_name)
            return cls
        return decorator

    def invoke_notification(self, session: pytest.Session) -> bool:
        """
        Check whether invoking the notification

        Args:
            session (pytest.Session)
        """
        invoke_notification = getattr(
            session.config.option,
            'invoke_setting_notification'
        )
        allure_report_path = getattr(
            session.config.option,
            'allure_report_dir',
            None
        )
        html_report_path = getattr(
            session.config.option,
            'htmlpath',
            None
        )
        self.log.debug(
            f"invoke_setting_notification: {invoke_notification}, "
            f"allure_report_path: {allure_report_path}, "
            f"html_report_path: {html_report_path}"
        )
        if invoke_notification and any([allure_report_path, html_report_path]):
            return True
        else:
            return False

    def get_report_base_info(self, session: pytest.Session) -> dict:
        """
        Get report base information from pytest.Session object

        Args:
            session (pytest.Session): _description_
        """
        date_fmt = '%Y-%m-%d %H:%M:%S'
        reporter: TerminalReporter = session.config.pluginmanager.get_plugin('terminalreporter')
        end_time = time.time()
        duration = round(end_time - reporter._sessionstarttime, 2)
        start_datetime = datetime.fromtimestamp(reporter._sessionstarttime).strftime(date_fmt)
        end_datetime = datetime.fromtimestamp(end_time).strftime(date_fmt)
        report_info = {
            'env': os.environ.get('DEPLOYMENT_ENV', None),
            'upload_status': self.upload_status,
            'start_time': start_datetime,
            'end_time': end_datetime,
            'elapsed_time': duration,
        }
        return report_info

    def get_test_summary_result(self, session: pytest.Session) -> List[dict]:
        summary_result = list()
        num_process = getattr(session.config.option, 'numprocesses')
        if num_process:
            try:
                summary_result = ReportUtils.summarize_parallel_result(session)
            except Exception as e:
                self.log.debug(e)
        else:
            try:
                summary_result = ReportUtils.summarize_test_result(session.items)
            except Exception as e:
                self.log.debug(e)

        # Print the summary result
        for result in summary_result:
            if result['service'] == 'all':
                self.log.debug(
                    f"Totally execute {result['total']} cases => {result['passed']} passed, "
                    f"{result['failed']} failed, {result['skipped']} skipped, "
                    f"pass_rate: {result['pass_rate']}"
                )
                break
        return summary_result

    @SynchronizedLock.lock()
    def collect_error_msg(self, key: str, value: str) -> dict:
        """
        Write error messages to `self.err_msg` and ensure its thread safe
        """
        self.err_msg[key] = value
        return self.err_msg

    @SynchronizedLock.lock()
    def collect_report_info(self, report_info: dict) -> List[dict]:
        """
        Write report result to `self.report_results` and ensure its thread safe
        """
        if 'report_type' not in report_info or 'url' not in report_info:
            raise ValueError("report_info must include key `report_type` and `url`")
        self.report_results.append(report_info)
        return self.report_results

    def upload_allure_report(self, session: pytest.Session) -> str:
        """
        Upload test result to allure server

        Args:
            config_options (pytest.Session): Get arguments from command line
        """
        config_options = session.config.option
        upload_result = dict()
        err_msg = ""
        allure_report_path = getattr(config_options, 'allure_report_dir', None)
        if allure_report_path:
            allure_server_host = getattr(config_options, 'allure_server_host', None)
            allure_server_port = getattr(config_options, 'allure_server_port', None)
            try:
                allure_report = AllureReport(allure_server_host, allure_server_port)
                upload_result = allure_report.upload_report(allure_report_path)
            except Exception:
                err_msg = traceback.format_exc()
                self.log.error(err_msg)

        # Collect the report info and error messages
        report_url = upload_result.get('url', '')
        if report_url:
            report_info = {'report_type': 'allure_report', 'url': report_url}
            self.collect_report_info(report_info)
            self.log.debug(
                "[AllureReport] Successfully upload result to "
                f"allure server, report_url: {report_url}"
            )
        if err_msg:
            self.collect_error_msg('upload_allure_server_failed', err_msg)
        return report_url

    def upload_html_report(self, session: pytest.Session) -> str:
        """
        Upload html report to S3 bucket

        Args:
            config_options (pytest.Session.config.option): Get arguments from command line
        """
        config_options = session.config.option
        # For uploading html report to S3
        object_url, err_msg = "", ""
        html_report_path = getattr(config_options, 'htmlpath', None)
        if html_report_path:
            try:
                html_report = HTMLReport()
                object_url = html_report.upload_report(html_report_path)
            except Exception as e:
                err_msg = traceback.format_exc()
                self.log.error(err_msg)
        # Collect the report info and error messages
        if object_url:
            report_info = {'report_type': 'html_report', 'url': object_url}
            self.collect_report_info(report_info)
            self.log.debug(
                "[htmlReport] Successfully upload html report to "
                f"s3 bucket, object_url: {object_url}"
            )
        if err_msg:
            self.collect_error_msg('upload_html_report_failed', err_msg)
        return object_url

    def upload_screen_recording(self, session: pytest.Session) -> str:
        """
        Upload mobile screen recording to S3 bucket

        Args:
            config_options (pytest.Session): Get arguments from command line
        """
        raise NotImplementedError

    def upload_error_log(self) -> str:
        object_url = ""
        if self.err_msg:
            self.upload_status = TestStatus.FAILED
            try:
                err_log_saver = ErrorLogSaver()
                err_logs_fpath = err_log_saver.write_to_json(self.err_msg)
                object_url = err_log_saver.upload_report(err_logs_fpath)
            except Exception as e:
                self.log.error(e)
                raise e
        if object_url:
            info = {'report_type': 'error_logs', 'url': object_url}
            self.collect_report_info(info)
        return object_url

    def upload_reports(self, session: pytest.Session, threaded: bool = True) -> List[dict]:
        """
        Upload all reports and results and collect upload results
        """
        upload_report_funcs = [
            self.upload_allure_report,
            self.upload_html_report,
            # self.upload_screen_recording,
        ]
        if threaded:
            all_tasks: List[Future] = list()
            with PoolExecutor.get_thread_executor() as executor:
                for func in upload_report_funcs:
                    task = executor.submit(func, session)
                    all_tasks.append(task)

            results = [t.result() for t in all_tasks]
        else:
            results = [func(session) for func in upload_report_funcs]
        # It must be invoked at last, and it can't run at pool
        self.upload_error_log()
        return results

    def generate_reports(self, session: pytest.Session):
        """
        Upload all reports and send notification to teams

        Args:
            session (pytest.Session)
        """
        summary_result = self.get_test_summary_result(session)
        if self.invoke_notification(session):
            _ = self.upload_reports(session)
            report_info = self.get_report_base_info(session)
            report_info['report_results'] = self.report_results
            report_info['summary_result'] = summary_result
            teams_report = TeamsReport()
            teams_report.send_msg(report_info)
            self.log.debug('Successfully send teams notification')
        else:
            self.log.debug("Not invoke the setting notification.....")
