from bo_shared.bo_service_pages import BoService
from utils.url_router import URLRouter

class BoPageRouter(URLRouter):

    ROUTE_MAP = {
        BoService.Shared.Shared: "",
        BoService.Shared.LogIn: "auth/sign-in",
        BoService.Shared.HomePage: "",
        # For Rebate service
        BoService.Rebate.DashBoard: "rebate-service/dashboard",
        BoService.Rebate.Management: "rebate-service/management",
        BoService.Rebate.RebateLevel: "rebate-service/member-exclusion/member-rebate-level",
        BoService.Rebate.RebateBlackList: "rebate-service/member-exclusion/rebate-blacklist",
        # For Role service
        BoService.Role.UserManagement: 'role-service/user-management',
        BoService.Role.RoleManagement: 'role-service/role-management',
        BoService.Role.PolicyManagement: 'role-service/policy-management',
        # For Bonus service
        BoService.Bonus.Management: 'bonus-service/management',
        BoService.Bonus.Finance: 'bonus-service/report/finance',
        BoService.Bonus.Player: 'bonus-service/report/player',
        # For Excluded Game
        BoService.ExcludedGame.ServiceName: 'excluded-game',
    }

    def __init__(self):
        super(BoPageRouter, self).__init__(
            env_host="BO_HOST",
            env_port="BO_PORT"
        )
