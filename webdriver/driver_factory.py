from enum import Enum
from typing import Optional, List

from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.firefox.service import Service as FirefoxService

from config_model import Config

from utils.utils import SystemLogger


log = SystemLogger.get_logger()

class Browser(str, Enum):
    CHROME = 'chrome'
    FIREFOX = 'firefox'
    OPERA = 'opera'
    EDGE = 'edge'
    IE = 'ie'


def build_capabilities(
        browser_type: str,
        capabilities: dict
    ) -> dict:
    caps = dict()
    browser_type = browser_type.lower()
    if browser_type == Browser.CHROME:
        caps.update(webdriver.DesiredCapabilities.CHROME.copy())

    elif browser_type == Browser.FIREFOX:
        caps.update(webdriver.DesiredCapabilities.FIREFOX.copy())

    elif browser_type == Browser.OPERA:
        caps.update(webdriver.DesiredCapabilities.OPERA.copy())

    elif browser_type == Browser.EDGE:
        caps.update(webdriver.DesiredCapabilities.EDGE.copy())

    elif browser_type == Browser.IE:
         caps.update(webdriver.DesiredCapabilities.INTERNETEXPLORER.copy())

    else:
        raise ValueError(f"{browser_type} is not supported.")

    if capabilities:
        caps.update(capabilities)

    return caps


def build_options(
        browser_type: str,
        browser_options: List[str],
        experimental_options: Optional[List[dict]],
        extension_paths: Optional[List[str]]
    ):
    browser_type = browser_type.lower()
    if browser_type == Browser.CHROME:
        options = webdriver.ChromeOptions()

    elif browser_type == Browser.FIREFOX:
        options = webdriver.FirefoxOptions()

    elif browser_type == Browser.OPERA:
        options = webdriver.ChromeOptions()

    elif browser_type == Browser.EDGE:
        options = webdriver.EdgeOptions()

    elif browser_type == Browser.IE:
        options = webdriver.IeOptions()

    else:
        raise ValueError(f"{browser_type} is not supported.")

    for option in browser_options:
        if option.startswith('--'):
            options.add_argument(option)
        else:
            options.add_argument(f'--{option}')

    if experimental_options:
        for exp_option in experimental_options:
            ((name, value), ) = exp_option.items()
            options.add_experimental_option(name, value)

    if extension_paths:
        for ext_path in extension_paths:
            options.add_extension(ext_path)

    return options


def build_chrome(
        driver_path: str,
        browser_options: List[str],
        capabilities: dict,
        experimental_options: Optional[List[dict]],
        extension_paths: Optional[List[str]],
        webdriver_kwargs: Optional[dict]
    ) -> WebDriver:
    options = build_options(
        Browser.CHROME,
        browser_options,
        experimental_options,
        extension_paths
    )
    driver = webdriver.Chrome(
        service=ChromeService(driver_path),
        options=options,
        desired_capabilities=capabilities,
        **(webdriver_kwargs or {})
    )
    driver.execute_cdp_cmd('Performance.enable', {})

    return driver


def build_firefox(
        driver_path: str,
        browser_options: List[str],
        capabilities: dict,
        experimental_options: Optional[List[dict]],
        extension_paths: Optional[List[str]],
        webdriver_kwargs: Optional[dict]
    ) -> WebDriver:
    options = build_options(
        Browser.FIREFOX,
        browser_options,
        experimental_options,
        extension_paths
    )
    caps = build_capabilities(Browser.FIREFOX, capabilities)
    driver = webdriver.Firefox(
        service=FirefoxService(driver_path),
        capabilities=caps,
        options=options,
        **(webdriver_kwargs or {})
    )
    return driver


def build_remote(
        browser_type: str,
        remote_url: str,
        capabilities: dict,
        browser_options: List[str],
        experimental_options: Optional[List[dict]],
        extension_paths: Optional[List[str]],
        webdriver_kwargs: Optional[dict]
    ) -> WebDriver:
    browser_type = browser_type.lower()
    caps = build_capabilities(browser_type, capabilities)
    options = build_options(
        browser_type,
        browser_options,
        experimental_options,
        extension_paths
    )
    driver = webdriver.Remote(
        command_executor=remote_url,
        desired_capabilities=caps,
        options=options,
        **(webdriver_kwargs or {})
    )
    return driver


def build_from_config(config: Config) -> WebDriver:
    if config.driver.remote_url:
        log.info(f"Create webdriver with remote: {config.driver.remote_url}")
        return build_remote(
            browser_type=config.driver.browser,
            remote_url=config.driver.remote_url,
            capabilities=config.driver.capabilities,
            browser_options=config.driver.options,
            experimental_options=config.driver.experimental_options,
            extension_paths=config.driver.extension_paths,
            webdriver_kwargs=config.driver.webdriver_kwargs
        )
    browser = config.driver.browser.lower()
    if browser == Browser.CHROME:
        log.info(f"Create webdriver with local driver path: {config.driver.local_path}")
        return build_chrome(
            driver_path=config.driver.local_path,
            browser_options=config.driver.options,
            capabilities=config.driver.capabilities,
            experimental_options=config.driver.experimental_options,
            extension_paths=config.driver.extension_paths,
            webdriver_kwargs=config.driver.webdriver_kwargs
        )
    elif browser == Browser.FIREFOX:
        log.info(f"Create webdriver with local driver path: {config.driver.local_path}")
        return build_firefox(
            driver_path=config.driver.local_path,
            browser_options=config.driver.options,
            capabilities=config.driver.capabilities,
            experimental_options=config.driver.experimental_options,
            extension_paths=config.driver.extension_paths,
            webdriver_kwargs=config.driver.webdriver_kwargs
        )
    else:
        raise ValueError(f'{browser} is not supported')