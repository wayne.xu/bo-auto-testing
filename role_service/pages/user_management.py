import time

import allure

from typing import List
from bo_shared.bo_service_pages import BoService
from role_service.pages.shared_page import RoleSharedPage
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from rebate_service.pages.rebate_login import RebateLogIn
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import (
    TimeoutException,
    NoSuchElementException,
    StaleElementReferenceException,
    ElementClickInterceptedException
)


class RoleUserManagement(RoleSharedPage):

    PAGE_NAME = BoService.Role.UserManagement

    def __init__(self, driver: WebDriver):
        super(RoleUserManagement, self).__init__(driver)

    # Input User Name
    @allure.step("Input User Name")
    def input_user_name(self, user_name: str):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="name"]'
        ).send_keys(user_name)

    # Submit filter condition
    @allure.step("Submit filter condition")
    def submit_search(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionSearch"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Get current user name
    @allure.step("Get current user name")
    def get_table_element(self, table: str) -> tuple:
        table_map = {
            'name': {'row': 1, 'column': 1},
            'teamrole': {'row': 1, 'column': 2},
            'job': {'row': 1, 'column': 4},
            'status': {'row': 1, 'column': 6},
        }
        table_xpath_format = (
            '//div[@data-testid="containerUserList"]//table//tr{row}//td{column}'
        )
        coordinate = table_map[table]
        target_table_xpath = self.xpath_editor(table_xpath_format, **coordinate)

        table_element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            target_table_xpath
        ).text
        print("this is", table_element)
        return table_element

    # Click the Detail button for 1st User
    @allure.step("Click the Detail button for 1st user")
    def click_the_1st_user_detail(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '(//button[@data-testid="actionViewDetail"])[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Activate User
    @allure.step("Activate User")
    def act_user(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionActivate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Deactivate User
    @allure.step("Deactivate User")
    def deact_user(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionDeactivate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Confirm Deactivate User
    @allure.step("Confirm Deactivate Role")
    def select_confirm_deactivate(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-content"]//button[2]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Click the System Role Edit button
    @allure.step("Click the System Role Edit button")
    def click_system_role_edit(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionOpenEditUserSystemRoleModal"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Click the Team Role Edit button
    @allure.step("Click the Team Role Edit button")
    def click_team_role_edit(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//a[@data-testid="actionUpdateTeamRole"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Clear System Role
    @allure.step("Clear System Role")
    def clear_system_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//span[@class='ant-select-clear']"
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Open System Role Selector
    @allure.step("Open System Role Selector")
    def open_system_role_selector(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@data-testid="profileNameList"]//div//div'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select Role Manager
    @allure.step("Select Role Manager")
    def select_role_manager(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@label="Role Manager"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select Role Assigner
    @allure.step("Select Role Assigner")
    def select_role_assigner(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@label="Role Assigner"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select Policy Maker
    @allure.step("Select Policy Maker")
    def select_policy_maker(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@label="Policy Maker"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select Viewer
    @allure.step("Select Viewer")
    def select_viewer(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@label="Viewer"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Click Apply Button
    @allure.step("Click Apply Button")
    def click_apply_btn(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionApply"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Input Name for team role in user detail page
    @allure.step("Input Name for team role in user detail page")
    def input_team_role_name(self, first_parent_role: str):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@data-testid="actionSearchRole"]'
        ).send_keys(first_parent_role)

    # Select the Target team role to User
    @allure.step("Select the Target team role to User")
    def select_target_team_role_to_user(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@data-testid="ADMIN"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Click the Update button to save result
    @allure.step("Click the Update button to save result")
    def click_the_update_btn(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionUpdate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()