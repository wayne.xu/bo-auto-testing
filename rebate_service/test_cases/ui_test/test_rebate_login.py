import time

import allure
import pytest

from rebate_service.pages.rebate_login import RebateLogIn
from utils.utils import SystemLogger

from utils.envs import Envs
from utils.decorators import execute_env


class TestRebateLogin():

    @execute_env(envs=Envs.ALL.value)
    @allure.story("Test login function")
    def test_login(self, driver, test_account):
        rebate_login = RebateLogIn(driver)
        rebate_login.visit_page()
        rebate_login.click_continue()
        rebate_login.input_login_email(test_account['email'])
        rebate_login.input_login_password(test_account['password'])
        rebate_login.click_submit_button()
        assert rebate_login.url_router.is_page_url(
            rebate_login.driver.current_url,
            'home_page'
        )