from selenium.webdriver.remote.webdriver import WebDriver

class AnyEC():
    """
    Use with WebDriverWait to combine expected_conditions
    in an OR.
    """
    def __init__(self, *ec):
        self.ecs = ec

    def __call__(self, driver: WebDriver):
        for fn in self.ecs:
            try:
                res = fn(driver)
                if res:
                    return True
            except Exception as e:
                pass