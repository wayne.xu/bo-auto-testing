import time
from typing import List

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC

from bo_shared.bo_service_pages import BoService
from rebate_service.pages.rebate_homepage import RebateHomePage


class RebateMemberBlackList(RebateHomePage):

    PAGE_NAME = BoService.Rebate.RebateBlackList

    def __init__(self, driver: WebDriver):
        super(RebateMemberBlackList, self).__init__(driver)