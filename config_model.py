from typing import List

from pydantic import BaseModel


default_options = [
    '--disable-gpu',
    '--no-sandbox',
]

# For getting the browser log
default_capabilities = {
    'goog:loggingPrefs': {
        "performance": "ALL",
        "browser": "ALL",
        "client": "ALL",
        "server": "ALL",
        "driver": "ALL",
    },
    'timeouts': {
        'implicit': 10 * 1000,
        'pageLoad': 30 * 1000,
    }
}


class DriverConfig(BaseModel):
    browser: str = 'chrome'
    remote_url: str = ''
    wait_time: int = 10 # Implicitly_wait
    page_load_wait_time: int = 30
    options: List[str] = default_options or {}
    capabilities: dict = default_capabilities or {}
    experimental_options: List[dict] = []
    extension_paths: List[str] = []
    webdriver_kwargs: dict = {}
    # version: str = 'latest'
    local_path: str = "./drivers/chrome/chromedriver"


class LogConfig(BaseModel):
    log_level: str = 'debug'
    screenshot_on: bool = True
    export_browser_log: bool = False
    output_log_type: str = 'performance'
    version: str = ''


class ViewportConfig(BaseModel):
    maximize: bool = True
    width: int = 1440
    height: int = 900
    orientation: str = 'portrait'


class Config(BaseModel):
    driver: DriverConfig = DriverConfig()
    viewport: ViewportConfig = ViewportConfig()
    log: LogConfig = LogConfig()

# How to set headless mode on selenium?
# Add '--headless' into DirverConfig.options
# '--incognito'
# '--disable-dev-shm-usage',
# 'goog:chromeOptions': {
#     "perfLoggingPerfs": {
#         'enableNetwork': True
#     }
# }
