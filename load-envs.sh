#!/bin/bash

ENV_FILE='./config/.env'

load_envs() {
    ENV_FILE=${1}
    # Check file whether existing or not
    if [ -f ${ENV_FILE} ]; then
        # export variable into environment
        export $( cat ${ENV_FILE} | sed 's/^#.*//g' | xargs )
    else
       >&2 echo "FileExistError: ${ENV_FILE} doesn't exist"
    fi
}

load_envs ${ENV_FILE}