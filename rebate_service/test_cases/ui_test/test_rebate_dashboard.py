import time
import allure

import pytest
import pytest_check as check
from rebate_service.pages.rebate_dashboard import RebateDashboard

from datetime import date
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from utils.envs import Envs
from utils.utils import HelperFuncs, SystemLogger
from utils.decorators import execute_env
from bo_shared.components.date_picker import DatePicker, DATE_PICKER_INFO
from bo_shared.components.select_dropdown import SelectDropDown, DROP_DOWN_INFO
from bo_shared.ui_shared import (
    PlatFormOptions, ProductOptions, RecurrenceOptions
)


log = SystemLogger.get_logger()


class TestRebateDashboardArgs:

    REBATE_CALENDAR_DATE_FORMAT = '%Y-%m'
    SCROLL_DOWN_PERCENT = 2/5
    MORE_THAN_THREE_MONTHS = 5
    TODAY_BTN_MONTH = 1

    ALL_MONTH_INFO = HelperFuncs.get_calendar_clickable_unclickable_months()
    NUM_MONTH_DATA_FOR_CALENDAR = 3


@pytest.fixture()
def test_dashboard_chart_title() -> str:
    return 'Total Turnover & Total Rebate Given'


@pytest.fixture()
def test_select_product_names() -> tuple:
    selected_product_names = (
        ProductOptions.KENO.value,
        ProductOptions.RNG.value,
        ProductOptions.SPORTSBOOK.value,
    )
    return selected_product_names


@pytest.fixture()
def default_product_selection() -> str:
    return ProductOptions._DEFAULT.value


@pytest.fixture()
def default_platform_selection() -> str:
    return PlatFormOptions._DEFAULT.value


@pytest.fixture()
def default_recurrence_selection() -> str:
    return RecurrenceOptions._DEFAULT.value


@pytest.fixture()
def expected_event_types():
    expected_types = {
        RecurrenceOptions.NO_RECURRENCE.value,
        RecurrenceOptions.DAILY.value,
        RecurrenceOptions.WEEKLY.value,
        RecurrenceOptions.MONTHLY.value,
    }
    return expected_types


@pytest.fixture()
def current_year_month() -> str:
    return datetime.today().strftime(
        TestRebateDashboardArgs.REBATE_CALENDAR_DATE_FORMAT
    )


@pytest.fixture()
def today_after_num_months() -> str:
    next_month = datetime.today() + relativedelta(
        months=TestRebateDashboardArgs.MORE_THAN_THREE_MONTHS
    )
    return next_month.strftime(
        TestRebateDashboardArgs.REBATE_CALENDAR_DATE_FORMAT
    )


@pytest.fixture()
def last_month() -> str:
    next_month = datetime.today() - relativedelta(months=1)
    return next_month.strftime(
        TestRebateDashboardArgs.REBATE_CALENDAR_DATE_FORMAT
    )


@allure.feature('Rebate Service dashboard test')
class TestRebateDashboard():
    """
    This class is used for test RebateDashboard
    """
    # Case 1 By default, check the Total TO & Rebate Given in Chart
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 1 By default, check the Total TO & Rebate Given in Chart")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.link('https://hermes.devgambit.net/browse/PSS-246',
                 name='[Dashboard][Chart] Test scripts for dashboard charts')
    @allure.story("Test function lists")
    def test_dashboard_first_chart_title(
            self,
            driver,
            test_account,
            test_dashboard_chart_title
        ):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard_element = dashboard.get_dashboard_chart_title()
        # Check its length
        assert dashboard_element == test_dashboard_chart_title

    # Case 2 By default, select the past 3 months data
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 2 By default, select the past 3 months data")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.link('https://hermes.devgambit.net/browse/PSS-247',
                 name='[Dashboard][Filter] Test scripts for filter date picker')
    @allure.story("Test for the default time range")
    def test_default_time_range(self, driver, test_account):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        date_picker = DatePicker(
            driver,
            'chart_filter_date',
            DATE_PICKER_INFO['chart_filter_date']['data_test_id']
        )
        start_date = date_picker.get_start_date_value(to_datetime_type=True)
        end_date = date_picker.get_end_date_value(to_datetime_type=True)
        expected_date = start_date + relativedelta(months=3)
        tolerance_days = 1
        tolerance_range = {
            expected_date - timedelta(days=tolerance_days),
            expected_date,
            expected_date + timedelta(days=tolerance_days)
        }
        assert end_date in tolerance_range

    # Case 3 Select more than past 1 year (manually)
    # Case 4 Select from past 1 year until now (manually)

    # Case 5 Select more than maximum 3 months
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 5 Select more than maximum 3 months")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.link('https://hermes.devgambit.net/browse/PSS-247',
                 name='[Dashboard][Filter] Test scripts for filter date picker')
    @allure.story("Test for the date picker function")
    def test_date_picker_start_date_limitation(self, driver, test_account):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        date_picker = DatePicker(
            driver,
            'chart_filter_date',
            DATE_PICKER_INFO['chart_filter_date']['data_test_id']
        )
        date_picker.click_end_date_input()
        for _ in range(3):
            date_picker.click_date_panel_header('next_month')
        selectable_content = date_picker.get_date_picker_end_side_selectable_content()
        assert len(selectable_content) == 0

    # Case 6 By default, no select
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 6 By default, no select (Product)")
    @allure.severity(allure.severity_level.MINOR)
    @allure.link('https://hermes.devgambit.net/browse/PSS-282',
                 name='[Dashboard][Filter] Test scripts for filter product')
    @allure.story("Test for the default selection product")
    def test_default_product_item(self, driver, test_account, default_product_selection):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        product_item = dashboard.get_product_selection(get_default_value=True)
        check.equal(product_item[0], default_product_selection)

    # Case 7 Multiple Select all of item
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 7 Multiple Select all of item (Product)")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.link('https://hermes.devgambit.net/browse/PSS-282',
                 name='[Dashboard][Filter] Test scripts for filter product')
    @allure.story("Test for the multi-select item for product")
    def test_select_target_products(
            self,
            driver,
            test_account,
            test_select_product_names
        ):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.select_target_products(
            select_product_names=test_select_product_names
        )
        current_displayed = dashboard.get_product_selection()
        check.equal(len(test_select_product_names), len(current_displayed))
        for test, current in zip(test_select_product_names, current_displayed):
            check.equal(test, current)

    # Case 8 By default, no select (Platform)
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 8 By default, no select (Platform)")
    @allure.severity(allure.severity_level.MINOR)
    @allure.link('https://hermes.devgambit.net/browse/PSS-248',
                 name='[Dashboard][Filter] Test scripts for filter platform')
    @allure.story("Test for the default select item for platform")
    def test_default_platform_item(
            self,
            driver,
            test_account,
            default_platform_selection
        ):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dropdown = SelectDropDown(
            driver,
            DROP_DOWN_INFO['dashboard_platform']['form_data_test_id'],
            DROP_DOWN_INFO['dashboard_platform']['form_info_data_test_id'],
            DROP_DOWN_INFO['dashboard_platform']['options_locate_id'],
            'dashboard_platform'
        )
        platform_item = dropdown.get_selection_default_value()
        check.equal(platform_item, default_platform_selection)

    # Case 9 Select Desktop & Case 10 Select Mobile
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 9 Select Desktop & Case 10 Select Mobile")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.link('https://hermes.devgambit.net/browse/PSS-248',
                 name='[Dashboard][Filter] Test scripts for filter platform')
    @allure.story("Test for select Desktop for platform")
    @pytest.mark.parametrize(
        "platform_name",
        [PlatFormOptions.DESKTOP.value, PlatFormOptions.MOBILE.value]
    )
    def test_select_platform(self, driver, test_account, platform_name):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dropdown = SelectDropDown(
            driver,
            DROP_DOWN_INFO['dashboard_platform']['form_data_test_id'],
            DROP_DOWN_INFO['dashboard_platform']['form_info_data_test_id'],
            DROP_DOWN_INFO['dashboard_platform']['options_locate_id'],
            'dashboard_platform'
        )
        dropdown.click_select_dropdown()
        dropdown.select_options([platform_name,])
        selection_items = dropdown.get_current_selection_items()
        check.equal(selection_items[0], platform_name)

    @execute_env(envs=Envs.ALL.value)
    @allure.title("clear product all selections")
    def test_clear_all_product_selections(
            self,
            driver,
            test_account,
            test_select_product_names,
            default_product_selection
        ):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.select_target_products(
            select_product_names=test_select_product_names
        )
        dashboard.clear_all_selections('product')
        time.sleep(3)
        current_product_name = dashboard.get_product_selection(get_default_value=True)
        assert default_product_selection == current_product_name[0]

    @execute_env(envs=Envs.ALL.value)
    @allure.title("Clean product the specific selections")
    def test_clear_target_product_selections(
            self,
            driver,
            test_account,
            test_select_product_names
        ):
        clear_names = {
            ProductOptions.KENO.value,
            ProductOptions.RNG.value,
        }
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.select_target_products(
            select_product_names=test_select_product_names
        )
        test_select_product_names = set(test_select_product_names)
        expected_names = test_select_product_names.difference(clear_names)
        dashboard.clear_product_selection_by_name(clear_names)
        # Get current product selections and convert to set
        current_product_names = dashboard.get_product_selection()
        current_product_names = set(current_product_names)
        assert expected_names == current_product_names

    @execute_env(envs=Envs.ALL.value)
    @allure.title("Clean platform selection")
    def test_clear_platform_selection(
            self,
            driver,
            test_account,
        ):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dropdown = SelectDropDown(
            driver,
            DROP_DOWN_INFO['dashboard_platform']['form_data_test_id'],
            DROP_DOWN_INFO['dashboard_platform']['form_info_data_test_id'],
            DROP_DOWN_INFO['dashboard_platform']['options_locate_id'],
            'dashboard_platform'
        )
        dropdown.click_select_dropdown()
        dropdown.select_options([PlatFormOptions.DESKTOP.value,])
        dropdown.clear_selection()
        selection_items = dropdown.get_current_selection_items()
        is_empty = False if selection_items else True
        check.equal(is_empty, True)

    # Case 11 By default, no select (Rebate Recurrence)
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 11 By default, no select (Rebate Recurrence)")
    @allure.severity(allure.severity_level.MINOR)
    @allure.link('https://hermes.devgambit.net/browse/PSS-250',
                 name='[Dashboard][Filter] Test scripts for filter rebate recurrence')
    @allure.story("Test for the default select item for Rebate recurrence")
    def test_default_recurrence(
            self,
            driver,
            test_account,
            default_recurrence_selection
        ):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dropdown = SelectDropDown(
            driver,
            DROP_DOWN_INFO['dashboard_recurrence']['form_data_test_id'],
            DROP_DOWN_INFO['dashboard_recurrence']['form_info_data_test_id'],
            DROP_DOWN_INFO['dashboard_recurrence']['options_locate_id'],
            'dashboard_recurrence'
        )
        default_value = dropdown.get_selection_default_value()
        check.equal(default_value, default_recurrence_selection)

    # Case 12 Select Daily, Weekly(case 13), Monthly(case 14), No Repeat
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 12 Select Recurrence Options")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.link('https://hermes.devgambit.net/browse/PSS-250',
                 name='[Dashboard][Filter] Test scripts for filter rebate recurrence')
    @allure.story("Test for select Rebate recurrence")
    @pytest.mark.parametrize(
        "recurrence_option",
        [
            RecurrenceOptions.NO_RECURRENCE.value,
            RecurrenceOptions.DAILY.value,
            RecurrenceOptions.WEEKLY.value,
            RecurrenceOptions.MONTHLY.value,
        ]
    )
    def test_select_recurrence(self, driver, test_account, recurrence_option):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dropdown = SelectDropDown(
            driver,
            DROP_DOWN_INFO['dashboard_recurrence']['form_data_test_id'],
            DROP_DOWN_INFO['dashboard_recurrence']['form_info_data_test_id'],
            DROP_DOWN_INFO['dashboard_recurrence']['options_locate_id'],
            'dashboard_recurrence'
        )
        dropdown.click_select_dropdown()
        dropdown.select_options([recurrence_option,])
        selection_item = dropdown.get_current_selection_items()
        check.equal(selection_item[0], recurrence_option)

    @execute_env(envs=Envs.ALL.value)
    @allure.title("Clean Select Recurrence Selection")
    def test_clear_recurrence_selection(
            self,
            driver,
            test_account
        ):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dropdown = SelectDropDown(
            driver,
            DROP_DOWN_INFO['dashboard_recurrence']['form_data_test_id'],
            DROP_DOWN_INFO['dashboard_recurrence']['form_info_data_test_id'],
            DROP_DOWN_INFO['dashboard_recurrence']['options_locate_id'],
            'dashboard_recurrence'
        )
        dropdown.click_select_dropdown()
        dropdown.select_options([RecurrenceOptions.WEEKLY.value,])
        dropdown.clear_selection()
        current_selections = dropdown.get_current_selection_items()
        is_empty = False if current_selections else True
        check.equal(is_empty, True)

    # Case 15 By default, selected
    # Case 16 Unselect
    # Case 17 By default, selected
    # Case 18 Unselect
    # Case 19 By default, selected
    # Case 20 Unselect

    # Case 21 By default, selected on current month
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 21 By default, selected on current month")
    @allure.severity(allure.severity_level.NORMAL)
    @allure.link('https://hermes.devgambit.net/browse/PSS-254',
                 name='[Dashboard][Calendar] Test scripts for Time selector')
    @allure.story("Test for time of calendar")
    def test_calendar_current_time(self, driver, test_account):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.scroll_down_percent(
            TestRebateDashboardArgs.SCROLL_DOWN_PERCENT
        )
        calendar_current_date = dashboard.get_calendar_current_date()
        current_time = date.today().strftime("%Y-%m-%d")
        check.equal(calendar_current_date, current_time)

    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 22 Click the preview month button")
    @allure.severity(allure.severity_level.MINOR)
    @allure.link('https://hermes.devgambit.net/browse/PSS-254',
                 name='[Dashboard][Calendar] Test scripts for Time selector')
    @allure.story("Test for the Click the preview month button")
    def test_calendar_preview_month_btn(self, driver, test_account, last_month):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.scroll_down_percent(
            TestRebateDashboardArgs.SCROLL_DOWN_PERCENT
        )
        dashboard.click_calendar_btn('preview_month')
        calendar_year_month = dashboard.get_calendar_pick_year_month()
        assert calendar_year_month == last_month

    # Case 22 Click the Today button
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 22 Click the Today button")
    @allure.severity(allure.severity_level.MINOR)
    @allure.link('https://hermes.devgambit.net/browse/PSS-254',
                 name='[Dashboard][Calendar] Test scripts for Time selector')
    @allure.story("Test for the click the today button")
    def test_calendar_today_btn(self, driver, test_account, current_year_month):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.scroll_down_percent(
            TestRebateDashboardArgs.SCROLL_DOWN_PERCENT
        )
        num_month = TestRebateDashboardArgs.TODAY_BTN_MONTH
        for _ in range(num_month):
            dashboard.click_calendar_btn('preview_month')
            time.sleep(1.5)
        dashboard.click_calendar_btn('today')
        calendar_year_month = dashboard.get_calendar_pick_year_month()
        assert calendar_year_month == current_year_month

    # Case 23 Select past more than 3 months
    @execute_env(envs=Envs.ALL.value)
    @allure.title("Case 23 Select past more than 3 months")
    @allure.severity(allure.severity_level.MINOR)
    @allure.link('https://hermes.devgambit.net/browse/PSS-254',
                 name='[Dashboard][Calendar] Test scripts for Time selector')
    @allure.story("Test for the select past more than 3 months")
    def test_select_more_than_three_month(
            self,
            driver,
            test_account,
            today_after_num_months
        ):
        num_month = TestRebateDashboardArgs.MORE_THAN_THREE_MONTHS
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.scroll_down_percent(
            TestRebateDashboardArgs.SCROLL_DOWN_PERCENT
        )
        for _ in range(num_month):
            dashboard.click_calendar_btn('next_month')
            time.sleep(1.5)
        calendar_year_month = dashboard.get_calendar_pick_year_month()
        assert calendar_year_month == today_after_num_months

    @execute_env(envs=Envs.ALL.value)
    @allure.title("Test calendar date picker select more than 3 months")
    @allure.severity(allure.severity_level.CRITICAL)
    @pytest.mark.parametrize(
        'month_num, month_str, month_abbr, current_or_last',
        TestRebateDashboardArgs.ALL_MONTH_INFO['unclickable_months'][
            slice(TestRebateDashboardArgs.NUM_MONTH_DATA_FOR_CALENDAR)
        ]
    )
    def test_calendar_select_more_than_three_month(
            self,
            driver,
            test_account,
            month_num,
            month_str,
            month_abbr,
            current_or_last
        ):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.scroll_down_percent(
            TestRebateDashboardArgs.SCROLL_DOWN_PERCENT
        )

        now = datetime.now()
        last_month_day_kwargs = {
            'hour': now.hour,
            'minute': now.minute,
            'second': now.second
        }
        before_three_month_datetime = now - relativedelta(months=3)
        # Get clickable and unclickable info
        dashboard.click_calendar_btn('pick_month')
        if current_or_last == 'last':
            dashboard.calendar_year_manipulation_btn('preview')

        is_clickable, select_date = dashboard.select_month_by_date_picker(month_abbr)
        check.equal(is_clickable, False)
        year, month = select_date.split('-')
        click_date_time = HelperFuncs.get_last_month_day(
            int(year),
            int(month),
            **last_month_day_kwargs
        )
        check.greater(before_three_month_datetime, click_date_time)
        log.info(
            'Test select more than 3 month => '
            f'test_date: {month_str}, click_date_time: {click_date_time}, '
            f'before_three_month_datetime: {before_three_month_datetime}'
        )

    @execute_env(envs=Envs.ALL.value)
    @allure.title("Test calendar date picker select months")
    @pytest.mark.parametrize(
        'month_num, month_str, month_abbr, current_or_last',
        TestRebateDashboardArgs.ALL_MONTH_INFO['clickable_months'][
            slice(TestRebateDashboardArgs.NUM_MONTH_DATA_FOR_CALENDAR)
        ]
    )
    def test_calendar_date_picker_select_month(
            self,
            driver,
            test_account,
            month_num,
            month_str,
            month_abbr,
            current_or_last
        ):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.scroll_down_percent(
            TestRebateDashboardArgs.SCROLL_DOWN_PERCENT
        )

        dashboard.click_calendar_btn('pick_month')
        if current_or_last == 'last':
            dashboard.calendar_year_manipulation_btn('preview')

        is_clickable, _ = dashboard.select_month_by_date_picker(month_abbr)
        check.equal(is_clickable, True)
        pick_year_month = dashboard.get_calendar_pick_year_month()
        check.equal(month_str, pick_year_month)
        log.info(
            'Test date picker select year month => '
            f'test_date: {month_str}, displayed_year_month: {pick_year_month}'
        )

    @execute_env(envs=Envs.ALL.value)
    @allure.title('Test calendar date picker year operator')
    def test_calendar_date_picker_year_operator(self, driver, test_account):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.scroll_down_percent(
            TestRebateDashboardArgs.SCROLL_DOWN_PERCENT
        )
        dashboard.click_calendar_btn('pick_month')
        dashboard.calendar_year_manipulation_btn('next')
        is_clickable, select_date = dashboard.select_month_by_date_picker("May")
        time.sleep(1)
        expected_year = (datetime.today() + relativedelta(years=1)).strftime('%Y')
        expected_date = '{}-{}'.format(expected_year, '05')
        if is_clickable:
            current_value = dashboard.get_calendar_pick_year_month()
            assert expected_date == current_value
        else:
            assert expected_date == select_date

    @execute_env(envs=Envs.ALL.value)
    @allure.title("Test calendar event type display")
    def test_event_type_display(self, driver, test_account, expected_event_types):
        dashboard = RebateDashboard(driver)
        dashboard.login(test_account)
        dashboard.scroll_down_percent(
            TestRebateDashboardArgs.SCROLL_DOWN_PERCENT
        )
        event_type_map = dashboard.get_event_types(normalize_string=False)
        event_types = set(event_type_map.values())
        assert event_types == expected_event_types

    # --------- Below code hasn't been refactored ---------------- #

    # # Case 24 Select Some Weekly Event in Mar, 2022
    # @allure.title("Case 24 Select Some Weekly Event in Mar, 2022")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-283',
    #              name='[Dashboard][Calendar] Test scripts for Actives')
    # @allure.story("Test for the Select Some Weekly Event in Mar, 2022")
    # def test_for_the_select_some_event_in_mar(self, driver, test_account):
    #     dashboard = RebateDashboard(driver)
    #     dashboard.login(test_account)
    #     calendar_forward_button = dashboard.driver.find_element(
    #         By.XPATH, '//*[@id="__next"]//div[2]/button'
    #     )
    #     dashboard.driver.execute_script(
    #         "arguments[0].scrollIntoView();", calendar_forward_button
    #     )
    #     dashboard.open_the_date_picker_of_calendar()
    #     dashboard.set_up_time_for_calendar()
    #     dashboard.select_some_event_by_weekly()

    # # Case 25 View event
    # @allure.title("Case 25 View event")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-283',
    #              name='[Dashboard][Calendar] Test scripts for Actives')
    # @allure.story("Test for the View event in Mar 2022")
    # def test_for_view_some_event_in_mar(self, driver, test_account):
    #     dashboard = RebateDashboard(driver)
    #     dashboard.login(test_account)
    #     calendar_forward_button = dashboard.driver.find_element(
    #         By.XPATH, '//*[@id="__next"]//div[2]/button'
    #     )
    #     dashboard.driver.execute_script(
    #         "arguments[0].scrollIntoView();", calendar_forward_button
    #     )
    #     dashboard.open_the_date_picker_of_calendar()
    #     dashboard.set_up_time_for_calendar()
    #     dashboard.select_some_event_by_weekly()
    #     dashboard.click_the_view_event()
    #     element = driver.find_element_by_xpath(
    #         '//*[@id="__next"]//*/div/div/div/div[2]/div[1]/div/div/div[1]/button').is_enabled()
    #     check.equal(element, False)

    # # Case 26 Duplicate event
    # @allure.title("Case 26 Duplicate event")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-283',
    #              name='[Dashboard][Calendar] Test scripts for Actives')
    # @allure.story("Test for the Duplicate event in Mar 2022")
    # def test_for_duplicate_some_event_in_mar(self, driver, test_account):
    #     dashboard = RebateDashboard(driver)
    #     dashboard.login(test_account)
    #     calendar_forward_button = dashboard.driver.find_element(
    #         By.XPATH, '//*[@id="__next"]//div[2]/button'
    #     )
    #     dashboard.driver.execute_script(
    #         "arguments[0].scrollIntoView();", calendar_forward_button
    #     )
    #     dashboard.open_the_date_picker_of_calendar()
    #     dashboard.set_up_time_for_calendar()
    #     dashboard.select_some_event_by_weekly()
    #     # rebate_dashboard.click_the_duplicate_event()
    #     element = driver.find_element_by_xpath(
    #         '//*[@id="__next"]//*/div/div/div/div[2]/div[1]/div/div/div[3]/button').is_enabled()
    #     check.equal(element, False)

    # # Case 27 Close event pop-up
    # @allure.title("Case 27 Close event pop-up")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-283',
    #              name='[Dashboard][Calendar] Test scripts for Actives')
    # @allure.story("Test for the Close event in Mar 2022")
    # def test_for_close_some_event_in_mar(self, driver, test_account):
    #     dashboard = RebateDashboard(driver)
    #     dashboard.login(test_account)
    #     calendar_forward_button = dashboard.driver.find_element(
    #         By.XPATH, '//*[@id="__next"]//div[2]/button'
    #     )
    #     dashboard.driver.execute_script(
    #         "arguments[0].scrollIntoView();", calendar_forward_button
    #     )
    #     dashboard.open_the_date_picker_of_calendar()
    #     dashboard.set_up_time_for_calendar()
    #     dashboard.select_some_event_by_weekly()
    #     dashboard.click_the_close_event()
    #     element = driver.find_element_by_xpath(
    #         '//*[@id="__next"]//*/div/div/div/div[2]/div[1]/div/div/div[4]/button').is_enabled()
    #     check.equal(element, False)

    # # Case 28 Assert the page name == current page name
    # @allure.title("Case 28 Assert the page name == current page name")
    # @allure.severity(allure.severity_level.NORMAL)
    # @allure.link('https://hermes.devgambit.net/browse/PSS-246',
    #              name='[Dashboard][Chart] Test scripts for dashboard charts')
    # @allure.story("Test for visiting dashboard page")
    # def test_visit_dashboard_page(self, driver, test_account):
    #     dashboard = RebateDashboard(driver)
    #     dashboard.login(test_account)
    #     page_name = dashboard.get_current_page_name()
    #     assert page_name == "Dashboard"

    # @allure.story("Test for the Calendar Control")
    # def test_for_the_select_of_platform(self, driver, test_account):
    #     dashboard = RebateDashboard(driver)
    #     dashboard.login(test_account)
    #     calendar_forward_button = dashboard.driver.find_element(
    #         By.XPATH, '//*[@id="__next"]//div[2]/button'
    #     )
    #     dashboard.driver.execute_script(
    #         "arguments[0].scrollIntoView();", calendar_forward_button
    #     )
    #     time.sleep(2)
    #     dashboard.click_the_last_month_btn()
    #     dashboard.click_the_next_month_btn()
    #     calendar_time = dashboard.check_the_time_equal_current_time()
    #     today = date.today()
    #     current_time = today.strftime("%Y/%m")
    #     check.equal(calendar_time, current_time)
