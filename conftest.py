import os
import json
import time

import pluggy
import pytest
import allure
import traceback

from pathlib import Path
from datetime import datetime

from _pytest.runner import CallInfo
from _pytest.reports import TestReport
from pytest_harvest import is_main_process
from allure_commons.types import AttachmentType

from config_model import Config
from webdriver.driver import BrowserEngine
from utils.utils import SystemLogger
from utils.file_manager import BrowserLogManagement
from utils.report import ReportUtils, ReportGenerator

from selenium.common.exceptions import WebDriverException

log = SystemLogger.get_logger()


@pytest.fixture(scope='session', autouse=True)
def project_root() -> str:
    return os.path.dirname(os.path.abspath(__file__))


@pytest.fixture(scope='session', autouse=True)
def driver_config(project_root, request: pytest.FixtureRequest) -> Config:
    config_file_name = 'driver_config.json'
    try:
        config_file = os.path.join(project_root, 'config', config_file_name)
        with open(config_file, 'r') as rf:
            config_file = json.load(rf)
        config = Config(**config_file)

    except FileNotFoundError:
        config = Config()

    cli_remote_url = request.config.getoption('--remote_url')
    if cli_remote_url:
        config.driver.remote_url = cli_remote_url

    cli_browser = request.config.getoption('--browser')
    if cli_browser:
        config.driver.browser = cli_browser

    cli_screenshot_on = request.config.getoption('--screenshot_on')
    if cli_screenshot_on:
        config.log.screenshot_on = cli_screenshot_on

    if config.driver.remote_url:
        log_msg_prefix = "Using remote selenium.\n"
    else:
        log_msg_prefix = "Using local webdriver.\n"

    log.info(log_msg_prefix + f"The configuration of driver:\n {config.dict()}")
    return config


@pytest.fixture(scope='function')
def driver(driver_config: Config, request: pytest.FixtureRequest):
    """
    Get webdriver
    """
    browser_engine = BrowserEngine(driver_config)
    yield browser_engine.webdriver
    try:
        # TODO: This is work around method, need to modify it.
        if hasattr(request.node, 'report') and hasattr(request.node.report, 'failed'):
            if request.node.report.failed:
                if driver_config.log.screenshot_on:
                    # Get save dir and file_name
                    folder_path, file_name = ReportUtils.nodeid_to_save_info(request.node.nodeid)
                    Path(folder_path).mkdir(parents=True, exist_ok=True)
                    save_path = os.path.join(folder_path, file_name)
                    browser_engine.webdriver.save_screenshot(save_path)
                    log.info(f"Successfully save screenshot on {save_path}")
                    # Upload image to allure report
                    if request.driver_config.getoption('--alluredir'):
                        with open(save_path, 'rb') as rf:
                            img_data = rf.read()
                            allure.attach(
                                img_data,
                                name="failed_screenshot",
                                attachment_type=AttachmentType.PNG
                            )
                        log.info(f"Attach screenshot: {save_path} to allure report.")
    except AttributeError:
        log.error("Unable to access request.node.report.failed, unable to screenshot.")
    except WebDriverException as e:
        err_msg = traceback.format_exc()
        log.error(
            "WebDriver occurs unexpected errors.\n"
            f"err_msg: {err_msg}"
        )
    finally:
        # if driver_config.log.export_browser_log:
        #     log_type = driver_config.log.output_log_type
        #     logs = browser_engine.webdriver.get_log(log_type)
        #     BrowserLogManagement.export_log(log_type, logs)
        browser_engine.webdriver.quit()


@pytest.fixture(scope='function', autouse=True)
def display_current_version(driver_config: Config):
    if driver_config.log.version:
        log.info(f"Current version: {driver_config.log.version}")


def pytest_sessionstart(session: pytest.Session):
    pass


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item: pytest.Item, call: CallInfo) -> TestReport:
    """
    The hook function will be invoked three times.
    (1). Item invoke its setup function
    (2). After executing the item
    (3). Item invoke its teardown function

    Args:
        item (pytest.Item): _description_
        call (CallInfo): _description_

    Returns:
        _type_: _description_
    """
    outcome: pluggy._callers._Result = yield
    report: TestReport = outcome.get_result()
    if report.when == 'call':
        # Set the new attribute to item object
        setattr(item, 'report', report)
    return report


def pytest_addoption(parser: pytest.Parser):
    parser.addoption(
        '--browser',
        action='store',
        default='chrome',
        help='The lowercase browser name. Ex: chrome or firefox'
    )
    parser.addoption(
        '--remote_url',
        action='store',
        default='',
        help='Selenium Grid url to test connect test'
    )
    parser.addoption(
        '--screenshot_on',
        action='store',
        default=True,
        help='Open screenshot when test failed, True or False'
    )
    parser.addoption(
        '--allure-server-host',
        action='store',
        default='',
        type=str,
        help='The host of allure server'
    )
    parser.addoption(
        '--allure-server-port',
        action='store',
        default='',
        type=str,
        help='The port of allure server'
    )
    parser.addoption(
        '--invoke-setting-notification',
        action='store',
        default=False,
        type=bool,
        help='Whether invoking the teams notification'
    )


# Only Main process can upload file the others just can skip
def pytest_sessionfinish(session: pytest.Session, exitstatus: int):
    if not is_main_process(session):
        return
    # Start generating the report and send notification
    report_generator = ReportGenerator()
    report_generator.generate_reports(session)