import allure

from typing import List

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from bo_shared.bo_service_pages import BoService
from rebate_service.pages.rebate_login import RebateLogIn

class RebateHomePage(RebateLogIn):

    PAGE_NAME = BoService.Shared.HomePage

    def __init__(self, driver: WebDriver):
        super(RebateHomePage, self).__init__(driver)