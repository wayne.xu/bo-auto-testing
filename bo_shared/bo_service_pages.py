
class SharePage:

    ServiceName = None

    HomePage = 'home_page'
    Shared = "share"
    LogIn = "login"


class RebateService:

    ServiceName = 'RebateService'

    DashBoard = "dashboard"
    Management = "management"
    RebateLevel = "member_exclusion/member_rebate_level"
    RebateBlackList = "member_exclusion/rebate_blacklist"


class RoleService:

    ServiceName = "RoleService"

    UserManagement = 'user_management'
    RoleManagement = 'role_management'
    PolicyManagement = 'policy_management'


class ExcludedGameService:

    ServiceName = "excluded_game"


class BonusService:

    ServiceName = "BonusService"

    Management = "management"
    Report = "report"
    Finance = "report/finance"
    Player = "report/player"


class MemberService():

    ServiceName = "MemberService"

    CSD = 'csd'
    PlayerList = 'player_list'
    HousePlayerList = 'house_player_list'
    ExportMemberData = 'export_member_data'
    Uploader = 'uploader'


class BoService:

    Rebate = RebateService
    Role = RoleService
    Shared = SharePage
    ExcludedGame = ExcludedGameService
    Bonus = BonusService
    Member = MemberService