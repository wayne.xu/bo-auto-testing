import os
import re
import json
import uuid
import time
import string
import pickle
import zipfile
import logging
import hashlib
import calendar
import operator
import threading

from enum import Enum
from pathlib import Path
from zipfile import ZipFile
from threading import RLock
from functools import wraps
from datetime import datetime, date, timezone
from collections import OrderedDict
from logging.handlers import RotatingFileHandler
from concurrent.futures.thread import ThreadPoolExecutor
from concurrent.futures.process import ProcessPoolExecutor
from typing import (
    Any, Callable, Optional, List, Union,
    Set, TypeVar, Dict, Tuple, Iterable

)

import boto3
from click import FileError
from filelock import FileLock, Timeout

from utils.encryption import Encryption


_KT = TypeVar("_KT")
_VT = TypeVar("_VT")
_T = TypeVar("_T")
_S = TypeVar("_S")


class SystemLogger():

    CURRENT = None
    LOG_PATH = os.path.join(os.getcwd(),'logs')
    FMT = (
        "%(asctime)s [%(levelname)s] "
        "%(module)s:%(funcName)s "
        "(%(lineno)d) %(message)s"
    )
    FORMATTER = logging.Formatter(
        FMT,
        datefmt="%Y-%m-%d %H:%M:%S"
    )

    ROTATING_CONFIG = {
        'info': {
            'file_name': 'log_info.log',
            'maxBytes': 8*1024*1024,
            'backupCount': 32,
            'level': logging.DEBUG,
        },
        'warn': {
            'file_name': 'log_warn.log',
            'maxBytes': 8*1024*1024,
            'backupCount': 16,
            'level': logging.WARN,
        },
        'error': {
            'file_name': 'log_error.log',
            'maxBytes': 8*1024*1024,
            'backupCount': 16,
            'level': logging.ERROR,
        }
    }

    LOG_LEVEL_MAP = {
        'critical': logging.CRITICAL,
        'error': logging.ERROR,
        'warning': logging.WARNING,
        'info': logging.INFO,
        'debug': logging.DEBUG,
    }

    @classmethod
    def create_log_dir(cls):
        """
        Create a log directory if it doesn't exist
        """
        Path(cls.LOG_PATH).mkdir(parents=True, exist_ok=True)

    @classmethod
    def create_logger(cls, name: str, level: str = 'debug') -> logging.getLogger:
        cls.create_log_dir()
        logger = logging.getLogger(name)
        handler = logging.StreamHandler()
        handler.setFormatter(cls.FORMATTER)
        logger.addHandler(handler)
        level = level.lower()
        level = cls.LOG_LEVEL_MAP[level]
        logger.setLevel(level)

        # Setting each log level for RotatingFileHandle.
        for _, config in cls.ROTATING_CONFIG.items():
            log_target = os.path.join(cls.LOG_PATH, config['file_name'])
            handler = RotatingFileHandler(
                log_target, maxBytes=config['maxBytes'],
                backupCount=config['backupCount']
            )
            handler.setLevel(config['level'])
            handler.setFormatter(cls.FORMATTER)
            logger.addHandler(handler)
        return logger

    @classmethod
    def get_logger(cls) -> logging.Logger:
        """
        Ensuring the log instance only be created one time
        """
        if cls.CURRENT is None:
            cls.CURRENT = cls.create_logger(__name__)
        return cls.CURRENT


class SingletonMeta(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]


class ExecutorMode(str, Enum):

    THREAD = 'thread'
    PROCESS = 'process'


class PoolExecutor():

    _PROCESS_POOL = None
    _THREAD_POOL = None
    _THREAD_NAME_PREFIX = "WorkerThreadPool"
    MAX_PROCESS_WORKERS = int(os.cpu_count()/2)
    MAX_THREAD_WORKERS = int(os.cpu_count()*2)

    @classmethod
    def get_process_executor(cls) -> ProcessPoolExecutor:
        if not cls._PROCESS_POOL:
            cls._PROCESS_POOL = ProcessPoolExecutor(cls.MAX_PROCESS_WORKERS)
        return cls._PROCESS_POOL

    @classmethod
    def get_thread_executor(cls) -> ThreadPoolExecutor:
        if not cls._THREAD_POOL:
            cls._THREAD_POOL = ThreadPoolExecutor(
                max_workers=cls.MAX_THREAD_WORKERS,
                thread_name_prefix=cls._THREAD_NAME_PREFIX
            )
        return cls._THREAD_POOL

    @classmethod
    def submit_task(
        cls,
        executor_mode: str = ExecutorMode.PROCESS.value,
        wait_result: bool = False
    ) -> Callable:
        def decorator(func) -> Callable:
            @wraps(func)
            def wrapper_func(*args, **kwargs) -> Any:
                if executor_mode.lower() == ExecutorMode.THREAD.value:
                    executor = cls.get_thread_executor()
                elif executor_mode.lower() == ExecutorMode.PROCESS.value:
                    executor = cls.get_process_executor()
                else:
                    raise ValueError(
                        "Only support `process` and `thread` mode, "
                        f"not support `{executor_mode}`"
                    )
                result = None
                with executor:
                    _future = executor.submit(func, *args, **kwargs)
                if wait_result:
                    result = _future.result()
                return result
            return wrapper_func
        return decorator


class SynchronizedLock():

    _RLOCK = None
    log = SystemLogger.get_logger()

    @classmethod
    def get_rlock(cls):
        """
        Get the reentrant lock instance
        """
        if not cls._RLOCK:
            cls._RLOCK = RLock()
        return cls._RLOCK

    @classmethod
    def lock(cls, lock: Optional[RLock] = None) -> Callable:
        if lock is None:
            _lock = cls.get_rlock()
        def decorator(func: Callable) -> Callable:
            @wraps(func)
            def wrapper_func(*args, **kwargs) -> Any:
                with _lock:
                    result = func(*args, **kwargs)
                    cls.log.debug(
                        f"Execute func: {func.__name__} with args: {args}, and"
                        f"kwargs: {kwargs} at thread_id: {threading.get_ident()}"
                    )
                return result
            return wrapper_func
        return decorator


class HelperFuncs():
    """
    This class is used for offering some useful functions.
    """
    log = SystemLogger.get_logger()

    @staticmethod
    def list_cls_callable_names(cls: object) -> list:
        callable_names = []
        for attribute in dir(cls):
            if attribute.startswith('__'):
                continue

            attribute_value = getattr(cls, attribute)
            if not callable(attribute_value):
                continue

            callable_names.append(attribute)
        return callable_names

    @classmethod
    def hash_dict(cls, _dict: dict) -> str:
        if not isinstance(_dict, dict):
            raise TypeError(
                f"The type of _dict is {type(_dict)}, "
                f"it must be dict."
            )
        # Normalize the dictionary data type.
        trans_dict = {str(k): v for k, v in _dict.items()}
        sort_trans_dict = sorted(trans_dict.items(), key=lambda x: x[0])
        sort_trans_dict = OrderedDict(sort_trans_dict)
        sort_trans_dict = pickle.dumps(sort_trans_dict)
        hash_string = hashlib.md5(sort_trans_dict).hexdigest()
        return hash_string

    @staticmethod
    def file_ext_checker(file_path: str, expected_exts: Union[str, Set[str]]) -> bool:
        """
        Check the file extension whether it conform the expected extension

        Args:
            file_path (str)
            expected_ext (str)
        """
        _, ext = os.path.splitext(file_path)
        checked_result = True
        if isinstance(expected_exts, str):
            expected_exts = {expected_exts, }
        if ext not in expected_exts:
            checked_result = False
        return checked_result

    @staticmethod
    def add_months(sourcedate: datetime, months: int):
        month = sourcedate.month - 1 + months
        year = sourcedate.year + month // 12
        month = month % 12 + 1
        day = min(sourcedate.day, calendar.monthrange(year,month)[1])
        return datetime(year, month, day)

    @staticmethod
    def get_month_ts_range(year: int, month: int) -> tuple:
        """Get the month timestamp range

        Args:
            year (int)
            month (int)

        Returns:
            tuple: (The first day of the month at 12 AM,
            the last day of the month at 23:59:59.999999)
        """
        num_of_days = calendar.monthrange(year, month)[1]
        first_day_timestamp = datetime(
            year=year,
            month=month,
            day=1,
            hour=0,
            minute=0,
            second=0,
            microsecond=0
        ).timestamp()
        last_day_timestamp = datetime(
            year=year,
            month=month,
            day=num_of_days,
            hour=23,
            minute=59,
            second=59,
            microsecond=999999
        ).timestamp()
        return (first_day_timestamp, last_day_timestamp)

    @staticmethod
    def is_ts_in_year_month(
        year: int,
        month: int,
        check_timestamp :int
    ) -> bool :
        """Check timestamp is in given year month

        Args:
            year (int)
            month (int)
            check_timestamp (int)

        Returns:
            bool: whether the given timestamp is in the given month
        """
        start, end = HelperFuncs.get_month_ts_range(year, month)
        if start <= check_timestamp <= end:
            return True
        else:
            return False

    @staticmethod
    def get_utc_local_time_offset() -> float:
        offset = time.timezone if (time.localtime().tm_isdst == 0) else time.altzone
        return offset * -1

    @staticmethod
    def convert_milliseconds_ts(milliseconds_timestamp: int) -> float:
        """Convert milliseconds timestamp into Unix timestamp

        Args:
            milliseconds_timestamp (int)
        """
        milliseconds_timestamp = int(milliseconds_timestamp)
        timestamp = milliseconds_timestamp/1000
        return timestamp

    @staticmethod
    def remove_punctuation(_string: str) -> str:
        remove = string.punctuation
        remove = remove.replace('-', '')
        pattern = r'[{}]'.format(remove)
        return re.sub(pattern, '', _string)

    @staticmethod
    def get_calendar_offset_months(offset_months: int = 0) -> List[tuple]:
        """Get the all months which don't include the offset months.

        Args:
            offset (int, optional): Offset of the current month, Defaults to 0.
        """
        all_months = [
            (idx, str(idx).zfill(2), abbr)\
            for idx, abbr in enumerate(calendar.month_abbr[:]) if idx
        ]
        idx = 0
        if offset_months:
            idx = datetime.now().month - offset_months

        if idx == 0:
            offset_months = all_months
        elif idx < 0:
            offset_months = all_months[:idx - 1]
        else:
            offset_months = all_months[idx - 1:]
        return offset_months

    @staticmethod
    def get_calendar_clickable_unclickable_months(offset_num: int = 3) -> dict:
        """Get current, last year clickable and unclickable months
        """
        all_months = HelperFuncs.get_calendar_offset_months()
        offset_months = HelperFuncs.get_calendar_offset_months(offset_num)

        now = datetime.now()
        current_year = now.year
        current_month = now.month
        last_year = current_year - 1
        # There are two situations
        # 1. current_month - offset <= 0
        #  -> HelperFuncs.get_calendar_offset_months will return unclickable months
        # 2. current_month - offset > 0
        #  -> HelperFuncs.get_calendar_offset_months will return clickable months
        if (current_month - offset_num) <= 0:
            unclickable_months = [
                (month_num, f'{last_year}-{month_str}', abbr, 'last')\
                for month_num, month_str, abbr in offset_months
            ]
            last_year_clickable_months = set(all_months).difference(set(offset_months))
            last_year_clickable_months = [
                (month_num, f'{last_year}-{month_str}', abbr, 'last')\
                for month_num, month_str, abbr in last_year_clickable_months
            ]
            clickable_months = [
                (month_num, f'{current_year}-{month_str}', abbr, 'current')\
                for month_num, month_str, abbr in all_months
            ]
            clickable_months.extend(last_year_clickable_months)
        else:
            clickable_months = [
                (month_num, f'{current_year}-{month_str}', abbr, 'current')\
                for month_num, month_str, abbr in offset_months
            ]
            unclickable_months = set(all_months).difference(offset_months)
            unclickable_months = [
                (month_num, f'{current_year}-{month_str}', abbr, 'current')\
                for month_num, month_str, abbr in unclickable_months
            ]
            last_year_unclickable_months = [
                (month_num, f'{last_year}-{month_str}', abbr, 'last')\
                for month_num, month_str, abbr in all_months
            ]
            unclickable_months.extend(last_year_unclickable_months)

        clickable_months = sorted(clickable_months, key=operator.itemgetter(1, 3))
        unclickable_months = sorted(unclickable_months, key=operator.itemgetter(1, 3))
        return {
            'clickable_months': clickable_months,
            'unclickable_months': unclickable_months,
        }

    @staticmethod
    def get_last_month_day(year: int ,month: int, **kwargs) -> datetime:
        num_of_days = calendar.monthrange(year, month)[1]
        return datetime(year, month, num_of_days, **kwargs)

    @staticmethod
    def convert_ts_format(timestamp: int, format: str = '%Y-%m-%d %H:%M:%S') -> str:
        """Convert timestamp into specific format

        Args:
            timestamp (int)
            format (_type_, optional): Defaults to '%Y-%m-%d %H:%M:%S'.
        """
        return datetime.fromtimestamp(timestamp).strftime(format)

    @staticmethod
    def get_data_from_args(
        key: str,
        args: Optional[tuple] = None,
        kwargs: Optional[dict] = None
    ) -> Any:
        """Get the data from args or kwargs

        Args:
            key (str): The key that you want to get
            args (Optional[tuple], optional): Position arguments.Defaults to None.
            kwargs (Optional[dict], optional): Keyword arguments.Defaults to None.
        Note:
            The `args` and `kwargs` can't be `None` at the mean time.
        """
        if args is None and kwargs is None:
            raise ValueError("`args` and `kwargs` can't be None at the same time")
        data = None
        if args:
            for arg in args:
                if isinstance(arg, dict) and key in arg:
                    data = arg[key]
                    return data
        if kwargs:
            try:
                data = kwargs[key]
            except KeyError:
                raise KeyError(
                    f"The `{key}` doesn't exist in kwargs: {kwargs}"
                )
            return data


class S3Bucket():
    """
    Manipulate the AWS S3 bucket
    """
    ACCEPT_EMPTY_STRING_KEYS = {"bucket_name"}

    def __init__(self):
        self.log = SystemLogger.get_logger()
        self.aws_keys = self._get_aws_keys()
        self.s3_resource = boto3.resource(
            service_name='s3',
            aws_access_key_id=self.aws_keys['aws_access_key_id'],
            aws_secret_access_key=self.aws_keys['aws_secret_access_key'],
            region_name=self.aws_keys['region_name']
        )
        if 'bucket_name' not in self.aws_keys \
            or isinstance(self.aws_keys, list):
            self.bucket_name = ''
            self.log.info(
                "You need to input the argument `bucket_name`, "
                "when executing `upload_file`"
            )
        else:
            self.bucket_name = self.aws_keys['bucket_name']

    def _get_aws_keys(self) -> dict:
        """
        Load AWS key from environment.
        """
        aws_key = {
            "bucket_name": os.environ.get('BUCKET_NAME', ""),
            "aws_access_key_id": os.environ.get("AWS_ACCESS_KEY_ID"),
            "aws_secret_access_key": os.environ.get("AWS_SECRET_ACCESS_KEY"),
            "region_name": os.environ.get("AWS_REGION_NAME"),
        }
        self.aws_keys_validator(aws_key)
        return aws_key

    def aws_keys_validator(self, aws_key: dict) -> bool:
        for k, v in aws_key.items():
            if k in self.ACCEPT_EMPTY_STRING_KEYS:
                continue
            if not v:
                raise ValueError(f"{k} can't be empty string or None.")

    @staticmethod
    def generate_object_url(
        bucket_name: str,
        region_name: str,
        object_name: str
    ) -> str:
        """
        Generate object url by bucket_name, region_name, and object_name.
        """
        url_format = (
            'https://{bucket_name}.s3.{region_name}.'
            'amazonaws.com/{object_name}'
        )
        url = url_format.format(
            bucket_name=bucket_name,
            region_name=region_name,
            object_name=object_name
        )
        return url

    def upload_file(
        self,
        file_name: str,
        bucket_name: Optional[str] = None,
        object_name=None
    ) -> str:
        """
        Upload file to S3 bucket

        Args:
            file_name: file that you upload to S3
            bucket_name: if it is None, it will use the bucket from configuration
            object_name: if it is None, it will use file_name(exclude directory) as object_name
        """
        if bucket_name is None:
            if self.bucket_name:
                bucket_name = self.bucket_name
            else:
                raise ValueError(
                    "Need to specific the bucket name. "
                    f"config: {self.aws_keys}"
                )
        try:
            bucket = self.s3_resource.Bucket(bucket_name)
        except Exception as e:
            self.log.error(f"Invalid bucket name: {bucket_name}")
            raise e

        if object_name is None:
            object_name = os.path.basename(file_name)
        try:
            if not os.path.exists(file_name):
                raise FileExistsError(
                    f"The file doesn't exists, the upload file: {file_name}"
                )
            bucket.upload_file(
                Filename=file_name,
                Key=object_name
            )
        except Exception as e:
            self.log.error(e)
            raise e

        object_url = self.generate_object_url(
            bucket_name,
            self.aws_keys['region_name'],
            object_name
        )
        self.log.info((
            f"Successfully upload file: {file_name} to s3 bucket, "
            f"bucket_name: {bucket_name}, object_url: {object_url}"
        ))
        return object_url


class ZipTools():

    log = SystemLogger.get_logger()
    # WORK_DIR = Path(os.path.dirname(os.path.abspath(__file__))).parent
    WORK_DIR = os.getcwd()
    ALLOW_EXT = {'.zip'}

    @classmethod
    def check_file_ext(cls, file_path: Path) -> bool:
        return HelperFuncs.file_ext_checker(file_path, cls.ALLOW_EXT)

    @classmethod
    def get_uuid_filename(cls) -> str:
        current_time = datetime.now().strftime("%Y%m%d_%H%M%S")
        _uuid = str(uuid.uuid4()).replace('-', '')
        file_name = f"{_uuid}_{current_time}"
        return file_name

    @classmethod
    def unzip_files(
        cls,
        unzip_file: str,
        extract_path: Optional[str] = None,
        members: Union[str, List[str], None] = None,
        pwd: bytes = None
    ) -> bool:
        unzip_result = True
        if not zipfile.is_zipfile(unzip_file):
            raise FileError(f"The {unzip_file} can't be unziped.")
        try:
            with ZipFile(unzip_file, 'r') as zip:
                zip.printdir()
                zip.extractall(
                    path=extract_path,
                    members=members,
                    pwd=pwd
                )
        except Exception as e:
            cls.log.error(e)
            raise e
        return unzip_result

    @classmethod
    @SynchronizedLock.lock()
    def zip_directory(
        cls,
        zip_dir: str,
        zip_output: Optional[str] = None,
        exclude_files: Union[List[str], None] = None,
    ) -> Optional[Path]:
        """
        Compress the directory. If argument `zip_output` is None, the output file will be
        generated on the current python `os.getcwd()` and use `zip_dir` as file name.

        Args:
            zip_dir: The path of directory
            zip_output: The output path after compression, if it is None, it will use zip_dir as name
            exclude_files: List of the files that you don't want to compress

        Returns:
            Optional[Path]: If compression is successful, it will return the output path. Or it will return `None`
        """
        # TODO:
        # exclude_files_pat: Union[List[str], None] = None,
        # Use unix like file pattern to exclude files
        if not os.path.isdir(zip_dir):
            raise FileError(f"The directory: {zip_dir} doesn't exists.")
        _path = Path(zip_dir)
        # Change working directory to avoid zip structure doesn't conform our expected
        os.chdir(_path.parent)
        # The returned value of Path('./').name is None, it need to adjust it.
        walk_dir = _path.name or './'

        # Go through the whole directory and filter exclude files
        file_paths = []
        for root, directories, files in os.walk(walk_dir):
            for filename in files:
                # if filename in exclude_files_pat:
                #     continue
                file_path = os.path.join(root, filename)
                if exclude_files and file_path in exclude_files:
                    continue
                else:
                    file_paths.append(file_path)
        if file_paths:
            # If zip_output is None, it will use zip_dir as output file name.
            if zip_output is None:
                if walk_dir == './':
                    # If it doesn't specific the directory,
                    # it will generate random_uuid_current_time as file_name
                    file_name = cls.get_uuid_filename()
                    file_name = f'{file_name}.zip'
                    zip_output = os.path.join(cls.WORK_DIR, file_name)
                    cls.log.debug((
                        "It doesn't specific the output file path, so"
                        "it use {random_uuid}_{current_time} format as file name."
                    ))
                else:
                    zip_output = os.path.join(cls.WORK_DIR, f'{walk_dir}.zip')

            if not cls.check_file_ext(zip_output):
                raise FileError(f"The file extension of `zip_output` must be in {cls.ALLOW_EXT}")
            # Compress the files into zip_output
            try:
                cls.log.debug(f"Compress file paths: {file_paths}")
                with ZipFile(zip_output, 'w') as zip:
                    for file_path in file_paths:
                        zip.write(file_path)
                cls.log.debug(f"Successfully compress {len(file_paths)} files into {zip_output}")
            except Exception as e:
                cls.log.debug(e)
                raise e
        else:
            zip_output = None
            cls.log.debug(
                "There is an empty file_paths can be compressed. "
                f"exclude_files: {exclude_files}"
                # f"exclude_files_pat: {exclude_files_pat}"
            )
        # Change work directory back to root path
        os.chdir(cls.WORK_DIR)
        return zip_output

    @classmethod
    def zip_specific_files(
        cls,
        output_path: str,
        *files
    ) -> bool:
        """
        Compress the specific files

        Args:
            output_path: output path
            *files: Files that you compress
        """
        zip_result = True
        try:
            with ZipFile(output_path, 'w', zipfile.ZIP_DEFLATED) as zip:
                for file in files:
                    zip.write(file)
                    zip.printdir()
            all_files = ', '.join(files)
            cls.log.info(f"Successfully compress {all_files} into {output_path}")
        except Exception as e:
            cls.log.error(e)
            raise e
        return zip_result


class TestDataLoader():
    """
    Load test data

    Args:
        file_path: the file path is loaded as test data.
            The default file path is ${WORK_DIR}/test-data/test_data.json
    """
    DIR = os.path.join(os.getcwd(), 'test-data')
    FILES_DIR = os.path.join(DIR, 'files')
    ALLOW_EXTS = {'.json'}

    DECRYPT_DATA_KEY = {
        'test_login_account',
        'test_player_membercode',
        'test_bo_account',
    }

    def __init__(self, file_path: Optional[str] = None):
        self.encryption = Encryption()
        if file_path:
            self.file_path = file_path
        else:
            self.file_path = os.path.join(self.DIR, 'test_data.json')
        self.file_ext = None
        # Check file extension
        self.file_ext_checker(self.file_path)
        # Read test_data
        with open(self.file_path, 'r') as rf:
            self.test_data = json.load(rf, object_hook=dict)

    def file_ext_checker(self, file_path: str) -> bool:
        _, self.file_ext = os.path.splitext(file_path)
        if self.file_ext not in self.ALLOW_EXTS:
            raise TypeError(
                f"The data must confirm to {self.ALLOW_EXTS} one of formats"
            )
        else:
            return True

    def get_file_path(self, service_name: str, file_name: str) -> str:
        file_path = os.path.join(self.FILES_DIR, service_name, file_name)
        if not os.path.exists(file_path):
            raise FileExistsError(f"The {file_path} doesn't exist.")
        return file_path

    def __getitem__(self, key: str) -> Any:
        if key in self.DECRYPT_DATA_KEY:
            data = self.test_data[key]
            decrypted_data = self.encryption.decrypt_data(data)
            decrypted_data = self.encryption.str_to_dict(decrypted_data)
            return decrypted_data
        else:
            return self.test_data[key]

    def __repr__(self) -> str:
        return f"{self.test_data}"


class EnumMixIn():

    @classmethod
    def to_dict(cls, include_default=False) -> dict:
        """Convert Enum to dict
        """
        member_names: list = getattr(cls, '_member_names_')
        if include_default == False:
            member_names.remove('_DEFAULT')
        _dict = dict()
        for member_name in member_names:
            attr = getattr(cls, member_name)
            _dict[member_name] = attr.value
        return _dict

class ThreadSafeDict(dict):

    @SynchronizedLock.lock()
    def __getitem__(self, __key: _KT) -> _VT:
        return super().__getitem__(__key)

    @SynchronizedLock.lock()
    def setdefault(self, __key: _KT, __default: _VT):
        return super().setdefault(__key, __default)

    @SynchronizedLock.lock()
    def copy(self) -> Dict[_KT, _VT]:
        return super().copy()

    @SynchronizedLock.lock()
    def popitem(self) -> Tuple[_KT, _VT]:
        return super().popitem()

    @SynchronizedLock.lock()
    def pop(self, __key: _KT) -> _VT:
        return super().pop(__key)

    @SynchronizedLock.lock()
    def update(self):
        return super().update()

    @SynchronizedLock.lock()
    def clear(self) -> None:
        self.fromkeys
        return super().clear()

    @classmethod
    @SynchronizedLock.lock()
    def fromkeys(cls, __iterable: Iterable[_T], __value: _S):
        return cls.fromkeys(__iterable, __value)