from enum import Enum


class Brands(Enum):

    P1M1 = 'p1m1'
    P1M2 = 'p1m2'


class Envs(Enum):

    ALL = 'all'
    DEV = 'dev'
    STAGE = 'stage'
    SOFT = 'soft'
    PROD = 'production'