from contextlib import contextmanager

from webdriver import driver_factory
from config_model import Config
from utils.utils import SystemLogger

from selenium.common.exceptions import WebDriverException


class BrowserEngine():
    """
    Initial the webdriver, and its setting depend on configuration.

    - When invoking the driver in conftest.py

        The priority of configuration:
            $(project_root)/config/driver_config.json -> config_model.py

    - Directly invoke this object, it will use the default values of config_model.py as configuration.
    """
    def __init__(self, config: Config):
        self.config = config
        self.log = SystemLogger.get_logger()
        self._webdriver = None
        self.log.info(f'Initializing {self.__class__.__name__} is finished.')

    def init_webdriver(self):
        self._webdriver = driver_factory.build_from_config(self.config)
        self._webdriver.implicitly_wait(self.config.driver.wait_time)
        caps = self._webdriver.capabilities
        try:
            self.log.info(
                f"Capabilities: "
                f'browserName: {caps["browserName"]}, '
                f'browserVersion: {caps["browserVersion"]}, '
                f'platformName: {caps["platformName"]}, '
                f'session_id: {self._webdriver.session_id}'
            )
        except Exception as e:
            self.log.warning(
                "webdriver.capabilities did not have a  "
                "key that BrowserEngine was expecting. "
                "Is your driver executable the right version?"
            )
            raise e
        # Initial Browser Setup
        if self.config.driver.page_load_wait_time:
            self.set_page_load_timeout(self.config.driver.page_load_wait_time)

        if self.config.viewport.maximize:
            self.maximize_window()
        else:
            self.viewport(
                self.config.viewport.width,
                self.config.viewport.height,
                self.config.viewport.orientation
            )
        self.log.debug(
            f"Setting the page_time_out: {self.config.driver.page_load_wait_time} secs "
            f"and implicitly_wait: {self.config.driver.wait_time} secs"
        )
        return self._webdriver

    @property
    def webdriver(self):
        if self._webdriver is None:
            driver = self.init_webdriver()
        else:
            driver = self._webdriver
        return driver

    @property
    def window_handle(self):
        return self.webdriver.window_handles

    @property
    def window_size(self):
        return self.webdriver.get_window_size()

    def set_page_load_timeout(self, timeout: int):
        self.webdriver.set_page_load_timeout(timeout)

    def maximize_window(self):
        try:
            self.webdriver.maximize_window()
        except WebDriverException as e:
            self.log.error(f"Can't maximize window: {e.msg}")

    def viewport(self, width: int, height: int, orientation: str = "portrait"):
        """
        Control the size and orientation of the current context's browser window.
        Args:
            width: The width in pixels
            height: The height in pixels
            orientation: default is 'portrait'. Pass 'landscape' to reverse the width/height.
        Examples:
            py.viewport(1280, 800) # macbook-13 size
            py.viewport(1440, 900) # macbook-15 size
            py.viewport(375, 667)  # iPhone X size
        """
        self.log.info(
            f"[STEP] py.viewport() - Viewport set to width={width}, "
            f"height={height}, orientation={orientation}"
        )
        if orientation == "portrait":
            self.webdriver.set_window_size(width, height)
        elif orientation == "landscape":
            self.webdriver.set_window_size(height, width)
        else:
            raise ValueError("Orientation must be `portrait` or `landscape`.")

    @contextmanager
    def auto_webdriver(self):
        try:
            yield self.webdriver
        except Exception as e:
            caps = self.webdriver.capabilities
            self.log.error(
                f"Capabilities: "
                f'browserName: {caps["browserName"]}, '
                f'browserVersion: {caps["browserVersion"]}, '
                f'platformName: {caps["platformName"]}, '
                f'session_id: {self._webdriver.session_id} '
                f'occur unexpected error.'
            )
            raise e
        finally:
            self.webdriver.quit()
