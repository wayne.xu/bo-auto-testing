#!/bin/bash

set -e

# Get arguments from command line
# Args:
#   [-r] remove_container_or_not: whether removing the container after all test casese are done
#   [-e] excute_test_cli: The command line for invoking the pytest.

EXECUTE_TEST_CLI=""
REMOVE_OR_NOT=true
while getopts "r:e:" flag
do
    case "${flag}" in
        r) REMOVE_OR_NOT=${OPTARG};;
        e) EXECUTE_TEST_CLI=${OPTARG};;
    esac
done

# If EXECUTE_TEST_CLI is empty, it will assign ${DEFAULT_EXECUTE_TEST_CLI} to ${EXECUTE_TEST_CLI}
DEFAULT_EXECUTE_TEST_CLI="python3 main.py ./test_setup/ -p True -n 3 -notification True"
if [ -z "${EXECUTE_TEST_CLI}" ];then
    EXECUTE_TEST_CLI="${DEFAULT_EXECUTE_TEST_CLI}"
fi

# The flag is used for determinating whether removing the container, after all test case are done
DK_REMOVE_FLAG=""
if [ ${REMOVE_OR_NOT} == true ];then
    DK_REMOVE_FLAG='--rm'
fi

echo "==========================================="
echo "| Start executing test cases in container |"
echo "==========================================="
echo "Argsuments:"
echo "   [-r] remove_container_or_not: ${REMOVE_OR_NOT}"
echo "   [-e] excute_test_cli:"
echo "           ${EXECUTE_TEST_CLI}"
echo ""

# Docker information
PWD=$( pwd )
IMG_VERSION="1.0"
IMAGE_NAME="bo-auto-testing"
DOCKER_FILE_PATH="${PWD}/Dockerfile"
ENV_PATH="${PWD}/config/.env"

# HOST volume info
HOST_CONFIG_DIR="${PWD}/config/"
HOST_TEST_DATA_DIR="${PWD}/test-data/"

# Container volume info
CONTAINER_WORK_DIR="/opt/bo-auto-testing"
CONTAINER_CONFIG_DIR="${CONTAINER_WORK_DIR}/config/"
CONTAINER_TEST_DATA_DIR="${CONTAINER_WORK_DIR}/test-data/"

DK_RUN_CLI=(
    "docker run --name ${IMAGE_NAME}"
    "--env-file ${ENV_PATH}"
    "-v ${HOST_CONFIG_DIR}:${CONTAINER_CONFIG_DIR}"
    "-v ${HOST_TEST_DATA_DIR}:${CONTAINER_TEST_DATA_DIR}"
    "${DK_REMOVE_FLAG} -it -d ${IMAGE_NAME}:${IMG_VERSION} ${EXECUTE_TEST_CLI}"
)
DK_RUN_CLI=$( printf "%s " "${DK_RUN_CLI[@]}" )

# Check image build or not
BUILD_OR_NOT=$(
    docker images --filter=reference="${IMAGE_NAME}:${IMG_VERSION}"\
        --format "{{.Repository}}" | wc -l
)

if [ ${BUILD_OR_NOT} -eq 0 ];then
    # Build docker image
    echo "The ${IMAGE_NAME}:${IMG_VERSION} image doesn't exist, so start executing docker build and docker run...."
    docker build --file ${DOCKER_FILE_PATH} --tag ${IMAGE_NAME}:${IMG_VERSION} --no-cache .
    exec ${DK_RUN_CLI}
fi

# Check container exists or not
CONTAINER_EXIST_OR_NOT=$(
    docker ps -a --filter "ancestor=${IMAGE_NAME}:${IMG_VERSION}"\
        --format "{{.Names}}" | wc -l
)
if [ ${CONTAINER_EXIST_OR_NOT} -ne 0 ];then
    _log_msg=(
        "The ${IMAGE_NAME} container exist, then remove it "
        "and using docker run to create a new one to "
        "avoid running not relevant test cases."
    )
    echo $( printf "%s" "${_log_msg[@]}" )
    docker container stop ${IMAGE_NAME}
    docker container rm ${IMAGE_NAME}
fi

# Execute docker run
exec ${DK_RUN_CLI}

# stdout and stderr
if [ ${?} -eq 0 ];then
    echo "Successfully finish all testing"
    echo "Execute docker cli:"
    echo "      ${DK_RUN_CLI}"
else
    >&2 echo "Failed: The shell exit status is ${?}, maybe occur some unexpected errors"
    >&2 echo "Execute docker cli:"
    >&2 echo "      ${DK_RUN_CLI}"
fi