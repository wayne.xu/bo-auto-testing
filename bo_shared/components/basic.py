from utils.base_page import BasePage
from bo_shared.bo_router import BoPageRouter
from bo_shared.bo_service_pages import BoService

from selenium.webdriver.remote.webdriver import WebDriver


class BasicComponent(BasePage):

    PAGE_NAME = BoService.Shared.Shared
    IS_COMPONENT = True

    def __init__(self, driver: WebDriver):
        super(BasicComponent, self).__init__(driver, BoPageRouter)