#!/bin/bash

set -e

touch ${BO_AUTO_TESTING_HOME}/healthy

./main.py $@ --parallel True -notification True

exit 0
