# Bo Automation Testing

## System Architecture

<img src="./images/auto-testing-architecture.drawio.png" width="600" height="550" alt="system-architecture" display="block" margin-left="auto" margin-right="auto">

## Project Description

- There are two configurations in this project.

  1. [`${pwd}/config/.env`](./config/): It  is used for environment setting, like `ALLURE_SERVER_HOST`, `AWS_ACCESS_KEY`,  `TEAMS_WEBHOOK_URL`, etc. 

  2. [`${pwd}/config/driver.config`](./config/): It is used for setting web driver. **Note that  the `remote_url` must be set when this project running in the container. The `remote_url` is the endpoint of selenium hub.(following the above system architecture)**

- [`${pwd}/test-data`](./test-data/): It used for placing the test data. Each test suit has its own directory.

- [`${pwd}/webdriver`](./webdriver/): Is the main module of the web driver.

- [`${pwd}/utils`](./utils/): It provides useful classes and functions for the project used. Including `url_router`, `allure_report`, etc.

- [`${pwd}/report-templates`](./report-templates/): It provides the report template for report module used.

- [`${pwd}/drivers`](./drivers/): It provides the web driver for local development. It has to download manually, and it needs to be noticed its version whether conforming to the version of the browser.

- [`${pwd}/test_setup`](./test_setup/): It provides some test cases for testing whether the enviroment is correcct or not.

- [`${pwd}/rebate_service`](./rebate_service/): The test cases related to rebate service.

- The rest of direcories are generated automatically. Most of them are used for saving the report, or some static files.

## How to Execute the Project?

### There are two ways to execute the project. 

- **Execute on Local**
  - You can directly use `pytest` command line to execute. But before executing the the project, you need to load environment variables first.

```bash
# Set environment variables
source load-envs.sh

########################################
# Execute test cases of the test_setup #
#######################################
# Method 1:
# Directly use `pytest` command line 
pytest --verbose ./test_setup\
	--alluredir=./allure-report --clean-alluredir\
	--html=./html-report/test_result.html --self-contained-html\
	--invoke-setting-notification True\
	--dist loadgroup -n 3\
	--max-worker-restart 2\
	--setup-show

# Method 2:
# Run man.py, which is wrapped common used arguments
python3 main.py ./test_setup/ -p True -n 3 -notification True
```

- **Execute in Container**
  - Even if you haven't had the `bo-auto-testing` image, executing [`run-test.sh`](./run-test.sh) will automatically build image. After building image, the container will start executing the test cases which you specify. Finally, when the test cases are done, the container will be automatically removed (default setting).
  - **Note: There is also a flag `-r`, which can control remove container or not**
  - The `-e` flag is used for invoke pytest in the container, so of cause it can accept any pytest command here. Also, it can also accept  [`python main.py`](./main.py) command, like as **Execute on Local** method2.

```bash
#################################
# Execute in container examples #
#################################

# Don't remove container after all test cases are done
# Note: The default -e is "python3 main.py ./test_setup/ -p True -n 3 -notification True"
bash run-test.sh -r false

# Also can use this way to execute test cases
bash run-test.sh -e "pytest --verbose ./test_setup 
		--alluredir=./allure-report --clean-alluredir
		--html=./html-report/test_result.html --self-contained-html
		--invoke-setting-notification True
		--dist loadgroup -n 3
		--max-worker-restart 2
"
# Prefer using this way to execute, it's more simple.
# Don't remove container, and run rebate service all test cases
bash run-test.sh -r false -e "python3 main.py ./rebate-service -p True -n 3 -notification True"
```

## Reference:

 - [Pytest](https://github.com/pytest-dev/pytest)
 - [Python testing with Pytest](https://github.com/jashburn8020/python-testing-with-pytest)
 - [Allure Server](https://github.com/kochetkov-ma/allure-server)
 - [Doker Selenium](https://github.com/SeleniumHQ/docker-selenium)
  









