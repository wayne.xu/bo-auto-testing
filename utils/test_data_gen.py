from datetime import datetime, timedelta
from typing import Tuple


class TestDataGenerator():

    @staticmethod
    def current_date_delta(
        delta_days: float = 0.0,
        date_format: str = "%Y-%m-%d"
    ) -> str:
        date_ = datetime.now() + timedelta(days=delta_days)
        return date_.strftime(date_format)

    @staticmethod
    def generate_range_date(
        delta_days: float,
        range_days: float,
        ensure_same_month: bool = True,
        date_format: str = "%Y-%m-%d"
    ) -> Tuple[str]:
        """
        Generate the range of dates

        Args:
            delta_days (float): It's used for adjusting the current date.
            range_days (float): The range between the start date and end date.
            ensure_same_month (bool, optional): It ensure the start date and end date on the same month. Defaults to True.
            date_format (str, optional): Defaults to "%Y-%m-%d".
        """
        if ensure_same_month:
            threshold_days = 29
            if range_days > threshold_days:
                raise ValueError(
                    "argument `ensure_same_month` is True, "
                    f"the `range_days` can't large than {threshold_days}."
                )
        adjust_day = 0
        while True:
            delta_date = datetime.now() + timedelta(days=(delta_days + adjust_day))
            end_date = delta_date + timedelta(days=range_days)
            if ensure_same_month:
                if delta_date.month == end_date.month:
                    break
                else:
                    adjust_day += 1
            else:
                break
        delta_date = delta_date.strftime(date_format)
        end_date = end_date.strftime(date_format)
        return (delta_date, end_date)
