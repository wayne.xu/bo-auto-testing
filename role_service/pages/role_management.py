import time
import allure
import random
import string

from bo_shared.bo_service_pages import BoService
from typing import List
from role_service.pages.shared_page import RoleSharedPage
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from rebate_service.pages.rebate_login import RebateLogIn
from selenium.webdriver.common.keys import Keys


class RoleManagement(RoleSharedPage):

    PAGE_NAME = BoService.Role.RoleManagement

    def __init__(self, driver: WebDriver):
        super(RoleManagement, self).__init__(driver)

    # Add Team Role
    @allure.step('Add Team Role')
    def add_team_role(self):
        click_element = self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH, '//a[@data-testid="actionAddTeamRole"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Get current Breadcrumb Link lv4
    @allure.step("Get current Breadcrumbs")
    def get_current_breadcrumb_lv4(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//nav[@data-testid="breadcrumb"]//li[4]'
        ).text
        return element

    # Get current Breadcrumb Link lv5
    @allure.step("Get current Breadcrumbs")
    def get_current_breadcrumb_lv5(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//nav[@data-testid="breadcrumb"]//li[5]'
        ).text
        return element

    # Input Role Name
    @allure.step("Input Role Name")
    def input_role_name(self, new_role_name: str):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="name"]'
        ).send_keys(new_role_name)

    # Input Random Role Name
    @allure.step("Input Random Role Name")
    def input_random_role_name(self):
        random_role_name = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="name"]'
        )
        random_str = ''.join(random.sample(string.ascii_letters + string.digits, 25))
        random_role_name.send_keys(random_str)

    # Filter Role by Name
    @allure.step("Filter Role by Name")
    def search_role_by_name(self, modify_role_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="roleNameList"]'
        )
        element.send_keys(modify_role_name)
        element.send_keys(Keys.ENTER)

    # Click Search Button
    @allure.step("Click Search Button")
    def click_search_btn(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionSearch"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Modify Role Name
    @allure.step("Modify Role Name")
    def modify_role_name(self, modify_role_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="name"]')
        element.send_keys(Keys.COMMAND, "a")
        element.send_keys(Keys.BACKSPACE)
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="name"]'
        ).send_keys(modify_role_name)

    # Select Parent Team
    @allure.step("Select Parent Team")
    def select_parent_team_for_create(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="parentName"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@data-testid="ADMIN"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select Parent Team
    @allure.step("Select Parent Team")
    def select_parent_team(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//span[@class="ant-select-selection-item"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@data-testid="ADMIN"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Modify Parent Team
    @allure.step("Modify Parent Team")
    def modify_parent_team(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//span[@class="ant-select-selection-item"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@data-testid="ADMIN"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Get Parent Team List
    @allure.step("Get Parent Team List")
    def get_parent_team_list(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//div[@class='rc-virtual-list']"
        ).text
        # element_split = element.split("\n")
        return element

    # Deactivate Role
    @allure.step("Deactivate Role")
    def select_deactivate(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@data-testid="deactivate"]'
        ).click()

    # Confirm Deactivate Role
    @allure.step("Confirm Deactivate Role")
    def select_confirm_deactivate(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-content"]//button[2]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Activate Role
    @allure.step("Activate Role")
    def select_activate(self):
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@data-testid="activate"]'
        ).click()

    # Select ALL Role's Policies
    @allure.step("Select ALL Role's Policies")
    def select_all_role_policies(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "//input[@type='checkbox' and @class='ant-checkbox-input']"
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Create Team role
    @allure.step("Create Team role")
    def create_team_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionCreate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Update Team role
    @allure.step("Update Team role")
    def update_team_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionUpdate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Confirm Create Team role
    @allure.step("Confirm Create Team role")
    def confirm_create_team_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-content"]//button[2]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Cancel Create Team Role
    @allure.step("Cancel Create Team Role")
    def cancel_create_team_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-content"]//button[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Get the 1st Role name by List
    @allure.step("Get the 1st Role name by List")
    def get_1st_role_name(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-table-content"]//tr[1]/td[1]'
        ).text
        return element

    # Get the 1st Role Parent Team by List
    @allure.step("Get the 1st Role Parent Team by List")
    def get_1st_parent_team_list(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-table-content"]//tr[1]/td[3]'
        ).text
        return element

    # Click the Detail button for 1st Role
    @allure.step("Click the Detail button for 1st Role")
    def click_the_1st_role_detail(self):
        click_element = self.find_element_wait(
            EC.element_to_be_clickable,
            By.XPATH,
            '(//*[@data-testid="actionViewDetail"])[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Click the Un/assign Users button for Role
    @allure.step("Click the Un/assign Users button for Role")
    def click_the_un_assign_by_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@class="ant-btn ant-btn-default ant-dropdown-trigger"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select Manually UN/Assign Users
    @allure.step("Select Manually UN/Assign Users")
    def manually_un_assign_by_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@role="menuitem" and contains(@data-menu-id,"manually")]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select Batch Assign Users
    @allure.step("Select Batch Assign Users")
    def batch_assign_by_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@role="menuitem" and contains(@data-menu-id,"paste")]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Input Email to Batch assign users
    @allure.step("Input Email to Batch assign users")
    def input_email_to_batch_assign(self, user_1_email: str, user_2_email: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@role="dialog"]//div[1]/input')
        element.send_keys(user_1_email)
        element.send_keys(Keys.ENTER)
        element1 = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@role="dialog"]//div[2]/input')
        element1.send_keys(user_2_email)
        element1.send_keys(Keys.ENTER)

    # Submit Batch Assign
    @allure.step("Submit Batch Assign")
    def submit_batch_assign(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-modal-content"]//button[2]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Editing Role
    @allure.step("Editing Role")
    def edit_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionEditRole"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Confirm Editing Role
    @allure.step("Confirm Editing Role")
    def confirm_edit_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '(//button[@type="button" and @class="ant-btn ant-btn-primary"])[2]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Cancel Editing Role
    @allure.step("Cancel Editing Role")
    def cancel_edit_role(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '(//button[@type="button" and @class="ant-btn ant-btn-default"])[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select the 1st Assign Users by checkBox
    @allure.step("Select the 1st Assign Users by checkBox")
    def select_the_checkbox_1st_assign_user(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@data-testid="containerAllExistingUsers"]//li[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select the 1st Un-assign Users by checkBox
    @allure.step("Select the 1st Un-assign Users by checkBox")
    def select_the_checkbox_1st_un_assign_user(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@data-testid="containerAssignedUsers"]//li[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Select all Users by un-assign table
    @allure.step("Select all Users by un-assign table")
    def select_all_users_to_un_assign(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@data-testid="containerAssignedUsers"]//label/span[1]/input'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Assign User
    @allure.step("Assign User")
    def assign_user(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionAssignUsers"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Un-assign User
    @allure.step("Un-assign User")
    def un_assign_user(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionUnassignUsers"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Apply Assign User
    @allure.step("Apply assign User")
    def apply_assign_user(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//button[@data-testid="actionApply"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Input Username to Assign
    @allure.step("Input Username to Assign")
    def input_username_to_assign(self, user_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '(//input[@data-testid="actionSearchUsername"])[1]'
        )
        element.send_keys(user_name)
        element.send_keys(Keys.ENTER)

    # Input Username to Un-assign
    @allure.step("Input Username to Un-assign")
    def input_username_to_un_assign(self, user_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '(//input[@data-testid="actionSearchUsername"])[2]'
        )
        element.send_keys(user_name)
        element.send_keys(Keys.ENTER)

    # Get the Role Name in Detail Page
    @allure.step("Get the Role Name in Detail Pag")
    def get_the_role_name_in_detail_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//h1[@class="ant-typography !mb-0"]'
        ).text
        return element

    # Get the Parent Team Name in Detail Page
    @allure.step("Get the Parent Team Name in Detail Page")
    def get_the_parent_in_detail_page(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@class="ant-card-body"]//tr[3]/td/div/span[2]'
        ).text
        return element

    # Get the Assign User in Detail Page
    @allure.step("Get the Assign User in Detail Page")
    def get_the_assign_user_in_detail_page(self):
        elements = WebDriverWait(self.driver, self.timeout).until(
            EC.presence_of_all_elements_located(
                (By.XPATH, '//*[@class="ant-table-content"]//tbody/tr[*]/td[1]/span[2]')
            )
        )
        user_list = [element.text for element in elements]
        if len(elements) > 0:
            print("--" * 15, user_list)
            return user_list
        elif len(elements) == 0:
            print("--" * 15, user_list)
            return False

        # element = self.find_element_wait(
        #     EC.presence_of_element_located,
        #     By.XPATH,
        #     '//*[@class="ant-table-content"]//tbody//tr[*]//td[1]').text
        # element_split = element.split("\n")
        # print("-"*10, element_split)
        # return element_split

    # Select Duplicate
    @allure.step("Select Duplicate")
    def select_duplicate(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@data-testid="duplicate"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Input the Duplicate Policy Name
    @allure.step("Modify the Policy Name")
    def input_duplicate_item_name(self, duplicate_role_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="name"]'
        )
        element.send_keys(Keys.COMMAND, "a")
        element.send_keys(Keys.BACKSPACE)
        self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="name"]'
        ).send_keys(duplicate_role_name)

    # Re-Select Role's Policies
    @allure.step("Re-Select Role's Policies")
    def reselect_role_policies(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-table-content"]//tr[3]//label[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-table-content"]//tr[5]//label[1]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Get the 1st Role Status by List
    @allure.step("Get the 1st Role Status by List")
    def get_1st_role_status_list(self):
        status = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            "(//button[@role='switch'])[1]"
        ).get_attribute("aria-checked")
        if status == 'true':
            status = True
        else:
            status = False
        return status

    # Delete Role
    @allure.step("Delete Role")
    def select_delete(self):
        click_element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//li[@data-testid="delete"]'
        )
        self.action_chains.move_to_element(click_element).click().perform()

    # Get Role Table Content
    @allure.step("Get Role Table Content")
    def get_role_table_content(self):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//div[@class="ant-table-content"]//tr[*]/td[1]'
        ).text
        element_split = element.split()
        print(element_split)
        return element_split

    # Create a new TeamRole
    @allure.step("Create New TeamRole")
    def create_new_teamrole(self, file_name: str):
        self.add_team_role()
        time.sleep(1)
        self.create_random_role(file_name)
        time.sleep(1)
        self.select_parent_team_for_create()
        time.sleep(1)
        self.select_all_role_policies()
        time.sleep(1)
        self.create_team_role()
        time.sleep(1)
        self.confirm_create_team_role()

    # Delete First TeamRole
    @allure.step("Delete First TeamRole")
    def delete_first_teamrole(self):
        self.click_the_action_btn_in_detail()
        time.sleep(1)
        self.select_delete()
        time.sleep(1)
        self.select_confirm_delete()
        time.sleep(1)

    @allure.step("Create Random Name")
    def create_random_item_name(self):
        random_str = ''.join(random.sample(string.ascii_letters + string.digits, 25))
        return random_str

    @allure.step("Create .txt file for case")
    def create_file_for_test_case(self, file_name: str):
        path = './temp_item_name/role/'
        random_item_name = self.create_random_item_name()
        with open(path + file_name + '.txt', 'w') as f:
            f.write(random_item_name)
        return random_item_name

    @allure.step("Get the Random Role name by File")
    def get_random_role_name_by_file(self, file_name: str):
        path = './temp_item_name/role/'
        with open(path + file_name + '.txt', 'r') as f:
            data = f.read()
        return data

    # Input Random Policy Name
    @allure.step("Input Random Role Name")
    def create_random_role(self, file_name: str):
        random_policy_name = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//*[@id="name"]'
        )
        random_str = self.create_file_for_test_case(file_name)
        random_policy_name.send_keys(random_str)
        return random_str

    # Filter Policy by Name
    @allure.step("Filter Role by Name")
    def search_role_by_random_name_file(self, file_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="roleNameList"]'
        )
        data = self.get_random_role_name_by_file(file_name)
        element.send_keys(data)
        time.sleep(3)
        element.send_keys(Keys.ENTER)

    # Filter Modify Role
    @allure.step("Filter Modify Role")
    def search_role_by_modify_name(self, modify_role_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="roleNameList"]'
        )
        element.send_keys(modify_role_name)
        time.sleep(1)
        element.send_keys(Keys.ENTER)

    # Filter Duplicate Role
    @allure.step("Filter Duplicate Role")
    def search_role_by_duplicate_name(self, duplicate_role_name: str):
        element = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            '//input[@id="roleNameList"]'
        )
        element.send_keys(duplicate_role_name)
        time.sleep(1)
        element.send_keys(Keys.ENTER)