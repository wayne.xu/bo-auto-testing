import re
import time
import allure

from typing import Tuple, List, Optional

from bo_shared.components.basic import BasicComponent
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import StaleElementReferenceException


SELECT_TREE_INFO = {
    'product_group': {
        'displayed_index': 1,
        'options_locate_id': 'productOptions',
    },
    'add_rebate_product_group': {
        'displayed_index': 1,
        'options_locate_id': 'productGroup',
    },
    'eligible_member': {
        'displayed_index': 2,
        'options_locate_id': 'memberOptions',
    },
}


class SelectTree(BasicComponent):

    def __init__(
        self,
        driver: WebDriver,
        displayed_index: int,
        options_locate_id: Optional[str] = None,
        options_locate_xpath: Optional[str] = None,
        scroll_step=100
    ):
        """
        Manipulate the SelectTree component

        Args:
            driver (WebDriver)
            displayed_index (int): The SelectTree options are displayed sequentially, according to the click sequence
            options_locate_id (Optional[str], optional): Defaults to None.
            options_locate_xpath (Optional[str], optional): Defaults to None.
            scroll_step (int, optional): Defaults to 100.

        Raises:
            ValueError: The argument `options_locate_id` and `options_locate_xpath` can be None at the same time.
        """
        super(SelectTree, self).__init__(driver)
        self.scroll_step = scroll_step
        self.displayed_index = displayed_index
        self.scroll_thread = 80
        self.expand_element_idx = 1
        self.click_element_idx = 2
        self.options_locate_id = options_locate_id
        self.options_locate_xpath = options_locate_xpath
        if self.options_locate_id is None\
            and self.options_locate_xpath is None:
            raise ValueError(
                "Argument `options_locate_id` and `options_locate_xpath` "
                "can be None at the same time"
            )

    @property
    def all_options_xpath(self) -> str:
        if self.displayed_index <= 1:
            options_xpath = (
                '//div[@class="ant-select-tree"]//'
                'div[@class="ant-select-tree-list"]//div[@data-testid]'
            )
        else:
            options_xpath = (
                f'(//div[@class="ant-select-tree"])[{self.displayed_index}]//'
                'div[@class="ant-select-tree-list"]//div[@data-testid]'
            )
        self.log.info(f'all_options_xpath: {options_xpath}')
        return options_xpath

    @property
    def current_group_options_height_xpath(self) -> str:
        if self.displayed_index <= 1:
            xpath = (
                '//div[@class="ant-select-tree-list"]/'
                'div[@class="ant-select-tree-list-holder"]/div'
            )
        else:
            xpath = (
                f'(//div[@class="ant-select-tree-list"])[{self.displayed_index}]'
                '/div[@class="ant-select-tree-list-holder"]/div'
            )
        self.log.info(f'current_group_options_height_xpath: {xpath}')
        return xpath

    @property
    def current_scrollbar_position_xpath(self) -> str:
        if self.displayed_index <= 1:
            xpath = (
                '//div[@class="ant-select-tree-list"]'
                '//div[@class="ant-select-tree-list-holder-inner"]'
            )
        else:
            xpath = (
                f'(//div[@class="ant-select-tree-list"])[{self.displayed_index}]'
                '//div[@class="ant-select-tree-list-holder-inner"]'
            )
        self.log.info(f"current_scrollbar_position_xpath: {xpath}")
        return xpath

    @property
    def scrollbar_xpath(self) -> str:
        if self.displayed_index <= 1:
            xpath = '//div[@class="ant-select-tree"]//div[@class="ant-select-tree-list-holder"]'
        else:
            xpath = (
                f'(//div[@class="ant-select-tree"])[{self.displayed_index}]'
                '//div[@class="ant-select-tree-list-holder"]'
            )
        self.log.info(f"scrollbar_xpath: {xpath}")
        return xpath

    @allure.step('Click the filter expanded options(select tree)')
    def click_expanded_options(self):
        space_pat = self.shared_regex['space_pat']
        if self.options_locate_id:
            xpath = '//div[@data-testid="{}"]'.format(
                self.options_locate_id
            )
        else:
            xpath = self.options_locate_xpath
        element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            xpath
        )
        attr = element.get_attribute('class')
        attr = space_pat.split(attr)
        if "ant-select-open" not in attr:
            self.action_chains.move_to_element(element).click().perform()

    def __get_group_id(self, data_test_id: str) -> Tuple[bool, str]:
        """
        Get group id and check it is master or not.

        Args:
            data_test_id (str): _description_

        Returns:
            Tuple[bool, str]: _description_
        """
        sub_options_pat = re.compile(r'[0-9]+\-[0-9]+')
        is_master = True
        if sub_options_pat.match(data_test_id):
            is_master = False
            group_id = data_test_id.split('-')[0]
        else:
            group_id = data_test_id
        return (is_master, group_id)

    def __reset_collection_element_info(self, collect_elements: dict) -> dict:
        """
        Reset all element value of option elements

        Args:
            collect_elements (dict)
        """
        for group_elements_info in collect_elements.values():
            for option_element_info in group_elements_info.values():
                option_element_info['element'] = None
        return collect_elements

    def _get_all_options(
        self,
        gid_map: Optional[dict] = None,
        default_collection_elements: Optional[dict] = None
    ) -> Tuple[dict, dict]:
        """
        Get all filter expanded options. The options is tree structure, and it needs to be clicked
        than the options will be displayed on HTML. If gid_map and default_collection_elements are given,
        this function will used them as default value(Only update the state of data to reach data memorable).

        Args:
            gid_map (Optional[dict], optional): Defaults to None.
            default_collection_elements (Optional[dict], optional): Defaults to None.

        """
        time.sleep(1)
        space_pat = self.shared_regex['space_pat']
        option_elements: List[WebElement] = self.find_element_wait(
            EC.presence_of_all_elements_located,
            By.XPATH,
            self.all_options_xpath
        )
        # Group id
        if gid_map is None:
            gid_map = dict()

        if default_collection_elements:
            # Reset old information
            collect_elements = self.__reset_collection_element_info(default_collection_elements)
        else:
            collect_elements = dict()

        for option_element in option_elements:
            element_text = option_element.text.strip().lower()
            if not element_text:
                continue
            element_text = self.normalize_string(element_text)
            data_test_id = option_element.get_attribute('data-testid')
            is_master, gid = self.__get_group_id(data_test_id)
            # Get span element to get the elements info
            span_eles: List[WebElement] = option_element.find_elements(By.XPATH, 'span')
            # Get attribute to know the element is expandable or not
            is_expandable_attr = span_eles[self.expand_element_idx].get_attribute('class')
            is_expandable_attr = space_pat.split(is_expandable_attr)
            # Get attribute to know the element is selectable or not
            is_selectable_attr = span_eles[self.click_element_idx].get_attribute('class')
            is_selectable_attr = space_pat.split(is_selectable_attr)

            is_expandable = False
            already_expanded = False
            element_selectable_attr = 'ant-select-tree-checkbox-checked'
            if 'ant-select-tree-switcher-noop' not in is_expandable_attr:
                is_expandable = True
            else:
                already_expanded = 'ant-select-tree-switcher_open' in is_expandable_attr

            element_info = {
                'text': element_text,
                'element': option_element,
                'data_id': data_test_id,
                'is_expandable': is_expandable,
                'is_selectable': False if element_selectable_attr in is_selectable_attr else True,
                'already_expanded': already_expanded,
                'is_master': is_master,
            }
            if is_master:
                gid_map[gid] = element_text
                group_name = element_text
                collect_elements[group_name] = {
                    group_name: element_info
                }
            else:
                group_name = gid_map[gid]
                collect_elements[group_name][element_text] = element_info

        return gid_map, collect_elements

    def __get_current_group_options_height(self) -> str:
        """
        Get current group option total height
        """
        dynamic_options_container: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            self.current_group_options_height_xpath
        )
        style_attrs = dynamic_options_container.get_attribute('style')
        if style_attrs:
            style_attrs = self.style_attrs_processor(style_attrs)
            try:
                total_height = style_attrs['height']
                total_height = total_height.replace('px', '')
            except KeyError:
                total_height = None
        else:
            total_height = None
        return total_height

    def _get_current_scroll_position(self) -> Optional[int]:
        """
        Get current scroll position, it's used to determine
        whether the scrollbar is on the bottom or not
        """
        number_pat = self.shared_regex['number_pat']
        element: WebElement = self.find_element_wait(
            EC.presence_of_element_located,
            By.XPATH,
            self.current_scrollbar_position_xpath
        )
        style_attrs = element.get_attribute('style')
        style_attrs = self.style_attrs_processor(style_attrs)
        current_position = None
        if 'transform' in style_attrs:
            position = style_attrs['transform']
            search_result = number_pat.search(position)
            current_position = int(search_result.group())
        return current_position

    def _manipulate_select_tree_element(
        self,
        web_element: WebElement,
        action: str
    ):
        """
        Manipulate the select tree element

        Args:
            web_element (WebElement)
            action (str): expand or click
        """
        if action == 'expand':
            idx = self.expand_element_idx
        elif action == 'click':
            idx = self.click_element_idx
        else:
            raise ValueError(f"Unknown action: {action}")
        span_eles = web_element.find_elements(By.XPATH, 'span')
        target_element = span_eles[idx]
        self.action_chains.move_to_element(target_element).click().perform()

    @allure.step("Expand select tre options")
    def expand_group_filter_option(self, element_info: dict):
        """
        Expand the group filter option

        Args:
            element_info (dict)
            Ex: {
                'text': jbp,
                'element': WebElement,
                'data_id': 1-13,
                'is_expandable': False,
                'is_selectable': True,
                'already_expanded': False,
                'is_master': False,
            }
        """
        if element_info['already_expanded'] == False:
            self._manipulate_select_tree_element(element_info['element'], 'expand')
            self.log.debug(f"Expand {element_info['text']} element")

    @allure.step("Click select tre options")
    def click_group_filter_option(self, element_info: dict):
        """
        Click the group filter option

        Args:
            element_info (dict):
            Ex: {
                'text': jbp,
                'element': WebElement,
                'data_id': 1-13,
                'is_expandable': False,
                'is_selectable': True,
                'already_expanded': False,
                'is_master': False,
            }
        """
        if element_info['is_selectable'] == True:
            self._manipulate_select_tree_element(element_info['element'], 'click')
            self.log.debug(f"Click {element_info['text']} element")

    def scroll_down_and_checked(self) -> bool:
        """
        Scroll down group option block and check whether is on the bottom or not.
        """
        scroll_locator = (By.XPATH, self.scrollbar_xpath)
        if self.scroll_step < self.scroll_thread:
            raise ValueError(
                f"The scroll_step: {self.scroll_step} must be large than {self.scroll_thread}"
            )
        current_position = self._get_current_scroll_position()
        scroll_to_position = current_position + self.scroll_step
        self.scroll_to(0, scroll_to_position, scroll_locator)
        current_position = self._get_current_scroll_position()
        if current_position == scroll_to_position:
            return False
        else:
            return True

    def scroll_until_clicking_option(
        self,
        gid_map: dict,
        group_name: str,
        option_names: List[str],
        collection_elements: dict
    ):
        """
        Scroll until clicking the all options, or until scrolling
        to the button

        Args:
            gid_map (dict)
            group_name (str)
            option_names (List[str])
            collection_elements (dict)
        """
        for option_name in option_names:
            while True:
                _, collection_elements = self._get_all_options(
                    gid_map=gid_map,
                    default_collection_elements=collection_elements
                )
                group_element_info = collection_elements[group_name]
                # If the option name doesn't exist in the group_element_info,
                # it haven't been found, or it doesn't exist
                try:
                    option_element_info = group_element_info[option_name]
                except KeyError:
                    if self.scroll_down_and_checked():
                        continue
                    else:
                        raise ValueError(
                            f"Can't find the group_name: {group_name} "
                            f"and option_name: {option_name}"
                        )
                # Check the option has been selectable or not
                if option_element_info['is_selectable'] == True:
                    self.click_group_filter_option(option_element_info)
                    break
                try:
                    self.click_group_filter_option(option_element_info)
                    break
                except StaleElementReferenceException:
                    if self.scroll_down_and_checked():
                        continue
                    else:
                        raise ValueError(
                            f"Can't find the group_name: {group_name} "
                            f"and option_name: {option_name}"
                        )
            self.log.debug(
                f'Successfully click group_name: {group_name} '
                f'and option_name: {option_name}'
            )

    def scroll_down_until_group_expanding(
        self,
        gid_map: dict,
        group_name: str,
        collection_elements: dict
    ):
        """
        Scroll down until finding the group WebElement and expand it

        Args:
            gid_map (dict)
            group_name (str)
            collection_elements (dict)
        """
        while True:
            if self.scroll_down_and_checked():
                _, collection_elements = self._get_all_options(gid_map, collection_elements)
                group_main_element_info = collection_elements[group_name][group_name]
                if group_main_element_info['element'] is not None:
                    self.expand_group_filter_option(group_main_element_info)
                    break
            else:
                raise ValueError(f"Can't find the group_name: {group_name}")

    @allure.step("Select select tree options")
    def select_filter_expanded_options(self, select_options: List[dict]):
        """
        Select the filter expanded options

        Args:
            select_options (dict): [{'keno': ['sgw', 'gpk', 'ows']}]

        Flow:
            Get options -> Expand the group -> Check whether it need to scroll or not
            1. Need to scroll -> Get options -> Click options that are already displayed on HTML
             -> Not displayed on HTML -> Scroll until finding or the scrollbar is on the bottom
             -> Back to first step
            2. No need to scroll -> Click options
        """
        gid_map, collection_elements = self._get_all_options()
        for select_option in select_options:
            for group_name, option_names in select_option.items():
                if group_name not in collection_elements:
                    raise ValueError(
                        f"The group_name: {group_name} doesn't in the "
                        f"collection_elements: {list(collection_elements.keys())}"
                    )
                group_info = collection_elements[group_name]
                group_element_info = group_info[group_name]
                # When scroll down, the group_element_info may not show on HTML.
                # In this time, the group_element_info['element'] will be None
                if group_element_info['element']:
                    self.expand_group_filter_option(group_element_info)
                else:
                    self.scroll_down_until_group_expanding(
                        gid_map=gid_map,
                        group_name=group_name,
                        collection_elements=collection_elements
                    )
                # Refresh the current group option
                _, collection_elements = self._get_all_options(
                    gid_map=gid_map,
                    default_collection_elements=collection_elements
                )
                group_info = collection_elements[group_name]
                options_total_height = self.__get_current_group_options_height()
                # If options_total_height is None, it means it doesn't need to scroll.
                # It can be considered as a signal to using `scroll_until_click_option`.
                if options_total_height is None:
                    for option_name in option_names:
                        option_element_info = group_info[option_name]
                        self.click_group_filter_option(option_element_info)
                        self.log.debug(f"Click the group name: {group_name}, option_name: {option_name}")
                else:
                    self.scroll_until_clicking_option(gid_map, group_name, option_names, collection_elements)

    def __repr__(self) -> str:
        return (
            f"<{self.__class__.__name__} Component "
            f"scroll_step: {self.scroll_step} "
            f"displayed_index: {self.displayed_index}>"
        )