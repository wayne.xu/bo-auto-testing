import os
import time

from copy import deepcopy
from pathlib import Path
from functools import wraps
from typing import (
    Set, Any, Union,
    Callable, Optional
)

from filelock import FileLock, Timeout

from utils.utils import SystemLogger, HelperFuncs


log = SystemLogger.get_logger()


def lock_file(timeout: int = 10, use_args_hash: bool = False) -> Callable:
    """
    Lock file to ensure the file consistency.
    """
    lock_file_ext = 'txt.lock'
    lock_file_dir = os.path.join(os.getcwd(), 'lock_file')
    path = Path(lock_file_dir)
    if not path.exists():
        path.mkdir(parents=True, exist_ok=True)
        log.info(f"Create a new directory on {lock_file_dir}")

    def decorator(func: Callable) -> Callable:
        @wraps(func)
        def wrapper_func(*args, **kwargs) -> Any:
            if use_args_hash:
                lock_identifier = {
                    'func_name': func.__name__,
                    'args': args, 'kwargs': kwargs
                }
            else:
                lock_identifier = {
                    'func_name': func.__name__
                }
            lock_identifier = HelperFuncs.hash_dict(lock_identifier)
            lock_file_path = os.path.join(
                lock_file_dir,
                f'{lock_identifier}.{lock_file_ext}'
            )
            try:
                with FileLock(lock_file_path, timeout):
                    log.info(
                        f'write_file_func => "{func.__name__}", '
                        f'pid: {os.getpid()} acquires the file lock to write file.'
                    )
                    result = func(*args, **kwargs)
            except Timeout as e:
                raise e
            return result
        return wrapper_func
    return decorator


def retry_func(
    exceptions: tuple,
    retry_times: int = 5,
    retry_time_interval: float = 0.5
) -> Callable:
    """
    Retry function when it occurs expected exceptions.

    Args:
        exceptions (tuple): The expected exceptions
        retry_times (int): retry times
        retry_time_interval (float): time interval between each execution
    """
    exceptions = tuple(exceptions)
    def decorator(func: Callable) -> Callable:
        @wraps(func)
        def wrapper_func(*args, **kwargs) -> Any:
            execute_times = 0
            result = None
            while execute_times < retry_times:
                try:
                    result = func(*args, **kwargs)
                    break
                except exceptions:
                    execute_times += 1
                    time.sleep(retry_time_interval)
                except Exception as e:
                    raise e
                log.debug(
                    f"Execute {func.__name__} function with args: {args}, "
                    f"kwargs: {kwargs}, {execute_times} times"
                )
            return result
        return wrapper_func
    return decorator


def execute_env(
    envs: Union[str, set, tuple, list],
    set_attr: bool = False,
    attr_name: Optional[str] = None
) -> Callable:
    """
    According the environment variable `DEPLOYMENT_ENV` to determine
    whether executing the function.

    Args:
        envs (Union[str, set, tuple, list])
        set_attr (bool, optional): Defaults to False.
        attr_name (Optional[str], optional): Defaults to None.
    """
    if isinstance(envs, (tuple, list)):
        envs = set(envs)
    elif isinstance(envs, str) and envs.lower() == 'all':
        envs = envs.lower()
    else:
        raise ValueError(
            f"InValid envs: {envs}. Only `all` can't be string, "
            "the reset of envs need to be as set, tuple or list."
        )
    if set_attr and attr_name is None:
        # Give a default attribute name for setting to function attribute
        attr_name = 'skip_by_env'

    # Set allow execute default value
    allow_execute = False
    current_env: str = os.environ.get("DEPLOYMENT_ENV", None)
    if current_env is None:
        raise ValueError("The variable `DEPLOYMENT_ENV` can't be None")
    else:
        language = None
        if '-' in current_env:
            language, current_env = current_env.split('-')

    def decorator(func) -> Callable:
        @wraps(func)
        def wrapper_func(*args, **kwargs) -> Any:
            result = None
            nonlocal allow_execute
            if envs == 'all' or current_env in envs:
                log_msg = (
                    f"Execute the {func.__name__}, and "
                    f"current_env: {current_env}, executable envs: {envs}"
                )
                if language:
                    log_msg += f', language: {language}'
                log.debug(log_msg)
                allow_execute = True
                result = func(*args, **kwargs)
            else:
                log_msg = (
                    f"Skip to execute the {func.__name__}, "
                    f"current_env: {current_env} doesn't include in executable envs: {envs}"
                )
                if language:
                    log_msg += f', language: {language}'
                log.debug(log_msg)
            return result
        # Add new attr into function
        if set_attr:
            setattr(wrapper_func, attr_name, allow_execute)
        return wrapper_func
    return decorator


def restart_func(max_time: int = 5, execution_interval: float = 1.0) -> Callable:
    """
    Restart function until max_time.
    It's used for rechecking whether the specific situation is occurred

    Args:
        max_time (int, optional): Defaults to 5
        execution_interval (float, optional): The stop time between each execution. Defaults to 1.0
    """
    def decorator(func: Callable) -> Callable:
        @wraps(func)
        def wrapper_func(*args, **kwargs) -> Any:
            execution_time = 0
            while True:
                execution_time += 1
                log.debug(
                    f"Execute func: {func.__name__} with args: {args}, "
                    f"kwargs: {kwargs}, execution_time: {execution_time}"
                )
                try:
                    result = func(*args, **kwargs)
                except Exception as e:
                    raise e
                if execution_time == max_time:
                    break
                time.sleep(execution_interval)
            return result
        return wrapper_func
    return decorator
